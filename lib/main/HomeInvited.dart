// ignore_for_file: file_names

import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:just_audio/just_audio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../datamap/AnswerMap.dart';
import '../datamap/InviteUserMap.dart';
import '../datamap/QuestionMap.dart';
import '../datamap/SimpleDataMap.dart';
import '../invited/InvitedPost.dart';
import '../main/profile.dart';
import '../start/globals.dart' as global;
import '../datamap/preMap.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../datamap/SettingsMap.dart';



import '../widget/cardShow.dart' as cardShow;

class InvitedHomePAge extends StatefulWidget {
  const InvitedHomePAge({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<InvitedHomePAge> createState() => _InvitedHomePAgeState();
}

class _InvitedHomePAgeState extends State<InvitedHomePAge> with TickerProviderStateMixin{

  ScrollController scrollController=ScrollController();


  late AudioPlayer player;
  final alarmAudioPath = "assets/didi.mp3";
  String user_id="", phone="",name="",pic="";
  late SharedPreferences prefs;
  List<PreMap> myPresent=[];
  List<PreMap> invitePresent=[];
  int maxQ=0,start=0,limit=0;

  late AnimationController controller;
  void checkFirst() async{
    prefs = await SharedPreferences.getInstance();
    phone = prefs.getString('phone').toString();
    pic = prefs.getString('pic').toString();
    name =await prefs.getString('name').toString();
    connectSocketMain(phone);
  }

  List<QuestionMap> questions=[];
  List<SimpleDataMap> aData=[];
  List<SimpleDataMap> qData=[];
  List<List<AnswerMap>> answers=[];
  List<SimpleDataMap> userData=[];


  @override
  void initState(){
    super.initState();
    player = AudioPlayer();
    fetchItem();
    checkFirst();
    controller =
        BottomSheet.createAnimationController(this);
    controller.duration = const Duration(seconds: 2);

  }


  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("جویا" ,style:  TextStyle(fontFamily: "estedad",fontSize: 12),),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(CupertinoIcons.home),
            onPressed: () => Navigator.pop(context),
          ),
          actions: [
            InkWell(
              onTap: () async {
                await Navigator.push( context,MaterialPageRoute(builder: (context) {
                  return profile(isMe: true,user_id: user_id,organizer: false,);
                }));
                setState(() {
                  pic=prefs.getString('pic').toString();
                });
                //globals.socket.emit("viewer"," {some}");
              },
              child: pic!=""?Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundImage: NetworkImage(global.ip+pic),
                  backgroundColor: Colors.transparent,
                  radius: 22,
                ),
              ):const Icon(CupertinoIcons.person_alt_circle),
            ),
          ],
        ),
          floatingActionButton:
        FloatingActionButton.extended(
          heroTag: "float_Home",
          onPressed: () {
            _writeQuestion(context);
          },
          label: const Text('پیوستن به رویداد'),
          icon: const Icon(CupertinoIcons.plus),
          backgroundColor: Colors.blue,
          extendedTextStyle: const TextStyle(fontFamily: "estedad",fontSize: 12),
        ),

        floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
        body:  Stack(
          children: [
            ClipPath(
              clipper: WaveClipperTwo(reverse: false),
              child: Container(
                height: 175,
                color: Colors.blue,
              ),
            ),
            Directionality(
              textDirection: TextDirection.rtl,
              child:
              Column(
                children: [
                  if(!kIsWeb) Container(
                    color: Colors.blue,
                    height: 28,
                  ),
                  Center(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 15),
                          child: Text("امروز: "+DateTime.now().toPersianDateStr(),
                            style: const TextStyle(fontFamily: "Vazir",fontWeight: FontWeight.bold,color: Colors.white,fontSize: 11),),
                        ),
                      ],
                    ),
                  ),

                  Expanded(
                    child: SingleChildScrollView(
                      child: Wrap(
                        children: List.generate(invitePresent.length,(int position){
                          return listMaker(invitePresent[position],position,context);
                        }),
                      ),
                    ),
                  ),

                ],
              ),
            ),

          ],
        ),
        // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
  playLocal() async {
    // await audioPlayer.play(alarmAudioPath, isLocal: true);
    await player.setAsset(alarmAudioPath);
    player.play();
  }
  fetchItem() async{
    print("fetch home");


    prefs = await SharedPreferences.getInstance();
    user_id =await prefs.getString('user_id').toString();
    phone =await prefs.getString('phone').toString();


    print("user_id = $user_id");
    _getPresent(phone,'invited',false);
  }
  Widget listMaker(PreMap preMap,int pos,BuildContext context) {
    var date = DateTime.fromMillisecondsSinceEpoch(preMap.time * 1000);
    String perDate=date.toPersianDateStr()+" ساعت: "+date.hour.toString()+":"+date.minute.toString();
    //print("owner ="+preMap.owner+" user"+ user_id +"group="+ preMap.group);
    String pic="";
    if(preMap.logo!="")pic=global.ip+global.minImage(preMap.logo);

    Color titleColor=Colors.blue;
    Color inviteColor=Colors.black87;
    Color dataColor=Colors.black87;
    String backPic="";
    String setting=preMap.settings;
    bool cropLogo=true,cover=true;
    int model=0;
    if(setting!=""){
      SettingsMap settingsMap=SettingsMap.decodeSettings(setting);
      titleColor=Color(settingsMap.titleColor);
      inviteColor=Color(settingsMap.inviteColor);
      dataColor=Color(settingsMap.dataColor);
      backPic=settingsMap.backImage;
      cropLogo=settingsMap.cropLogo;
      cover=settingsMap.cover;
      model=settingsMap.model;
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          onTap: (){
            Navigator.push( context,MaterialPageRoute(builder: (context) =>  InvitedPostPage(myPresent: preMap,preView: false,orgInfo: [],)),);
          },
          child: cardShow.cardMakerX(preMap,false, true,context),
        ),
        CountdownTimer(
          endTime: preMap.time*1000,
          widgetBuilder: (_, CurrentRemainingTime? time) {
            if (time == null) {
              return Row(
                mainAxisSize: MainAxisSize.min,
                textDirection: TextDirection.ltr,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  countDown(0),
                  countDown(0),
                  countDown(0),
                  countDown(0),
                ],
              );
            }
            else {
              return Row(
                textDirection: TextDirection.ltr,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  countDown(time.days),
                  countDown(time.hours),
                  countDown(time.min),
                  countDown(time.sec),
                ],
              );
            }
            /*return Text(
                'days: [ ${time.days} ], hours: [ ${time.hours} ], min: [ ${time.min} ], sec: [ ${time.sec} ]');*/
          },
        ),
      ],
    );
  }
  Widget countDown(int? time){
    time ??= 0;
    String sm = time.toString().padLeft(2, "0");
    return Container(
      width: 22,
      height: 27,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Center(child: Text(sm,style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white),)),
    );
  }
  Widget listMaker2(PreMap preMap,int pos,BuildContext context) {

    String pic="";
    if(preMap.logo!="")pic=global.ip+global.minImage(preMap.logo);

    Color titleColor=Colors.blue;
    String backPic="";
    String setting=preMap.settings;
    bool cropLogo=true;
    if(setting!=""){
      SettingsMap settingsMap=SettingsMap.decodeSettings(setting);
      titleColor=Color(settingsMap.titleColor);
      backPic=settingsMap.backImage;
      cropLogo=settingsMap.cropLogo;
    }


    return Container(
      margin: EdgeInsets.only(right: pos==0? kIsWeb?115:135:0),
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(top:8),
            child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: const EdgeInsets.all(10),
                child: InkWell(
                  onTap: () {
                  /*  preMap.group=='invited'?*/
               /*     Navigator.push( context,MaterialPageRoute(builder: (context) =>  WatchPresentation(myPresent: preMap)),):
                    Navigator.push( context,MaterialPageRoute(builder: (context) =>  OnePresentation(myPresent: preMap,fromArchive: false,)),);
                 */ },
                  child: SizedBox(
                    width: 140,
                    height:50,
                    child: Stack(
                      children: [
                        CachedNetworkImage(
                          imageUrl: global.ip+backPic,
                          width: 140,
                          height: 50,
                          fit: BoxFit.cover,
                          errorWidget: (context, url, error) =>  Container(
                            width: 130,
                            height: 43,
                          ),
                        ),
                        Directionality(
                          textDirection: TextDirection.rtl,
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Center(
                                      child:
                                      AutoSizeText(
                                        preMap.title.toString().toPersianDigit(),
                                        style: TextStyle(color:titleColor,fontFamily: "estedad",fontSize: 14,fontWeight: FontWeight.bold),
                                        minFontSize: 9,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      )
                                  ),
                                ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                )),
          ),
          if(preMap.logo!="") Align(
            alignment: Alignment.topCenter,
            child:  Padding(
              padding: const EdgeInsets.only(left: 15,right: 15),
              child:  CachedNetworkImage(
                imageUrl: pic,
                height: 40,
                width: 40,
                imageBuilder: (context, imageProvider) => Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: cropLogo?BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ): BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.contain)
                  ),
                ),
                placeholder: (context, url) => const CircularProgressIndicator(),
                errorWidget: (context, url, error) => Image.asset("image/rad_survey_back.png"),
              ),
            ) ,
          ),
          if(preMap.owner != user_id && preMap.group!='invited')
            Container(
              margin: const EdgeInsets.only(top: 50,right: 140),
              child: const Icon(CupertinoIcons.person_solid,size: 17,),),
        ],
      ),
    );
  }

  Future<bool> _getPresent(String phone,String type,bool one) async {
    print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/presentation";
    if(one){
      url=global.ip+"/api/one_presentation";
    }
    else if(type=='invited') {
      url=global.ip+"/api/invite_present";
    }
    else if(type=='manager'){
      url=global.ip+"/api/manage_present";
    }
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    // String token=prefs.getString("access_token").toString();
    print("url ="+url);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'phone': phone,
        'user_id': phone,
        'p_id': phone,
      }),
    );
    print("response invite="+response.body.toString());
    if(response.statusCode == 200) {
      print(response.body);
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        if(jsonResponse['ok']){

          var datas=json.decode(jsonResponse['data']);
          /*if(type=='invited'){
              invitePresent.clear();
            }
            else if(!one){
              myPresent.clear();
            }*/
          _doProcess(datas,type);
        }
      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    return true;
  }
  void _doProcess(var dataS,String type){
    for(var data in dataS ) {
      String id = data['_id'];

      String owner = data['owner'];
      String title = data['title'];
      String titleExp = data['title_exp'];
      String logo = data['logo'];
      String pic = data['pic'];
      int p_code = data['p_code'];

      String inviteText = data['invite_text'];
      String settings = data['settings'];
      int time_date = data['time_date'];
      int time_date_end = data['time_date_end'];
      String invitedInfo = data['invitedInfo'];
      String location = data['location'];
      String tel = data['tel'];
      String tags = data['tags'];
      String email = data['email'];
      String web = data['address'];
      bool isActive = data['is_active'];
      bool freeQuestion = data['free_question'];
      print('invite_text=$inviteText');
      if(type=='invited'){
        invitePresent.add(PreMap(owner,time_date,time_date_end,title,
            titleExp, pic, logo, p_code,type, inviteText, settings, location,
            invitedInfo, tel, tags, email, web, id,freeQuestion, isActive));
        connectSocket(id);
      }
      else {
        myPresent.add(PreMap(owner,time_date,time_date_end,title,
            titleExp, pic, logo, p_code,type, inviteText, settings, location,
            invitedInfo, tel, tags, email, web, id,freeQuestion, isActive));
        connectSocket(id);
      }
    }
    setState(() {

    });

  }
  void connectSocket(String p_id){
    global.socket.on(p_id, (dataS) {
      var data=json.decode(dataS);
      print("connectSocketOne= "+data.toString());
      String id = data['_id'];

      String owner = data['owner'];
      String title = data['title'];
      String titleExp = data['title_exp'];
      String logo = data['logo'];
      String pic = data['pic'];
      int p_code = data['p_code'];

      String inviteText = data['invite_text'];
      String settings = data['settings'];
      int time_date = data['time_date'];
      int time_date_end = data['time_date_end'];
      String invitedInfo = data['invitedInfo'];
      String location = data['location'];
      String tel = data['tel'];
      String tags = data['tags'];
      String email = data['email'];
      String web = data['address'];
      bool isActive = data['is_active'];
      bool is_del = data['is_del']??false;
      bool freeQuestion = data['free_question'];
      for(int i=0;i<myPresent.length;i++){
        PreMap pre=myPresent[i];
        if(pre.id==id){
          String group=pre.group;
          myPresent.removeAt(i);
          if(!is_del) {
            myPresent.add(PreMap(owner,time_date,time_date_end,title,
                titleExp, pic, logo, p_code,group, inviteText, settings, location,
                invitedInfo, tel, tags, email, web, id,freeQuestion, isActive));
          }
        }
      }
      for(int i=0;i<invitePresent.length;i++){
        PreMap pre=invitePresent[i];
        if(pre.id==id){
          String group=pre.group;
          invitePresent.removeAt(i);
          if(!is_del) {
            invitePresent.add(PreMap(owner,time_date,time_date_end,title,
                titleExp, pic, logo, p_code,group, inviteText, settings, location,
                invitedInfo, tel, tags, email, web, id,freeQuestion, isActive));
          }
        }
      }
      setState(() {

      });

      return null;
    });

  }
  void processSocketMyPhone(var data){
    var jso=json.decode(data);
    String type=jso['type'];
    String pId=jso['pId'];
    if(type.contains("not")){
      if(type.contains('invited')){

        for(int i=0;i<invitePresent.length;i++){
          if(invitePresent[i].id==pId){
            setState(() {
              invitePresent.removeAt(i);
            });
            break;
          }
        }
      }
      else {
        for(int i=0;i<myPresent.length;i++){
          if(myPresent[i].id==pId){
            setState(() {
              myPresent.removeAt(i);
            });
            break;
          }
        }
      }
    }
    else {
      if (type.contains('invited')) {
        for (int i = 0; i < invitePresent.length; i++) {
          if (invitePresent[i].id == pId) {
            invitePresent.removeAt(i);
            break;
          }
        }
      }
      else {
        for (int i = 0; i < myPresent.length; i++) {
          if (myPresent[i].id == pId) {
            myPresent.removeAt(i);
            break;
          }
        }
      }

      playLocal();
      _getPresent(jso['pId'], jso['type'], true);
    }
  }
  void connectSocketMain(String phone){
    global.socket.on(phone, (data) {
      processSocketMyPhone(data);
      return null;
    });
  }
  void _writeQuestion(BuildContext context) {

    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        TextEditingController questionController=TextEditingController(text: "");
        return AlertDialog(

          title: const Center(child: Text('کد رویداد را از برگزارکننده بپرسید',style: TextStyle(color: Colors.blueAccent,fontFamily: "estedad",fontSize: 15),)),
          content: Directionality(
            textDirection: TextDirection.rtl,
            child: SingleChildScrollView(
              child:  Center(
                child: Column(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical:4),
                        child: TextFormField(
                          autofocus: true,
                          textDirection: TextDirection.ltr,
                          controller: questionController,
                          decoration: const InputDecoration(
                            // ignore: prefer_const_constructors
                              border: OutlineInputBorder(),
                              labelText: 'کد رویداد'
                          ),
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          onFieldSubmitted: (pCode)async{
                            PreMap pr=await _getPreByCode(pCode.toString().toEnglishDigit());
                          if(pr.owner.isNotEmpty) {
                            if (pr.isActive) {
                              if (pr.freeQuestion) {
                                await _sendUser(pr.id);
                                Navigator.pop(context);
                              } else {
                                Fluttertoast.showToast(
                                    msg: "این رویداد عمومی نیست!",
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 3,
                                    backgroundColor: Colors.blue,
                                    webBgColor: '#2196F3',
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }
                            }
                            else {
                              Fluttertoast.showToast(
                                  msg: "این رویداد فعال نیست!",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.blue,
                                  webBgColor: '#2196F3',
                                  textColor: Colors.white,
                                  fontSize: 16.0
                              );
                            }
                          }

                          },
                          style: const TextStyle(
                              fontSize: 15.0,
                              fontFamily: 'estedad',
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: MaterialButton(
                        child: const SizedBox(
                          width: double.infinity,
                          child: Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Center(
                              child: Text('تایید',
                                  style: TextStyle(fontFamily: "Vazir",fontSize: 15 ,fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ),
                        onPressed: () async{
                          PreMap pr=await _getPreByCode(questionController.text.toString().toEnglishDigit());
                          if(pr.owner.isNotEmpty) {
                            if (pr.isActive) {
                              if (pr.freeQuestion) {
                                await _sendUser(pr.id);
                                Navigator.pop(context);
                              } else {
                                Fluttertoast.showToast(
                                    msg: "این رویداد عمومی نیست!",
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 3,
                                    backgroundColor: Colors.blue,
                                    webBgColor: '#2196F3',
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }
                            }
                            else {
                              Fluttertoast.showToast(
                                  msg: "این رویداد فعال نیست!",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.blue,
                                  webBgColor: '#2196F3',
                                  textColor: Colors.white,
                                  fontSize: 16.0
                              );
                            }
                          }
                        },
                        color: Colors.blue,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<PreMap> _getPreByCode(String pCode) async {

    PreMap project=PreMap("", 0,0, "", "", "", "", 0, "", "", "", "", "", "",
        "", "", "", "", false, false);
    var jsonResponse;
    String url=global.ip+"/api/preByCode";
    print("url ="+url);
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'p_code': pCode,
      }),
    );
    print("_getPreByCode ="+response.body.toString());
    if(response.statusCode == 200) {
      //  print(response.body);
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        bool correct=false;
        if(jsonResponse['ok']){
          if(jsonResponse['data']!=null) {
            if (jsonResponse['data'].toString() .length > 8) {
              correct=true;
              var data = json.decode(jsonResponse['data']);
              String id = data['_id'];
              String owner = data['owner'];
              String title = data['title'];
              String titleExp = data['title_exp'];
              String logo = data['logo'];
              String pic = data['pic'];
              int p_code = data['p_code'];

              String inviteText = data['invite_text'];
              String settings = data['settings'];
              int time_date = data['time_date'];
              int time_date_end = data['time_date_end'];
              String invitedInfo = data['invitedInfo'];
              String location = data['location'];
              String tel = data['tel'];
              String tags = data['tags'];
              String email = data['email'];
              String web = data['address'];
              bool isActive = data['is_active'];
              bool freeQ = data['free_question'];
              project= PreMap(owner,time_date,time_date_end,title,
                  titleExp, pic, logo, p_code,"", inviteText, settings, location,
                  invitedInfo, tel, tags, email, web, id,freeQ, isActive);
            }
          }
        }
        if(!correct){
          Fluttertoast.showToast(
              msg: "کد ورودی شما اشتباه است",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 3,
              backgroundColor: Colors.red,
              webBgColor: '#F44336',
              textColor: Colors.white,
              fontSize: 16.0
          );

        }
      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    return project;
  }
  Future<String> _sendUser(String p_id) async {
    String url = global.ip + "/api/new/person";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'p_id': p_id,
        'phone': phone,
        'type': "invited",
        'more': "accept",
      }),
    );
    String id="";
    if (response.statusCode == 200) {
      if (response.body != null) {
        var jsonResponse = json.decode(response.body);
        if (jsonResponse['ok']) {

          InviteUserMap inviteUserMap=InviteUserMap("invited", p_id, phone);
          String inviteUser=InviteUserMap.encode(inviteUserMap);
          global.socket.emit("newInviteUser",inviteUser);

          return jsonResponse['id'];
        } else {
          Fluttertoast.showToast(
              msg: jsonResponse['id'],
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 3,
              backgroundColor: Colors.red,
              webBgColor: '#F44336',
              textColor: Colors.white,
              fontSize: 16.0
          );
          return id;
        }
      }
      else {
        return id;
      }
    }
    else {
      return id;
    }
  }



  List<SimpleDataMap> oneQuestionAnswer(String qId){
    List<SimpleDataMap> sdm=[];
    for(int i=0;i<aData.length;i++){
      if(aData[i].qId==qId) {
        sdm.add(SimpleDataMap(qId, aData[i].id, aData[i].num));
      }
    }
    return sdm;
  }
  Widget generateListAnswers2(String title,int n,context) {
    // if(title.length>17)  title=title.substring(0,14)+"...";

    return Card(
      color: Colors.blue,
      shape:const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      elevation: 1,
      child:  Wrap(
        textDirection: TextDirection.rtl,
        children: [
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(n.toString().toPersianDigit(),style:const TextStyle(color: Colors.yellow,fontWeight:FontWeight.bold,fontSize: 11,fontFamily: "estedad",),),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: AutoSizeText(
              title.toString().toPersianDigit(),
              style:const TextStyle(color: Colors.white,fontFamily: "estedad",fontSize: 10),
              minFontSize: 7,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }
  Widget generateListAnswers(AnswerMap ans,int n,int count,context) {
    String title=ans.title;
    int color=ans.color;
    double percent=0;
    String percents="0";
    for(SimpleDataMap x in aData){
      if(x.id==ans.id){
        int n=x.num;
        percent=n/count;
        percents=(percent*100).toStringAsFixed(2);

        break;
      }
    }
    double w=300;
    w=MediaQuery.of(context).size.width-16;
    if(w>370)w=370;
    return SizedBox(
      height: 62,
      width: w,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        textDirection: TextDirection.rtl,
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            textDirection: TextDirection.rtl,
            children: [
              Text(n.toString().toPersianDigit(),style:
              const TextStyle(fontFamily: "estedad",color: Colors.blueGrey,fontWeight:FontWeight.bold,fontSize: 11.5),),
              AutoSizeText(
                "  "+ title.toString().toPersianDigit(),
                style:const TextStyle(color: Colors.blueGrey,fontFamily: "estedad",fontSize: 11.5),
                minFontSize: 8,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            textDirection: TextDirection.rtl,
            children: [
              Text(
                percents.toString().toPersianDigit()+"%",
                style:  const TextStyle(fontSize: 11.0,color:Colors.blueGrey,fontFamily: "estedad",fontWeight: FontWeight.bold),
              ),
            /*  LinearPercentIndicator(
                width: w-20,
                lineHeight: 5.0,
                percent: percent,
                backgroundColor: Colors.grey[300],
                progressColor: Color(color),
              ),*/
            ],
          ),
        ],
      ),
    );
  }

  void setSocket(String q_id){
    print("setSocket q_id="+q_id);
    global.socket.on("A"+q_id, (dataS) {
      print("A_______"+dataS.toString());
      var na=json.decode(dataS);
      String q_id=na['q_id'];
      String a_id=na['a_id'];
      String user_id=na['user_id'];
      bool addOne=true;
      for(int i=0;i<qData.length;i++){
        SimpleDataMap sdm=qData[i];
        if(sdm.id==q_id){
          qData[i].num=qData[i].num+1;
          addOne=false;
          break;
        }
      }
      if(addOne)qData.add(SimpleDataMap(q_id, q_id, 1));
      int n=1;
      for(int i=0;i<aData.length;i++){
        SimpleDataMap sdm=aData[i];
        if(sdm.id==a_id){
          n=aData[i].num+1;
          aData.removeAt(i);
          break;
        }
      }
      for(int i=0;i<userData.length;i++){
        SimpleDataMap u=userData[i];
        if(u.id==user_id){
          userData.removeAt(i);
          break;
        }
      }
      userData.add(SimpleDataMap( q_id,user_id, 1));
      aData.add(SimpleDataMap(q_id, a_id, n));
      setState(() {

      });
      // _getAllDataAns(q_id);
      return null;
    });
  }
}

class MyCustomScrollBehavior extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => {
    PointerDeviceKind.touch,
    PointerDeviceKind.mouse,
  };
}


