// ignore_for_file: file_names, use_key_in_widget_constructors, avoid_print

import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../start/login_page.dart';

import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'HomeScreen.dart';
import '../start/globals.dart' as globals;


class SplashScreen extends StatefulWidget{


  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  bool show_refresh=false;
  bool show_exit=false;
  Future check() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString('user_id').toString();
    String token = prefs.getString('token').toString();
    bool _isLogin = (prefs.getBool('isLogin') ?? false);
    if (_isLogin) {
      if(userId.length>5) {
        _checkUser(userId,token);
      }
      else{
        prefs.setString("name", "");
        prefs.setString("phone","");
        prefs.setString("user_id", "");
        prefs.setBool("isLogin", false);
        Timer(const Duration(seconds: 1), () {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => LoginPage()));
        });
      }
    } else {
     // _getStations();
     Timer(const Duration(seconds: 1), () {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginPage()));
      });

    }
  }
  @override
  void initState() {
    super.initState();
    check();

   /**/
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black12,
      body: Container(
        color: Colors.white,
        child: Center(
          child:
         /* Material(
            child: InkWell(
              onTap: () {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => new ThirdPage()));
              },
              child:Image.asset('images/raxi_icon.png'),
            ),
          ),*/
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 5,
                  child: Image.asset('image/rad_survey.png',width: 150,height: 150,)),
              if(show_refresh)Container(
                margin: const EdgeInsets.only(top: 45),
                child: MaterialButton(child:const Text("تلاش مجدد",
                  style: TextStyle(fontFamily: "Vazir",fontSize: 14,color: Colors.blue),
                ),onPressed: (){
                  setState(() {
                  show_refresh=false;
                });}),
              ),
              if(show_exit)Container(
                margin: const EdgeInsets.only(top: 45),
                child: MaterialButton(child:const Text("خروج",
                  style: TextStyle(fontFamily: "Vazir",fontSize: 16,color: Colors.blue),
                ),onPressed: (){
                  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                  Navigator.pop(context);
                  ;}),
              ),
            ],
          ),

        ),
      ),
    );
  }

  Future<void> _checkUser(String userId,String token) async {

      var jsonResponse = null;


      String url = globals.ip+"/api/checkUser";
      var response = await http.post(
        Uri.parse(url),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'user_id': userId,
          'token': token,
        }),
      );

      if (response.statusCode == 200) {
        print("response code="+response.body);
        jsonResponse = json.decode(response.body);
        print("response js=" + jsonResponse.toString());
        if (jsonResponse != null) {
          bool ok = jsonResponse['ok'];
          if (ok) {
            double now= DateTime.now().millisecondsSinceEpoch/1000;
            int serverTime=jsonResponse['time'];
            double x=now-serverTime;
            print("x=" + x.toString());
            if(x>120 || x<-120){
              setState(() {
                show_exit=true;
              });
              Fluttertoast.showToast(
                  msg: "زمان سیستم شما با سرور یکی نیست لطفا ابتدا آن را تنظیم کنید",
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.TOP,
                  timeInSecForIosWeb: 3,
                  webBgColor: '#F44336',
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0
              );
            }else{
              if(jsonResponse['token']) {
                var data = json.decode(jsonResponse['data']);
                print("data=" + data.toString());
                bool userActive = data['user_active'];
                if (userActive) {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                          builder: (context) => const HomeScreen()));
                }
                else {
                  Fluttertoast.showToast(
                      msg: "حساب کاربری شما فعال نیست!",
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.TOP,
                      timeInSecForIosWeb: 3,
                      webBgColor: '#F44336',
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );
                  SharedPreferences prefs = await SharedPreferences
                      .getInstance();
                  prefs.setString("name", "");
                  prefs.setString("phone", "");
                  prefs.setString("user_id", "");
                  prefs.setString("token", "");
                  prefs.setString("pic", "");
                  prefs.setBool("isLogin", false);
                  Timer(const Duration(seconds: 1), () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  });
                }
              }else{
                Fluttertoast.showToast(
                    msg: "برای امنیت بیشتر دوباره وارد حساب خود شوید",
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.TOP,
                    timeInSecForIosWeb: 3,
                    webBgColor: '#F44336',
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0
                );
                SharedPreferences prefs = await SharedPreferences
                    .getInstance();
                prefs.setString("name", "");
                prefs.setString("phone", "");
                prefs.setString("user_id", "");
                prefs.setString("token", "");
                prefs.setString("pic", "");
                prefs.setBool("isLogin", false);
                Timer(const Duration(seconds: 1), () {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => LoginPage()));
                });
              }
            }
          }else{
            Fluttertoast.showToast(
                msg: jsonResponse['data'],
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.TOP,
                timeInSecForIosWeb: 3,
                webBgColor: '#F44336',
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0
            );
            setState(() {
              show_refresh=true;
            });
          }
        }
        else {
          Fluttertoast.showToast(
              msg: jsonResponse['data'],
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.TOP,
              timeInSecForIosWeb: 3,
              webBgColor: '#F44336',
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
          setState(() {
            show_refresh=true;
          });
          print("response.statusCode is: ${response.statusCode}");
          print("The error message is: ${response.body}");
        }
        return;
      }
    }


}