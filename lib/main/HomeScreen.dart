// ignore_for_file: file_names

import 'dart:convert';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter/cupertino.dart';
import 'package:just_audio/just_audio.dart';

import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../main/profile.dart';
import '../start/globals.dart' as globals;
import 'package:carousel_slider/carousel_slider.dart';

import 'HomeInvited.dart';


class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      scrollBehavior: MyCustomScrollBehavior(),
      title: 'نظرسنجی جویا',

      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'نظرسنجی جویا'),
      debugShowCheckedModeBanner: false,
    );
  }
}
final List<String> imageList = [
  "/uploads/ads/1.jpg",
  "/uploads/ads/2.jpg",
  "/uploads/ads/3.jpg",
];

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String user_id="", phone="",name="",pic="";
  late SharedPreferences prefs;

  void checkFirst() async{
    prefs = await SharedPreferences.getInstance();
    phone = prefs.getString('phone').toString();
    pic = prefs.getString('pic').toString();
    user_id = prefs.getString('user_id').toString();
    connectSocketMain(phone);
    name =await prefs.getString('name').toString();
    setState(() {

    });

  }
  late AudioPlayer player;
  final alarmAudioPath = "assets/didi.mp3";

  bool invitedMe=false;
  bool managerMe=false;
  @override
  void initState(){
    super.initState();
    player = AudioPlayer();
    checkFirst();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        notchMargin: 10.0,
        clipBehavior: Clip.antiAlias,
        elevation: 10,
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: SizedBox(
            height: 90,
            child: Row(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      MaterialButton(
                        color: Colors.white,
                        child:Padding(
                          padding: const EdgeInsets.only(top: 5,bottom: 5),
                          child: SizedBox(
                            height: 90,
                            width: double.infinity,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [
                                Icon(CupertinoIcons.person_solid,color: Colors.blue,),
                                Text("رویدادهای من",style: TextStyle(fontFamily: "estedad",fontSize: 12,color: Colors.blue),),
                              ],
                            ),
                          ),
                        ),
                        onPressed: ()  {
                          setState(() {
                            invitedMe=false;
                          });
                          Navigator.push( context,
                            MaterialPageRoute(builder: (context) =>  const InvitedHomePAge(title: "جویا")),);
                        },
                      ),
                      if(invitedMe)const Padding(
                        padding:  EdgeInsets.all(2.0),
                        child: CircleAvatar(
                          backgroundColor: Colors.red,
                          radius: 5.0,
                        ),
                      ),
                    ],
                  ),
                ),
               /* Expanded(
                  child: Stack(
                    children: [
                      MaterialButton(
                        color: Colors.white,
                        child:Padding(
                          padding: const EdgeInsets.only(top: 5,bottom: 5),
                          child: SizedBox(
                            height: 90,
                            width: double.infinity,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [
                                Icon(CupertinoIcons.group_solid,color: Colors.blue,),
                                Text("برگزار کننده هستم",style: TextStyle(fontFamily: "estedad",fontSize: 12,color: Colors.blue),),
                              ],
                            ),
                          ),
                        ),
                        onPressed: () async {
                          setState(() {
                            managerMe=false;
                          });
                          Navigator.push( context,
                            MaterialPageRoute(builder: (context) =>  const HomeOrganizer(title: "جویا")),);
                        },
                      ),
                      if(managerMe) const Positioned.fill(
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding:  EdgeInsets.all(2.0),
                              child: CircleAvatar(
                                backgroundColor: Colors.red,
                                radius: 5.0,
                              ),
                            ),
                        ),
                      ),
                      *//**//*
                    ],
                  ),
                )*/
              ],
            ),
          ),
        ),

      ),
      floatingActionButton:
      InkWell(
          onTap: () async {
            await Navigator.push( context,MaterialPageRoute(builder: (context) {
              return profile(isMe: true,user_id: user_id,organizer: false,);
            }));
            setState(() {
              pic=prefs.getString('pic').toString();
            });
            //globals.socket.emit("viewer"," {some}");
          },
          child: pic!=""?CircleAvatar(
            backgroundImage: NetworkImage(globals.ip+globals.minImage(pic)),
            backgroundColor: Colors.transparent,
            radius: 30,
          ):const CircleAvatar(
            backgroundColor: Colors.transparent,
            backgroundImage: ExactAssetImage('image/rad_survey.png'),
            radius: 30,
          ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked
      /*FloatingActionButton(

        onPressed: ()  {

        globals.socket.emit("viewer"," {some}");
        print("testSSS");
      *//*  Navigator.push( context,
          MaterialPageRoute(builder: (context) =>  NewQuestionMultiItems(freeQuestion: true,p_id: "0",type: "oneAnswer",)),);
       *//* // Add your onPressed code here!
         },backgroundColor: Colors.blue,child: const Icon(CupertinoIcons.chart_pie),),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,*/
      ,
      body:  Stack(
        children: [
          ClipPath(
            clipper: WaveClipperTwo(reverse: false),
            child: Container(
              height: 175,
              color: Colors.blue,
            ),
          ),
          Directionality(
            textDirection: TextDirection.rtl,
            child:
            Column(
              children: [
                if(!kIsWeb) Container(
                  color: Colors.blue,
                  height: 28,
                ),
                Center(
                  child: Row(
                    children: [
                      const Spacer(),
                      Padding(
                        padding: const EdgeInsets.only(right: 15),
                        child: Text("امروز: "+DateTime.now().toPersianDateStr(),
                          style: const TextStyle(fontFamily: "Vazir",fontWeight: FontWeight.bold,color: Colors.white,fontSize: 11),),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 40),
                  child: CarouselSlider.builder(
                    itemCount: imageList.length,
                    options: CarouselOptions(
                      enlargeCenterPage: true,
                      height: MediaQuery.of(context).size.height-220,
                      autoPlay: true,
                      autoPlayInterval: const Duration(seconds: 8),
                      reverse: false,
                      aspectRatio: 5.0,
                    ),
                    itemBuilder: (context, i, id){
                      //for onTap to redirect to another screen
                      return GestureDetector(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                          ),
                          //ClipRRect for image border radius
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: CachedNetworkImage(
                              imageUrl: globals.ip+imageList[i],
                              fit: BoxFit.contain,
                            ),

                          ),
                        ),
                        onTap: (){
                          var url = imageList[i];
                          print(url.toString());
                        },
                      );
                    },
                  ),
                ),

              ],
            ),
          ),

        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
  void connectSocketMain(String phone){
    globals.socket.on(phone, (data) {
      processSocketMyPhone(data);
      return null;
    });
  }
  void processSocketMyPhone(var data){
    var jso=json.decode(data);
    String type=jso['type'];
    if(!type.startsWith("not")) {
      if (type.contains('invited')) {
        setState(() {
          invitedMe=true;
        });
      }
      else if(type.contains("managers")){
        setState(() {
          managerMe=true;
        });
      }
      playLocal();
    }
  }
  playLocal() async {
    // await audioPlayer.play(alarmAudioPath, isLocal: true);
    await player.setAsset(alarmAudioPath);
    player.play();
  }

}

class MyCustomScrollBehavior extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => {
    PointerDeviceKind.touch,
    PointerDeviceKind.mouse,
  };
}


