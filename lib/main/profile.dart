// ignore_for_file: file_names


import 'dart:convert';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';
import 'package:persian_number_utility/src/extensions.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../datamap/OrgProfileMap.dart';
import '../datamap/UserProfileMap.dart';
import '../datamap/orgConnectionMap.dart';
import '../start/login_page.dart';
import '../widget/globalQuestion.dart';
import '../start/globals.dart' as global;
import 'package:http/http.dart' as http;


class profile extends StatefulWidget{
  bool isMe,organizer;
  String user_id;
  profile({required this.isMe,required this.user_id,required this.organizer});
  @override
  State<StatefulWidget> createState() => myProfile(isMe: isMe,user_id: user_id,organizer:organizer);

}

class myProfile extends State<profile> with TickerProviderStateMixin{

  bool isMe,organizer;
  String user_id;
  myProfile({required this.isMe,required this.user_id,required this.organizer});
  UserProfileMap user=UserProfileMap("", "", "", "", "", "", "", "", "", "", ""
      ,"", "", "", "", 0, 0, 0);
  String birthday="";

  int _genderValue=0; //Initial definition of radio button value
  int choice=0;
  late AnimationController controller;
  void radioButtonChanges(int? value) {
    if(value!=null) {
      setState(() {
        _genderValue = value;
        choice=value;
        user.gender=choice;
      });
    }
  }
  int witchPage=0;
  bool sendingImage=false;
  orgConnectionMap orgConnection=orgConnectionMap("", "", "", "", "", "", "", "", "", "");
  bool isOrgConEmpty=true;
  var _image;
  String imageAddress="",imgPath="";
  PageController page_controller=PageController();
   OrgProfileMap orgProfile=OrgProfileMap("", "",  "",  "",  "",  "",  "",  "",  "", 0);
   @override
  void initState(){
    super.initState();
    //orgProfile==OrgProfileMap(user_id, "",  "",  "",  "",  "",  "",  "",  "", 0);
    _getOwner(user_id);
    _getOrgProfile(user_id);
    controller =
        BottomSheet.createAnimationController(this);
    controller.duration = const Duration(milliseconds: 500);
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp)  {
        if(organizer) {
          page_controller.animateToPage(1, duration: const Duration(milliseconds: 20), curve: Curves.ease);
        }
    });

   }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("پروفایل",style: TextStyle(fontFamily: "estedad",fontSize: 14),),
        ),
        body:SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  width:MediaQuery.of(context).size.width-40,
                  child: Card(
                    shape:const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: ()async{
                              uploadImage2().then((value) async {
                                if(value!="") {
                                  setState(() {
                                    sendingImage = false;
                                    user.pic = value;

                                  });
                                  SharedPreferences prefs = await SharedPreferences.getInstance();
                                  prefs.setString("pic", user.pic);
                                  String my_user_id =await prefs.getString('user_id').toString();
                                  _sendName(my_user_id);
                                }
                              });
                            },
                            child:  sendingImage?const CircularProgressIndicator():
                            user.pic!=""?
                            CircleAvatar(
                              backgroundImage: NetworkImage(global.ip+user.pic),
                              radius: 30,
                            ): Icon(
                                CupertinoIcons.person_crop_circle,size: 60,
                                color: Colors.grey[700]),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Column(
                                children: [
                                  user.gender==0?Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: TextButton(
                                      child: Row(
                                        children: [
                                          SizedBox(
                                              child: Text(user.name,style: const TextStyle(fontSize: 14,fontFamily: "estedad"),)),
                                        ],
                                      ),
                                      onPressed: (){_writeName(context);},),
                                  ):Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: TextButton(
                                      child: Row(
                                        children: [
                                          SizedBox(
                                              child: Text(user.gender==1?"آقای "+user.name:"خانم "+user.name,style: const TextStyle(fontSize: 14,fontFamily: "estedad"),)),
                                        ],
                                      ),
                                      onPressed: (){_writeName(context);},),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: TextButton(child:
                                    Row(
                                      children: [
                                        SizedBox(
                                            child: Text(user.user_company==""?"؟":user.user_company,style: const TextStyle(fontSize: 13,fontFamily: "estedad"),)),
                                      ],
                                    ),
                                      onPressed: (){_writeSomthing(context);},),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Center(
                child:  Container(
                  margin: const EdgeInsets.all(10.0),
                  width:MediaQuery.of(context).size.width-40,
                  child: Card(
                    shape:const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          children: [
                            TextButton(onPressed: () async {
                              List<String> needInfo=[];
                              List<String> needInfoE=[];
                              needInfoE.add("job");
                              bool reload=await showDialog(
                                context: context,
                                builder: (_) => globalQuestion(needInfo: needInfo, needInfoE: needInfoE,user: user,),
                              );
                              if(reload){
                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                String my_user_id =await prefs.getString('user_id').toString();
                                _getOwner(my_user_id);
                              }

                            },
                                child: Row(
                                  children: [
                                    Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children:  [
                                        const Text("شغل:",style: TextStyle(fontFamily: "estedad",fontSize: 12),),
                                        Text(user.user_job,style: const TextStyle(fontFamily: "estedad",fontSize: 13,color: Colors.blue),),
                                      ],
                                    ),
                                  ],
                                )),
                            Container(
                              height: 1,
                              color: Colors.black45,
                              width: double.infinity,
                              margin: EdgeInsets.fromLTRB(15, 5, 15, 5),
                            ),
                            TextButton(onPressed: () async {
                              List<String> needInfo=[];
                              List<String> needInfoE=[];
                              needInfoE.add("edu");
                              bool reload=await showDialog(
                                context: context,
                                builder: (_) => globalQuestion(needInfo: needInfo, needInfoE: needInfoE,user: user,),
                              );
                              if(reload){
                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                String my_user_id =await prefs.getString('user_id').toString();
                                _getOwner(my_user_id);
                              }
                            },
                                child: Row(
                                  children: [
                                    Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children:  [
                                        const Text("تحصیلات:",style: TextStyle(fontFamily: "estedad",fontSize: 12),),
                                        Text(user.education,style: const TextStyle(fontFamily: "estedad",fontSize: 13,color: Colors.blue),),
                                      ],
                                    ),
                                  ],
                                )),
                            Container(
                              height: 1,
                              color: Colors.black45,
                              width: double.infinity,
                              margin: EdgeInsets.fromLTRB(15, 5, 15, 5),
                            ),
                            TextButton(onPressed: () async {
                              List<String> needInfo=[];
                              List<String> needInfoE=[];
                              needInfoE.add("city");
                              bool reload=await showDialog(
                                context: context,
                                builder: (_) => globalQuestion(needInfo: needInfo, needInfoE: needInfoE,user: user,),
                              );
                              if(reload){
                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                String my_user_id =await prefs.getString('user_id').toString();
                                _getOwner(my_user_id);
                              }
                            },
                                child: Row(
                                  children: [
                                    Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children:  [
                                        const Text("شهر محل زندگی:",style: TextStyle(fontFamily: "estedad",fontSize: 12),),
                                        Text(user.user_city,style: const TextStyle(fontFamily: "estedad",fontSize: 13,color: Colors.blue),),
                                      ],
                                    ),
                                  ],
                                )),
                            Container(
                              height: 1,
                              color: Colors.black45,
                              width: double.infinity,
                              margin: EdgeInsets.fromLTRB(15, 5, 15, 5),
                            ),
                            TextButton(onPressed: () async {
                              List<String> needInfo=[];
                              List<String> needInfoE=[];
                              needInfoE.add("birthday");
                              bool reload=await showDialog(
                                context: context,
                                builder: (_) => globalQuestion(needInfo: needInfo, needInfoE: needInfoE,user: user,),
                              );
                              if(reload){
                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                String my_user_id =await prefs.getString('user_id').toString();
                                _getOwner(my_user_id);
                              }
                            },
                                child: Row(
                                  children: [
                                    Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children:  [
                                        const Text("تاریخ تولد:",style: TextStyle(fontFamily: "estedad",fontSize: 12),),
                                        Text(birthday,style: const TextStyle(fontFamily: "estedad",fontSize: 13,color: Colors.blue),),
                                      ],
                                    ),
                                  ],
                                )),
                          ],
                        )
                    ),
                  ),
                ),
              ),
              Center(
                child:  Container(
                  margin: const EdgeInsets.all(10.0),
                  width:MediaQuery.of(context).size.width-40,
                  child: Card(
                    shape:const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          children: [
                            TextButton(onPressed: (){},
                                child: Row(
                                  children: [
                                    Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children:  [
                                        const Text("شماره همراه:",style: TextStyle(fontFamily: "estedad",fontSize: 12),),
                                        Text(user.phone,style: const TextStyle(fontFamily: "estedad",fontSize: 13,color: Colors.blue),),
                                      ],
                                    ),
                                  ],
                                )),
                            Container(
                              height: 1,
                              color: Colors.black45,
                              width: double.infinity,
                              margin: EdgeInsets.fromLTRB(15, 5, 15, 5),
                            ),
                            TextButton(onPressed: ()async {
                              List<String> needInfo=[];
                              List<String> needInfoE=[];
                              needInfoE.add("email");
                              bool reload=await showDialog(
                                context: context,
                                builder: (_) => globalQuestion(needInfo: needInfo, needInfoE: needInfoE,user: user,),
                              );
                              if(reload){
                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                String my_user_id =await prefs.getString('user_id').toString();
                                _getOwner(my_user_id);
                              }
                            },
                                child: Row(
                                  children: [
                                    Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children:  [
                                        const Text("ایمیل:",style: TextStyle(fontFamily: "estedad",fontSize: 12),),
                                        Text(user.email,style: const TextStyle(fontFamily: "estedad",fontSize: 13,color: Colors.blue),),
                                      ],
                                    ),
                                  ],
                                )),
                          ],
                        )
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child:   MaterialButton(onPressed: () async {

                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  prefs.setString("token", "");
                  prefs.setString("user_id", "");
                  prefs.setBool("isLogin", false);
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => LoginPage()));

                },child: Card(
                  color: Colors.red,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                        width: 180,
                        child: const Center(
                            child: Text("خروج از حساب",style: TextStyle(fontFamily: "estedad",fontSize: 12,color: Colors.white),))),
                  ),
                ),),

              ),
            ],
          ),
        )


      ),
    );
  }
  void _writeName(BuildContext context) {

    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        TextEditingController questionController=TextEditingController(text: "");
        return AlertDialog(

          title: const Center(child: Text('نام و نام خانوادگی جدید:',style: TextStyle(color: Colors.blueAccent,fontFamily: "estedad",fontSize: 13),)),
          content: Container(
            height: 190,
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: Directionality(
                textDirection: TextDirection.rtl,
                child:  Center(
                  child: Column(
                    children: [
                      /*Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: const Text("جنسیت",style: TextStyle(color: Colors.black87, fontSize: 13,fontFamily: "estedad"),),
                          ),
                          Radio(
                            value: 1,
                            groupValue: _genderValue,
                            onChanged: radioButtonChanges,
                          ),
                          const Text(
                            'مرد',
                            style:  TextStyle(
                                fontSize: 12.0,
                                fontFamily: "estedad",
                                color: Colors.black87
                            ),
                          ),
                          Radio(
                            value: 2,
                            groupValue: _genderValue,
                            onChanged: radioButtonChanges,
                          ),
                          const Text(
                            'زن',
                            style:  TextStyle(
                                fontSize: 12.0,
                                fontFamily: "estedad",
                                color: Colors.black87
                            ),
                          ),
                        ],
                      ),*/
                      SizedBox(
                        width: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8, vertical:4),
                          child: TextFormField(
                            autofocus: true,

                            controller: questionController,
                            decoration: const InputDecoration(
                              // ignore: prefer_const_constructors
                                border: OutlineInputBorder(),
                                labelText: 'نام و نام خانوادگی'
                            ),
                            style: const TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'estedad',
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: MaterialButton(
                          child: const SizedBox(
                            width: double.infinity,
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Center(
                                child: Text('ارسال',
                                    style: TextStyle(fontFamily: "estedad",fontSize: 15 ,fontWeight: FontWeight.bold)),
                              ),
                            ),
                          ),
                          onPressed: () async{
                            if(questionController.text.length>2) {
                              user.name=questionController.text;

                              SharedPreferences prefs = await SharedPreferences.getInstance();
                              String my_user_id =await prefs.getString('user_id').toString();
                              await _sendName(my_user_id);
                              Navigator.pop(context);
                            }
                            else{
                              Fluttertoast.showToast(
                                  msg: "لطفا نام خود را کامل وارد نمایید",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.red,
                                  webBgColor: '#F44336',
                                  textColor: Colors.white,
                                  fontSize: 16.0
                              );
                            }

                          },
                          color: Colors.blue,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
  void _writeSomthing(BuildContext context) {

    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        TextEditingController questionController=TextEditingController(text: "");
        return AlertDialog(
          title: const Center(child: Text('چند کلمه از خود بنویسید',style: TextStyle(color: Colors.blueAccent,fontFamily: "estedad",fontSize: 13),textDirection: TextDirection.rtl,)),
          content: Container(
            height: 190,
            child: Directionality(
              textDirection: TextDirection.rtl,
              child:  Center(
                child: Column(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical:4),
                        child: TextFormField(
                          autofocus: true,

                          controller: questionController,
                          decoration: const InputDecoration(
                            // ignore: prefer_const_constructors
                              border: OutlineInputBorder(),
                              labelText: 'بیوگرافی'
                          ),
                          maxLines: 1,
                          style: const TextStyle(
                              fontSize: 13.0,
                              fontFamily: 'estedad',
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: MaterialButton(
                        child: const SizedBox(
                          width: double.infinity,
                          child: Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Center(
                              child: Text('ثبت',
                                  style: TextStyle(fontFamily: "estedad",fontSize: 15 ,fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ),
                        onPressed: () async{
                          if(questionController.text.length>1) {
                            user.user_company=questionController.text;
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            String my_user_id =await prefs.getString('user_id').toString();
                            await _sendName(my_user_id);
                            Navigator.pop(context);
                          }
                          else{
                            Fluttertoast.showToast(
                                msg: "لطفا چیزی وارد نمایید",
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 3,
                                backgroundColor: Colors.red,
                                webBgColor: '#F44336',
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                          }

                        },
                        color: Colors.blue,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
  void _writeBio(BuildContext context,String ab) {

    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        TextEditingController questionController=TextEditingController(text:ab);
        return AlertDialog(

          title: const Center(child: Text('بیوگرافی:',style: TextStyle(color: Colors.blueAccent,fontFamily: "estedad",fontSize: 13,),textDirection: TextDirection.rtl,)),
          content: Container(
            height: 200,
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: Directionality(
                textDirection: TextDirection.rtl,
                child:  Center(
                  child: Column(
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8, vertical:4),
                          child: TextFormField(
                            autofocus: true,

                            controller: questionController,
                            decoration: const InputDecoration(
                              // ignore: prefer_const_constructors
                                border: OutlineInputBorder(),
                            ),
                            style: const TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'estedad',
                                fontWeight: FontWeight.bold
                            ),
                            maxLines: 7,
                            minLines: 3,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: MaterialButton(
                          child: const SizedBox(
                            width: double.infinity,
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Center(
                                child: Text('تایید',
                                    style: TextStyle(fontFamily: "estedad",color:Colors.white,fontSize: 15 ,fontWeight: FontWeight.bold)),
                              ),
                            ),
                          ),
                          onPressed: () async{
                            setState(() {
                              orgProfile.about=questionController.text;
                            });
                            _setOrgProfile(user_id);
                            Navigator.pop(context);
                          },
                          color: Colors.blue,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
  void _writeCompany(BuildContext context,String ab) {

    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        TextEditingController questionController=TextEditingController(text:ab);
        return AlertDialog(

          title: const Center(child: Text('نام شرکت یا موسسه:',style: TextStyle(color: Colors.blueAccent,fontFamily: "estedad",fontSize: 13),)),
          content: Container(
            height:180,
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: Directionality(
                textDirection: TextDirection.rtl,
                child:  Center(
                  child: Column(
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8, vertical:4),
                          child: TextFormField(
                            autofocus: true,

                            controller: questionController,
                            decoration: const InputDecoration(
                              // ignore: prefer_const_constructors
                                border: OutlineInputBorder(),
                            ),
                            style: const TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'estedad',
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: MaterialButton(
                          child: const SizedBox(
                            width: double.infinity,
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Center(
                                child: Text('تایید',
                                    style: TextStyle(fontFamily: "estedad",fontSize: 15 ,fontWeight: FontWeight.bold)),
                              ),
                            ),
                          ),
                          onPressed: () async{
                            setState(() {
                              orgProfile.company=questionController.text;
                            });
                            _setOrgProfile(user_id);
                            Navigator.pop(context);
                          },
                          color: Colors.blue,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
  Future<String> uploadImage2() async {
    String path="";
    final result = await FilePicker.platform.pickFiles(
      type: FileType.image,
      withData: false,
      withReadStream: true,
    );
    if (result != null) {
      final file = result.files.first;
      var uri = Uri.parse(global.ip + "/upload");
      Stream<List<int>>? ss = file.readStream;
      if (ss != null) {
        var stream = http.ByteStream(ss);
        var request = http.MultipartRequest('POST', uri);
        var multipartFile = http.MultipartFile(
            user.phone,
            stream,
            file.size,
            filename: file.name
        );
        request.files.add(multipartFile);

        final httpClient = http.Client();
        final response = await httpClient.send(request);

        if (response.statusCode != 200) {
          throw Exception('HTTP ${response.statusCode}');
        }
        final body = await response.stream.transform(
            utf8.decoder).join();

        print("body ="+body.toString());
        var data=json.decode(body);
        path=data['path'];
        return path;
      }
    }
    return path;
  }
  Future<bool> _sendName(String user_id) async {
    print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/changeName";
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    // String token=prefs.getString("access_token").toString();
    print("url ="+url);
    print("user_id ="+user_id);
    print("user_id ="+user_id);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'name': user.name,
        'user_id': user_id,
        'user_pic': user.pic,
        'user_birthday': user.user_birthday,
        'email': user.email,
        'gender': user.gender,
        'whats_up': user.whats_up,
        'user_pass': "",
        'user_company': user.user_company,
        'user_job': user.user_job,
        'user_city': user.user_city,
        'education': user.education,
      }),
    );
    print("response invite="+response.body.toString());
    if(response.statusCode == 200) {
      print(response.body);
      jsonResponse = json.decode(response.body);
      if(jsonResponse['ok']){
        setState(() {

        });
        return true;
      }else{
        Fluttertoast.showToast(
            msg: jsonResponse['msg'],
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    return false;
  }
Future<bool> _setOrgProfile(String user_id) async {
    print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/set/orgProfile";
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    // String token=prefs.getString("access_token").toString();
    print("url ="+url);
    print("user_id ="+user_id);
    print("user_id ="+user_id);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'user_id': user_id,
        'title': orgProfile.title,
        'company': orgProfile.company,
        'about': orgProfile.about,
        'pic': orgProfile.pic,
        'logo': orgProfile.logo,
        'more_info': orgProfile.more_info,
        'topic': orgProfile.topic,
        'connect_info': orgProfile.connect_info,
      }),
    );
    print("response invite="+response.body.toString());
    if(response.statusCode == 200) {
      print(response.body);
      jsonResponse = json.decode(response.body);
      if(jsonResponse['ok']){
        return true;
      }else{
        Fluttertoast.showToast(
            msg: jsonResponse['msg'],
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    return false;
  }

  Future<void> _getOwner(String user_id) async {

    var jsonResponse = null;
    String url = global.ip+"/api/getUserById";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'user_id': user_id,
      }),
    );
    if (response.statusCode == 200) {
      print("response code="+response.body);
      jsonResponse = json.decode(response.body);
      print("response js=" + jsonResponse.toString());
      if (jsonResponse != null) {
        bool ok = jsonResponse['ok'];
        if (ok) {
          var data = json.decode(jsonResponse['data']);
          print("data=" + data.toString());
          bool userActive=data['user_active'];
          String user_name=data['user_name'];
          String user_phone=data['user_phone'];
          String user_pic=data['user_pic']??"";
          String user_device=data['user_device']??"";
          String email=data['email']??"";
          String whats_up=data['whats_up']??"";
          String user_job=data['user_job']??"";
          String user_city=data['user_city']??"";
          String interest1=data['interest1']??"";
          String interest2=data['interest2']??"";
          String interest3=data['interest3']??"";
          String interest4=data['interest4']??"";
          String education=data['education']??"";
          String user_company=data['user_company']??"";
          int user_date=data['user_date']??0;
          int user_birthday=data['user_birthday']??0;
          int gender=data['gender'];
          user=UserProfileMap(user_id, user_phone, user_device, user_name, user_pic,
              email, whats_up, user_company, user_job, user_city, interest1,
              interest2, interest3, interest4, education, user_date, user_birthday, gender);
          if(user_birthday!=0) {
            try {
            var date =  DateTime.fromMillisecondsSinceEpoch(user.user_birthday * 1000);
            Jalali jl=Jalali.fromDateTime(date);
            birthday=jl.formatCompactDate();
          } on Exception catch (_) {
            print('no info');
          }
          }
          setState(() {


          });
        }
      }
      else {
        Fluttertoast.showToast(
            msg: jsonResponse['data'],
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#F44336',
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        print("response.statusCode is: ${response.statusCode}");
        print("The error message is: ${response.body}");
      }
      return;
    }
  }
  Future<void> _getOrgProfile(String user_id) async {

    var jsonResponse = null;
    String url = global.ip+"/api/getOrgProfile";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'user_id': user_id,
      }),
    );
    setState(() {
      orgProfile.user_id=user_id;
    });
    if (response.statusCode == 200) {
      print("response code=" + response.body);
      jsonResponse = json.decode(response.body);
      print("response js=" + jsonResponse.toString());
      bool ok = jsonResponse['ok'];
      if (ok && jsonResponse['data'].toString().length>5) {
        var data = json.decode(jsonResponse['data']);
          print("data=" + data.toString());
          String title = data['title'] ?? "";
          String company = data['company'] ?? "";
          String about = data['about'] ?? "";
          String pic = data['pic'] ?? "";
        print("pic=" + pic);
          String logo = data['logo'] ?? "";
          String more_info = data['more_info'] ?? "";
          String topic = data['topic'] ?? "";
          String connect_info = data['connect_info'] ?? "";
          if(connect_info.length>10){
            orgConnection=orgConnectionMap.decodeSettings(connect_info);
            if(orgConnection.telegram!="" || orgConnection.tel!=""||
                orgConnection.youtube!=""|| orgConnection.instagram!=""|| orgConnection.whatsUp!=""||
                orgConnection.web!=""|| orgConnection.email!=""|| orgConnection.other2!=""|| orgConnection.other1!=""|| orgConnection.other3!=""){
              isOrgConEmpty=false;
            }
          }
          int approved = data['approved'] ?? 0;
        print("approved=" + approved.toString());
          orgProfile = OrgProfileMap(
              user_id,
              title,
              company,
              about,
              pic,
              logo,
              more_info,
              topic,
              connect_info,
              approved);
          setState(() {

          });
      }
    }
      else {
        Fluttertoast.showToast(
            msg: jsonResponse['data'],
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#F44336',
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        print("response.statusCode is: ${response.statusCode}");
        print("The error message is: ${response.body}");
      }
      return;
    }

  Future<void> _showMenu(BuildContext context) async {



    orgConnectionMap? org=orgConnection;


 /*   org=await showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        isScrollControlled: true,

        transitionAnimationController: controller,
        builder: (BuildContext bc) {
          return bottom(orgConnect: orgConnection);// return a StatefulWidget widget
        });*/

    org=await showDialog<orgConnectionMap>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
            title:  const Center(child: Text('تلفن تماس و شبکه های اجتماعی',style: TextStyle(color: Colors.blueAccent,fontFamily: "Vazir",fontSize: 15),)),
            content:  bottom(orgConnect: orgConnection)
        );
      },
    );
    if(org!=null){
      String orgs=orgConnectionMap.encode(org);
      orgProfile.connect_info=orgs;
      _setOrgProfile(user_id);
      setState(() {
        orgConnection=org!;
        if(orgConnection.telegram!="" || orgConnection.tel!=""||
            orgConnection.youtube!=""|| orgConnection.instagram!=""|| orgConnection.whatsUp!=""||
            orgConnection.web!=""|| orgConnection.email!=""|| orgConnection.other2!=""|| orgConnection.other1!=""|| orgConnection.other3!=""){
          isOrgConEmpty=false;
        }
      });
    }
  }
}

class bottom extends StatefulWidget {
  orgConnectionMap orgConnect;

  bottom({required this.orgConnect});
  @override
  _bottomState createState() => _bottomState(orgConnect: orgConnect);
}

class _bottomState extends State<bottom> {

  orgConnectionMap orgConnect;
  _bottomState({required this.orgConnect});
  bool sending=false;

  @override
  void initState() {



    setState(() {
      telController.text=orgConnect.tel;
      watsController.text=orgConnect.whatsUp;
      telgController.text=orgConnect.telegram;
      instController.text=orgConnect.instagram;
      emaController.text=orgConnect.email;
      webController.text=orgConnect.web;
    });
    super.initState();
  }
  TextEditingController telController=TextEditingController();
  TextEditingController watsController=TextEditingController();
  TextEditingController telgController=TextEditingController();
  TextEditingController instController=TextEditingController();
  TextEditingController emaController=TextEditingController();
  TextEditingController webController=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical:4),
                child: TextFormField(
                  textDirection: TextDirection.ltr,
                  controller: telController,
                  decoration:  const InputDecoration(
                    // ignore: prefer_const_constructors
                    border: OutlineInputBorder(),
                    labelText: "تلفن تماس:",
                  ),
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  maxLines: 1,
                  style: const TextStyle(
                      fontSize: 13.0,
                      fontFamily: 'estedad',
                      color: Colors.blue
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical:4),
                child: TextFormField(
                  textDirection: TextDirection.ltr,
                  controller: watsController,
                  decoration:  const InputDecoration(
                    // ignore: prefer_const_constructors
                    border: OutlineInputBorder(),
                    labelText: "شماره واتساپ:",
                  ),
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  maxLines: 1,
                  style: const TextStyle(
                      fontSize: 13.0,
                      fontFamily: 'estedad',
                      color: Colors.blue
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical:4),
                child: TextFormField(
                  textDirection: TextDirection.ltr,
                  controller: telgController,
                  decoration:  const InputDecoration(
                    // ignore: prefer_const_constructors
                    border: OutlineInputBorder(),
                    labelText: "لینک تلگرام:",
                  ),
                  maxLines: 1,
                  style: const TextStyle(
                      fontSize: 12.0,
                      fontFamily: 'estedad',
                      color: Colors.blue
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical:4),
                child: TextFormField(
                  textDirection: TextDirection.ltr,
                  controller: instController,
                  decoration:  const InputDecoration(
                    // ignore: prefer_const_constructors
                    border: OutlineInputBorder(),
                    labelText: "لینک اینستاگرام:",
                  ),
                  maxLines: 1,
                  style: const TextStyle(
                      fontSize: 12.0,
                      fontFamily: 'estedad',
                      color: Colors.blue
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical:4),
                child: TextFormField(
                  textDirection: TextDirection.ltr,
                  controller: webController,
                  decoration:  const InputDecoration(
                    // ignore: prefer_const_constructors
                    border: OutlineInputBorder(),
                    labelText: "وبسایت:",
                  ),
                  maxLines: 1,
                  style: const TextStyle(
                      fontSize: 12.0,
                      fontFamily: 'estedad',
                      color: Colors.blue
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical:4),
                child: TextFormField(
                  textDirection: TextDirection.ltr,
                  controller: emaController,
                  decoration:  const InputDecoration(
                    // ignore: prefer_const_constructors
                    border: OutlineInputBorder(),
                    labelText: "ایمیل:",
                  ),
                  maxLines: 1,
                  keyboardType: TextInputType.emailAddress,
                  style: const TextStyle(
                      fontSize: 12.0,
                      fontFamily: 'estedad',
                      color: Colors.blue
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical:4),
                  child: MaterialButton(
                    color: Colors.blue,
                    onPressed: (){
                        orgConnect.tel=telController.text;
                        orgConnect.whatsUp=watsController.text;
                        orgConnect.telegram=telgController.text;
                        orgConnect.instagram=instController.text;
                        orgConnect.web=webController.text;
                        orgConnect.email=emaController.text;
                      Navigator.pop(context,orgConnect);
                    },
                    child: const Text("ثبت",style: TextStyle(color: Colors.white, fontSize: 13,fontFamily: "estedad"),),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
  Future<String> uploadImage2() async {
    String path="";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String phone = prefs.getString('phone').toString();
    final result = await FilePicker.platform.pickFiles(
      type: FileType.image,
      withData: false,
      withReadStream: true,
    );

    if (result != null) {
      final file = result.files.first;


      var uri = Uri.parse(global.ip + "/upload");
      Stream<List<int>>? ss = file.readStream;
      if (ss != null) {
        var stream = http.ByteStream(ss);

        var request = http.MultipartRequest('POST', uri);

        var multipartFile = http.MultipartFile(
            phone,
            stream,
            file.size,
            filename: file.name
        );

        request.files.add(multipartFile);

        final httpClient = http.Client();
        final response = await httpClient.send(request);

        if (response.statusCode != 200) {
          throw Exception('HTTP ${response.statusCode}');
        }

        final body = await response.stream.transform(
            utf8.decoder).join();

        print("body ="+body.toString());
        var data=json.decode(body);
        path=data['path'];
        return path;
      }
    }
    return path;
  }
}