library my_prj.globalQuestion;

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';

import 'package:persian_number_utility/src/extensions.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../datamap/UserProfileMap.dart';
import '../start/globals.dart' as global;
import 'package:http/http.dart' as http;
import 'package:numberpicker/numberpicker.dart';
import 'package:email_validator/email_validator.dart';

class globalQuestion extends StatefulWidget {
  List<String> needInfo;
  List<String> needInfoE;
  UserProfileMap user;
  globalQuestion({Key? key,required this.needInfo,required this.needInfoE,required this.user,});
  @override
  _globalQuestionState createState() => _globalQuestionState(needInfo,needInfoE,user);
}

class _globalQuestionState extends State<globalQuestion> with SingleTickerProviderStateMixin{
  List<String> needInfo;
  List<String> needInfoE;
  UserProfileMap user;
  _globalQuestionState(this.needInfo,this.needInfoE,this.user){
    emailController.addListener(() {
      setState(() {
        canGoNext=EmailValidator.validate(emailController.text);
        if(canGoNext)user.email=emailController.text;
      });

    });
  }
  //if(bbirthDay && user_birthday==0) { needInfo.add(" تاریخ تولد ");needInfoE.add("birthday");}
  //             if(bemail && email==""){needInfo.add(" ایمیل ");needInfoE.add("email");}
  //             if(bcity && user_city==""){needInfo.add(" شهر محل زندگی ");needInfoE.add("city");}
  //             if(beducation && education==""){needInfo.add(" تحصیلات ") ;needInfoE.add("edu");}
  //             if(bjob && user_job==""){needInfo.add(" شغل ") ;needInfoE.add("job");}
  //             if(bgender
  bool sending=false,birthDay=false,email=false,city=false,edu=false,
      job=false,gender=false;
  String citySelect = '';
  String stateSelect = '';
  String selectEdu = '';
  String selectJob = '';
  bool emailValid=false;
  TextEditingController emailController = TextEditingController(text: "");
  List <String> stateItems =[];
  List<List<String>> cityItems =[];
  List<String> cityStateItems =[];
  List<String> eduList =educationList;
  List<String> jobsList =jobList;
  String logoAddress="";
  bool sendingImage=false;
  String user_id="";
  int witch=0;
  int year=1375;
  int month=5;
  int day=5;
  int maxDay=31;
  bool canGoNext=false;
  check()async{

      String need=needInfoE[witch];
      birthDay=false;email=false;city=false;edu=false;job=false;gender=false;
        if(need == "birthday")birthDay=true;
        else if(need == "email")email=true;
        else if(need == "edu")edu=true;
        else if(need == "job")job=true;
        else if(need == "gender")gender=true;
        else if(need == "city")city=true;

      canGoNext=false;
      selectEdu=eduList[0];
      selectJob=jobsList[0];

      setState(() {

      });

  }
 //late AnimationController controller;
 // late Animation<double> scaleAnimation;
  int _genderValue=0; //Initial definition of radio button value
  int choice=0;

  void radioButtonChanges(int? value) {
    if(value!=null) {
      setState(() {
        _genderValue = value;
        choice=value;
        canGoNext=true;
        user.gender=choice;
      });
    }
  }

  @override
  void initState() {
    super.initState();
     _getIranState();
  /*  controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticIn);

    controller.addListener(() {
      setState(() {});
    });
    controller.forward();*/
    check();
  }
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          margin: EdgeInsets.all(20.0),
          padding: EdgeInsets.all(15.0),
          height: birthDay?300:200.0,
          decoration: ShapeDecoration(
            color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0))),
          child: Directionality(
            textDirection: TextDirection.rtl,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if(city)Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [  Row(
                    textDirection: TextDirection.rtl,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("استان:",style: TextStyle( fontSize: 13,fontFamily: "estedad"),),
                      Container(
                        margin: const EdgeInsets.only(right: 20),
                        child: DropdownButton<String>(
                          value:stateSelect ,
                          icon: const Icon(CupertinoIcons.arrowtriangle_down_fill),
                          iconSize: 12,
                          elevation: 8,
                          style: const TextStyle(color: Colors.blue, fontSize: 13,fontFamily: "estedad"),
                          underline: Container(
                            height: 2,
                            color: Colors.blue,
                          ),
                          onChanged: (String? data) {
                            if(data!=null) {
                              int i=0;
                              for(;i<stateItems.length;i++){
                                if(data==stateItems[i]){
                                  break;
                                }
                              }
                              setState(() {
                                stateSelect = data;
                                cityStateItems=cityItems[i];
                                citySelect=cityStateItems[0];
                                user.user_city=citySelect;
                              });
                            }
                          },
                          items: stateItems.map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ],
                  ),
                    Row(
                      textDirection: TextDirection.rtl,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text("شهر:",style: TextStyle( fontSize: 13,fontFamily: "estedad"),),
                        if(cityStateItems.isNotEmpty) Container(
                          margin: const EdgeInsets.only(right: 20),
                          child: DropdownButton<String>(
                            value:citySelect ,
                            icon: const Icon(CupertinoIcons.arrowtriangle_down_fill),
                            iconSize: 12,
                            elevation: 8,
                            style: const TextStyle(color: Colors.blue, fontSize: 13,fontFamily: "estedad"),
                            underline: Container(
                              height: 2,
                              color: Colors.blue,
                            ),
                            onChanged: (String? data) {
                              if(data!=null) {
                                setState(() {
                                  citySelect = data;
                                  canGoNext=true;
                                  user.user_city=citySelect;
                                });
                              }
                            },
                            items: cityStateItems.map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),],
                ),
                if(job)Row(
                  textDirection: TextDirection.rtl,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("شغل:",style: TextStyle( fontSize: 13,color:Colors.black87,fontFamily: "estedad"),),
                    Container(
                      margin: const EdgeInsets.only(right: 20),
                      child: DropdownButton<String>(
                        value:selectJob ,
                        icon: const Icon(CupertinoIcons.arrowtriangle_down_fill),
                        iconSize: 12,
                        elevation: 8,
                        style: const TextStyle(color: Colors.blue, fontSize: 13,fontFamily: "estedad"),
                        underline: Container(
                          height: 2,
                          color: Colors.blue,
                        ),
                        onChanged: (String? data) {
                          if(data!=null) {
                            setState(() {
                              selectJob = data;
                              canGoNext=true;
                              user.user_job=selectJob;
                            });
                          }
                        },
                        items: jobsList.map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
                if(edu)Row(
                  textDirection: TextDirection.rtl,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("تحصیلات:",style: TextStyle( fontSize: 13,color:Colors.black87,fontFamily: "estedad"),),
                    Container(
                      margin: const EdgeInsets.only(right: 20),
                      child: DropdownButton<String>(
                        value:selectEdu ,
                        icon: const Icon(CupertinoIcons.arrowtriangle_down_fill),
                        iconSize: 12,
                        elevation: 8,
                        style: const TextStyle(color: Colors.blue, fontSize: 13,fontFamily: "estedad"),
                        underline: Container(
                          height: 2,
                          color: Colors.blue,
                        ),
                        onChanged: (String? data) {
                          if(data!=null) {
                            setState(() {
                              selectEdu = data;
                              canGoNext=true;
                              user.education=selectEdu;
                            });
                          }
                        },
                        items: eduList.map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
                if(gender)Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text("جنسیت",style: TextStyle(color: Colors.black87, fontSize: 13,fontFamily: "estedad"),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                       Radio(
                        value: 1,
                         groupValue: _genderValue,
                         onChanged: radioButtonChanges,
                      ),
                      const Text(
                        'مرد',
                        style:  TextStyle(
                          fontSize: 12.0,
                          fontFamily: "estedad",
                          color: Colors.black87
                        ),
                      ),
                      Radio(
                        value: 2,
                        groupValue: _genderValue,
                        onChanged: radioButtonChanges,
                      ),
                      const Text(
                        'زن',
                        style:  TextStyle(
                          fontSize: 12.0,
                          fontFamily: "estedad",
                          color: Colors.black87
                        ),
                      ),
                  ],
                ),
                ]),
                if(birthDay)Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text("تاریخ تولد",style: TextStyle(color: Colors.black87, fontSize: 13,fontFamily: "estedad"),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      NumberPicker(
                        value: year,
                        minValue: 1300,
                        maxValue: 1401,
                        step: 1,
                        haptics: true,
                        onChanged: (value) => setState(() {
                          year = value;
                          canGoNext=true;
                        }),
                      ),
                      const Text(
                        '/',
                      ),
                      NumberPicker(
                        value: month,
                        minValue: 1,
                        maxValue: 12,
                        step: 1,
                        haptics: true,
                        onChanged: (value) => setState(() {
                          month = value;
                          if(month<7) {
                            maxDay=31;
                          } else if(month<12) {
                            if(day>30)day=30;
                            maxDay=30;
                          } else {
                            if(day>29)day=29;
                            maxDay=29;
                          }
                        }),
                      ),
                      const Text(
                        '/',
                      ),
                      NumberPicker(
                        value: day,
                        minValue: 1,
                        maxValue: maxDay,
                        step: 1,
                        haptics: true,
                        onChanged: (value) => setState(() => day = value),
                      ),
                  ],
                ),
                ]),
                if(email)  SizedBox(
                  width: 220,
                  child: TextFormField(
                    autofocus: true,
                    controller: emailController,
                    textDirection: TextDirection.ltr,
                    decoration:   const InputDecoration(
                      // ignore: prefer_const_constructors
                        border: OutlineInputBorder(),
                        labelText:'ایمیل',
                      hintText: "ایمیل همراه با @ است"
                    ),
                    style: const TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                const Spacer(),
                sending?const LinearProgressIndicator():MaterialButton(
                  color: canGoNext?Colors.blue:Colors.black26,
                  onPressed: (){
                    setState(() async {
                      if(canGoNext) {
                        if(birthDay){
                          Jalali ja=Jalali.now();
                          ja=ja.withYear(year);
                          ja=ja.withMonth(month);
                          ja=ja.withDay(day);
                          user.user_birthday=ja.toDateTime().millisecondsSinceEpoch~/1000;
                        }
                        if (witch + 1 < needInfoE.length) {
                          witch++;
                          check();
                        }
                        else {
                          setState(() {
                            sending=true;
                          });
                           SharedPreferences prefs = await SharedPreferences.getInstance();
                            user_id = prefs.getString('user_id').toString();
                          bool ret=await _sendName(user_id);
                          setState(() {
                            sending=false;
                          });
                          if(ret)
                          Navigator.pop(context,true);
                        }
                      }
                    });
                  },
                  child:  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text((witch+1)<needInfoE.length?"بعدی":"ثبت",style: TextStyle(color: Colors.white, fontSize: 13,fontFamily: "estedad"),),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Future<bool> _getIranState() async {

    var jsonResponse = null;

    String url = global.ip+"/api/getIranState";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      //print("response code="+response.body);
      jsonResponse = json.decode(response.body);
      // print("response js=" + jsonResponse.toString());
      bool ok = jsonResponse['ok'];
      if (ok) {
        var states=jsonResponse['data'];


        // print("key=" + states.toString());
        var res=json.decode(states);
        res.forEach((key, value){
          stateItems.add(key);
          List<String> cityList=[];
          for(String city in value){
            cityList.add(city);
          }
          cityItems.add(cityList);
        });
        setState(() {
          stateSelect=stateItems[0];
        });
        return true;
      }
    }

    return false;
  }



  Future<bool> _sendName(String user_id) async {
    print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/changeName";
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    // String token=prefs.getString("access_token").toString();
    print("url ="+url);
    print("user_id ="+user_id);
    print("user_id ="+user_id);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'name': user.name,
        'user_id': user_id,
        'user_pic': user.pic,
        'user_birthday': user.user_birthday,
        'email': user.email,
        'gender': user.gender,
        'whats_up': user.whats_up,
        'user_pass': "",
        'user_company': user.user_company,
        'user_job': user.user_job,
        'user_city': user.user_city,
        'education': user.education,
      }),
    );
    print("response invite="+response.body.toString());
    if(response.statusCode == 200) {
      print(response.body);
      jsonResponse = json.decode(response.body);
        if(jsonResponse['ok']){
          return true;
        }else{
          Fluttertoast.showToast(
              msg: jsonResponse['msg'],
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 3,
              backgroundColor: Colors.red,
              webBgColor: '#F44336',
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    return false;
  }
}

List<String> educationList=[
  'دیپلم و قبل',
  'کاردانی',
  'کارشناسی',
  'کارشناسی ارشد',
  'دکترا',
  'فوق دکترا',
];
List<String> jobList=[
  'کارمند اداری',
  'مربی ورزشی',
  'معلم',
  'دانشجو',
  'مهندس عمران',
  'سئوکار',
  'برنامه نویس',
  'طراح وب',
  'روانشناس',
  'پیش خدمت',
  'خلبان',
  'مهماندار هواپیما',
  'بازیگر',
  'معمار',
  'بازاریاب',
  'کارشناس فروش',
  'حسابدار',
  'مشاور املاک',
  'منشی',
  'مسئول تبلیغات',
  'مشاور مالی',
  'کارگزار بیمه',
  'وکیل',
  'مدیر فروش',
  'پلیس',
  'ناشر کتاب',
  'نویسنده',
  'ویراستار',
  'پزشک',
  'پرستار',
  'دامپزشک',
  'آزاد',
  'بیکار',
];

List <String> spinnerItems1 = [
  'تلفن',
  'واتساپ',
  'تلگرام',
  'اینستاگرام',
  'وبسایت',
  'ایمیل',
  'یوتیوب',
  'کد دعوت',
  'لینک',
] ;
List <String> spinnerItems2 = [
  'درباره برگزارکننده',//0
  'شرکت ، موسسه یا شغل',//1
  'سخنران',//2
  'برگزارکننده',//3
  'اطلاعاتی مانند تحصیلات',//4
];

