// ignore_for_file: file_names

import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../widget/globalQuestion.dart' as globalQ;
import '../start/globals.dart' as global;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class SelectTopics extends StatefulWidget {


  @override
  State<SelectTopics> createState() => _SelectTopicsState();
}


class _SelectTopicsState extends State<SelectTopics> {

  String selTop="";
  String user_id="";
  void check()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
   // phone = prefs.getString('phone').toString();
    user_id = prefs.getString('user_id').toString();
     topics=await _getTopics();
     setState(() {

     });
  }
  @override
  void initState() {
    check();
    super.initState();
  }
  bool sending=false;
  List<String> topics=[];

  @override
  Widget build(BuildContext context) {
    return
        WillPopScope(
          onWillPop: () async{

            return false;
          },
          child: Directionality(
              textDirection: TextDirection.rtl,
              child:Scaffold(
                appBar: AppBar(title: const Text("انتخاب موضوعات مورد علاقه",style:  TextStyle(color: Colors.white,
                    fontSize:13,fontFamily: "estedad"),),
                  leading: Container(),
                ),
                  body: Column(
                    children: [
                      if(selTop.isEmpty)const Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Text("حداقل یک موضوع را انتخاب کنید",style: TextStyle(color:Colors.black87,fontFamily: "estedad",fontSize: 13),),
                      ),
                     if(topics.isNotEmpty) Wrap(
                       // This next line does the trick.
                         textDirection: TextDirection.rtl,
                         children:
                         List.generate(topics.length, (int position){
                           return listMaker(context,topics[position],position);
                         })
                     ),
                      if(selTop.isNotEmpty)Padding(
                        padding: const EdgeInsets.only(top: 100),
                        child: MaterialButton(
                          color: Colors.blue,
                          onPressed: (){
                            Navigator.pop(context,selTop);
                          },
                          child: Container(
                                padding: const EdgeInsets.fromLTRB(45, 15, 45, 15),
                              child: const Text("تایید",style: TextStyle(fontFamily: "Vazir",color: Colors.white,fontWeight: FontWeight.bold),)),
                        ),
                      )
                    ],
                  )
              )),
        );
  }

  Widget listMaker(BuildContext context,String tx,int pos) {

      bool isSelect=false;
      if(selTop.contains("A"+pos.toString().padLeft(2, '0')))isSelect=true;
      return Container(
        margin: EdgeInsets.all(5),
        child: Card(
            shape:const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            elevation: 4,
            color: isSelect?Colors.green:Colors.white,
            child: InkWell(
              onTap: () {
                String sel="A"+pos.toString().padLeft(2, '0');
                print("sel="+sel);
                  if(isSelect){
                    selTop=selTop.replaceAll(sel, "");
                  }else {
                    selTop+=sel;
                  }
                  print(selTop);
                  setState(() {

                  });
              },
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(tx,style: TextStyle(color:isSelect?Colors.white:Colors.black87,fontFamily: "estedad",fontSize: 14,fontWeight: FontWeight.bold),),
              ),
            )),
      );
  }

  Future<List<String>> _getTopics() async {

    var jsonResponse = null;

    String url = global.ip+"/api/getTopics";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      print("response code="+response.body);
      jsonResponse = json.decode(response.body);
      print("response js=" + jsonResponse.toString());
      bool ok = jsonResponse['ok'];
      if (ok) {
        var tops=json.decode(jsonResponse['data']);
        for(var top in tops){
          topics.add(top['f_title']);
        }
      }
    }
    return topics;
  }

}