library my_prj.globals;
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:persian_number_utility/src/extensions.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;


bool isLoggedIn = false;
//String ip="http://192.168.1.103:4165";
//String ip="http://192.168.1.103:4165";
//String ip="http://192.168.0.33:8000";


//String ip="http://2.180.17.36:8000";
String ip="https://jooyaap.com";


late  IO.Socket socket = IO.io(ip,
      IO.OptionBuilder()
        .setTransports(['websocket']) // for Flutter or Dart VM
        .setExtraHeaders({'foo': 'bar'}) // optional
        .build());
String minImage(String path){
  if(path.length<5)return "";
  String lastPath="";
  int lastPoint=path.lastIndexOf(".");
  String firstSlice=path.substring(0,lastPoint);
  String secondSlice=path.substring(lastPoint);
  lastPath=firstSlice+"min"+secondSlice;
  return lastPath;
}
List<MaterialColor> colors=[Colors.blue,Colors.orange,Colors.red,Colors.green,
  Colors.purple,Colors.indigo,Colors.blueGrey,Colors.pink,Colors.yellow,
  Colors.lightGreen,Colors.brown,Colors.lightBlue,Colors.amber,Colors.lime,
  Colors.cyan,
    ];
String nameColor(Color color){
  String name="blue";
  if(color==Colors.amber)name="amber";
  if(color==Colors.red)name="red";
  if(color==Colors.amber)name="amber";
  if(color==Colors.amber)name="amber";
  if(color==Colors.amber)name="amber";
  return name;
}
String checkPhone(String number){
  String phone="";
  if (number.isNumeric() && number.length>10 && (number.startsWith("09") ||number.startsWith("98")
      ||number.startsWith("+98") || number.startsWith("0098") &&number.length<14 )) {
  number=number.replaceAll("+98", "0");
  number=number.replaceAll("0098", "0");
  if(number.startsWith("98")){
    number=number.substring(2);
  }
  phone=number.toEnglishDigit();
  }
  return phone;
}
List <String> spinnerItems1 = [
  'تلفن',
  'واتساپ',
  'تلگرام',
  'اینستاگرام',
  'وبسایت',
  'ایمیل',
  'یوتیوب',
  'کد دعوت',
  'لینک',
] ;
List <String> spinnerItemsProfile = [
  'تلفن',
  'واتساپ',
  'تلگرام',
  'اینستاگرام',
  'وبسایت',
  'ایمیل',
  'یوتیوب',
] ;
List <String> spinnerItems2 = [
  'درباره برگزارکننده',//0
  'شرکت ، موسسه یا شغل',//1
  'سخنران',//2
  'برگزارکننده',//3
  'اطلاعاتی مانند تحصیلات',//4
];