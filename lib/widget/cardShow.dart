library my_prj.cardShow;
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:persian_number_utility/src/extensions.dart';
import '../datamap/preMap.dart';
import '../datamap/SettingsMap.dart';
import '../start/globals.dart' as global;


Widget cardMake(PreMap preMap,bool fromOwner,bool invitedShow,BuildContext context) {
  var date = DateTime.fromMillisecondsSinceEpoch(preMap.time * 1000);
  String perDate=date.toPersianDateStr()+" ساعت: "+date.hour.toString()+":"+date.minute.toString();
  // print("owner xxxxxxxxx ="+preMap.owner+" user"+ user_id);
  String logoAddress="";
  if(preMap.logo!="")logoAddress=global.ip+global.minImage(preMap.logo);
  Color _titleColor=Colors.blue;
  Color _inviteColor=Colors.black87;
  Color _timeColor=Colors.black87;
  String backAddress="";
  String setting=preMap.settings;
  bool cropLogo=true,cover=true;
  int model=0;
  if(setting!=""){
    SettingsMap settingsMap=SettingsMap.decodeSettings(setting);
    _titleColor=Color(settingsMap.titleColor);
    _inviteColor=Color(settingsMap.inviteColor);
    _timeColor=Color(settingsMap.dataColor);
    backAddress=settingsMap.backImage;
    cropLogo=settingsMap.cropLogo;
    cover=settingsMap.cover;
    model=settingsMap.model;
  }
  String title=preMap.title;
  String inviteText=preMap.inviteText;

  return SizedBox(
    width: 350,
    height: 200,
    child: Stack(
      fit: StackFit.expand,
      children: [
        Positioned.fill(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: const EdgeInsets.all(10),
                child: SizedBox(
                  width: 350,
                  height: 170,
                  child: Stack(
                    children: [
                      CachedNetworkImage(
                        imageUrl: global.ip+backAddress,
                        width: 350,
                        height: 170,
                        fit: BoxFit.cover,
                        errorWidget: (context, url, error) =>  Container(
                          width: 250,
                          height: 170,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                                  Color(0xFFFFFFFF),
                                  Color(0xFFFFFFFF),
                                ],
                                begin: FractionalOffset(0.0, 0.0),
                                end: FractionalOffset(1.0, 0.0),
                                stops: [0.0, 1.0],
                                tileMode: TileMode.clamp),
                          ),
                        ),
                      ),
                      Directionality(
                        textDirection: TextDirection.rtl,
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Center(
                                    child:
                                    AutoSizeText(
                                      title.toString().toPersianDigit(),
                                      style: TextStyle(color: _titleColor,fontFamily: "estedad",fontSize: 14,fontWeight: FontWeight.bold),
                                      minFontSize: 9,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    )
                                ),
                                Center(
                                  child:
                                  Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child:
                                      AutoSizeText(
                                        inviteText.toString().toPersianDigit(),
                                        style:  TextStyle(color: _inviteColor,fontFamily: "estedad",fontSize: 14),
                                        minFontSize: 8,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      )
                                  )  ,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(perDate.toPersianDigit(),style: TextStyle(color: _timeColor,fontFamily: "estedad",fontSize: 13),),
                                ),

                              ]),
                        ),
                      ),
                    ],
                  ),
                )),
          ),
        ),
        if(logoAddress!="")  Positioned.fill(
          child: Align(
            alignment: Alignment.topRight,
            child:  Padding(
              padding:  const EdgeInsets.only(left: 15,right: 15),
              child:  CachedNetworkImage(
                imageUrl: logoAddress,
                height: 50,
                width: 50,
                imageBuilder: (context, imageProvider) => Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: cropLogo?BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider,
                        fit:  cover?BoxFit.fill:BoxFit.contain
                    ),
                  ): BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider, fit:  cover?BoxFit.fill:BoxFit.contain)
                  ),
                ),
                placeholder: (context, url) => const CircularProgressIndicator(),
                errorWidget: (context, url, error) {
                  print("error="+error.toString());
                  return Image.asset("image/rad_survey_back.png", width: 50.0,height: 50.0,);
                },
              ),
            ) ,
          ),
        ),
        if(!invitedShow && !fromOwner)const Positioned.fill(child: Align(
          alignment: Alignment.bottomRight,
          child: Text("همکاری",style:TextStyle(fontSize: 10,color: Colors.green,fontWeight: FontWeight.bold),),
        )),
      ],
    ),
  );
}
Widget cardMake2(PreMap preMap,bool fromOwner,bool invitedShow,BuildContext context) {
  var date = DateTime.fromMillisecondsSinceEpoch(preMap.time * 1000);
  String perDate=date.toPersianDateStr()+" ساعت: "+date.hour.toString()+":"+date.minute.toString();
  // print("owner xxxxxxxxx ="+preMap.owner+" user"+ user_id);
  String logoAddress="";
  if(preMap.logo!="")logoAddress=global.ip+global.minImage(preMap.logo);
  Color _titleColor=Colors.blue;
  Color _inviteColor=Colors.black87;
  Color _timeColor=Colors.black87;
  String backAddress="";
  String setting=preMap.settings;
  bool cropLogo=true,cover=true;
  int model=0;
  if(setting!=""){
    SettingsMap settingsMap=SettingsMap.decodeSettings(setting);
    _titleColor=Color(settingsMap.titleColor);
    _inviteColor=Color(settingsMap.inviteColor);
    _timeColor=Color(settingsMap.dataColor);
    backAddress=settingsMap.backImage;
    cropLogo=settingsMap.cropLogo;
    cover=settingsMap.cover;
    model=settingsMap.model;
  }
  String title=preMap.title;
  String inviteText=preMap.inviteText;
  return SizedBox(
    width: 350,
    height: 230,
    child: Stack(
      children: [
        Positioned.fill(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: const EdgeInsets.all(10),
                child: SizedBox(
                  width: 350,
                  height: 230,
                  child: Column(
                    children: [
                      CachedNetworkImage(
                        imageUrl: global.ip+backAddress,
                        width: 350,
                        height: 110,
                        fit: BoxFit.cover,
                        errorWidget: (context, url, error) =>  Container(
                          width: 250,
                          height: 110,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                                  Color(0xFFFFFFFF),
                                  Color(0xFFFFFFFF),
                                ],
                                begin: FractionalOffset(0.0, 0.0),
                                end: FractionalOffset(1.0, 0.0),
                                stops: [0.0, 1.0],
                                tileMode: TileMode.clamp),
                          ),
                        ),
                      ),
                      Directionality(
                        textDirection: TextDirection.rtl,
                        child: Padding(
                          padding: const EdgeInsets.all(2),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Center(
                                    child:
                                    AutoSizeText(
                                      title.toString().toPersianDigit(),
                                      style: TextStyle(color: _titleColor,fontFamily: "estedad",fontSize: 14,fontWeight: FontWeight.bold),
                                      minFontSize: 9,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    )
                                ),
                                Center(
                                  child:
                                  Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child:
                                      AutoSizeText(
                                        inviteText.toString().toPersianDigit(),
                                        style:  TextStyle(color: _inviteColor,fontFamily: "estedad",fontSize: 14),
                                        minFontSize: 8,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      )
                                  )  ,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Text(perDate.toPersianDigit(),style: TextStyle(color: _timeColor,fontFamily: "estedad",fontSize: 13),),
                                ),

                              ]),
                        ),
                      ),
                    ],
                  ),
                )),
          ),
        ),
        if(logoAddress!="")  Positioned.fill(
          child: Align(
            alignment: Alignment.topRight,
            child:  Padding(
              padding:  const EdgeInsets.only(left: 15,right: 15),
              child:  CachedNetworkImage(
                imageUrl: logoAddress,
                height: 50,
                width: 50,
                imageBuilder: (context, imageProvider) => Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: cropLogo?BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider,
                        fit:  cover?BoxFit.fill:BoxFit.contain
                    ),
                  ): BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider, fit:  cover?BoxFit.fill:BoxFit.contain)
                  ),
                ),
                placeholder: (context, url) => const CircularProgressIndicator(),
                errorWidget: (context, url, error) {
                  print("error="+error.toString());
                  return Image.asset("image/rad_survey_back.png", width: 50.0,height: 50.0,);
                },
              ),
            ) ,
          ),
        ),
        if(!invitedShow && !fromOwner)const Positioned.fill(child: Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: EdgeInsets.only(right: 15),
            child: Text("همکاری",style:TextStyle(fontSize: 11,color: Colors.green,fontWeight: FontWeight.bold),),
          ),
        )),
      ],
    ),
  );
}
Widget cardMakerX(PreMap preMap,bool fromOwner,bool invitedShow,BuildContext context){
  String setting=preMap.settings;
  int model=0;
  if(setting!=""){
    SettingsMap settingsMap=SettingsMap.decodeSettings(setting);
    model=settingsMap.model;
  }


  if(model==0){
    return cardMake(preMap,fromOwner,invitedShow,context,);
  }
  else {
    return cardMake2(preMap,fromOwner,invitedShow, context);
  }


}
Widget cardMaker(String backAddress,String title,String logoAddress,bool cropLogo,
    Color _titleColor,Color _inviteColor,Color _timeColor,bool cover,
    String inviteText,int time,int model,bool isActive,String textActive,bool showAdmin,BuildContext context) {
    debugPrint("model=="+model.toString());

    SettingsMap settingsMap = SettingsMap(
        _titleColor.value,
        _inviteColor.value,
        _timeColor.value,
        Colors.blue.value,
        backAddress,
        "",
        cropLogo,cover,model);
    String settings = SettingsMap.encode(settingsMap);
    PreMap temp=PreMap("", time, 0, title, "", "", logoAddress, 0, "new", inviteText,
        settings, "", "", "", "", "", "", "", false, false);

    if(model==0){
      return cardMake(temp,false,true, context);
    }
    else {
      return cardMake2(temp,false,true, context);
    }

}








