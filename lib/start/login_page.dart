import 'dart:convert';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import '../main/HomeScreen.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import  'package:persian_number_utility/persian_number_utility.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:math' as math;
import 'globals.dart' as globals;
import 'dart:io';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter_countdown_timer/index.dart';


void login_page() {
  runApp(LoginPage());
}
enum ImageSourceType { gallery, camera,nothing }
class LoginPage extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ورود',
      theme: ThemeData(
        brightness: Brightness.light,
        /* light theme settings */
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        /* dark theme settings */
      ),
      themeMode: ThemeMode.system,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin{

  final TextEditingController _phoneControll = TextEditingController(text: "09");
  final TextEditingController _codeControll = TextEditingController(text:"");
  final TextEditingController _nameControll = TextEditingController(text:"");
  late AnimationController _controller;
  FocusNode nodeFamily = FocusNode();
  FocusNode nodeRef= FocusNode();
  int val = -1;

  String _phone = '';
  bool visibilityPhone = true;
  bool _show_gif=false;
  bool _exist=false;
  bool _get_name=false;
  _MyHomePageState(){
    _phoneControll.addListener(_phoneListen);
    _codeControll.addListener(_codeListen);
  }
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};

String fire_token="",platform="",device="",brand="",more_info="";
  bool canResend=false;
  int endTime=DateTime.now().millisecondsSinceEpoch + 1000 * 60;
  late AnimationController controller;
  int _genderValue=0; //Initial definition of radio button value
  String interest1="";
  void radioButtonChanges(int? value) {
    if(value!=null) {
      setState(() {
        _genderValue = value;
      });
    }
  }
  @override
  void initState() {
    super.initState();
    initPlatformState();
    _controller = AnimationController(vsync: this, duration: const Duration(seconds: 2))..repeat();
    controller =BottomSheet.createAnimationController(this);
    controller.duration = const Duration(milliseconds: 500);
   /* if(!kIsWeb) {
      FirebaseMessaging.instance.getToken().then((value) {
        fire_token=value ?? "";
        setState(() {

        });
        print("my token is= " + value.toString());
      });
    }*/

  }

  Future<void> initPlatformState() async {
    try {
      if (kIsWeb) {
        _readWebBrowserInfo(await deviceInfoPlugin.webBrowserInfo);
      } else {
        if (Platform.isAndroid) {
          _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
        } else if (Platform.isIOS) {
          _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
        } else if (Platform.isLinux) {
          _readLinuxDeviceInfo(await deviceInfoPlugin.linuxInfo);
        } else if (Platform.isMacOS) {
          _readMacOsDeviceInfo(await deviceInfoPlugin.macOsInfo);
        } else if (Platform.isWindows) {
          _readWindowsDeviceInfo(await deviceInfoPlugin.windowsInfo);
        }
      }
    } on PlatformException {}
    setState(() {

    });
  }
  void _readAndroidBuildData(AndroidDeviceInfo build) {
    platform="android";
    device=build.device??"";
    brand=build.brand??"";
    more_info="sdk="+build.version.sdkInt.toString()+" isPhysicalDevice="+
        build.isPhysicalDevice.toString()+" product="+build.product.toString()
        +" model="+build.model.toString();
  }

  void _readIosDeviceInfo(IosDeviceInfo data) {

    platform="iOS";
    device=data.name??"?";
    brand=data.systemName??"";
    more_info="systemVersion="+data.systemVersion.toString()+" model="+data.model.toString()+
    " isPhysicalDevice="+data.isPhysicalDevice.toString()+" utsname.version="+data.utsname.version.toString();

  }

  void _readLinuxDeviceInfo(LinuxDeviceInfo data) {
    platform="linux";
    device=data.name;
    brand=data.prettyName;
    more_info="version="+data.version.toString()+" versionCodename="+data.versionCodename.toString()+
    " id"+data.id.toString()+" idLike="+data.idLike.toString();
  }

  void _readWebBrowserInfo(WebBrowserInfo data) {
    platform="browser";
    device=data.platform??"?";
    brand=describeEnum(data.browserName);
    more_info="";
  }

  void _readMacOsDeviceInfo(MacOsDeviceInfo data) {
    platform="MacOs";
    device=data.model;
    brand=data.computerName;
    more_info="hostName="+data.hostName+" arch="+data.arch+" kernelVersion="+data.kernelVersion;


  }

  void _readWindowsDeviceInfo(WindowsDeviceInfo data) {
    platform="windows";
    device=data.numberOfCores.toString();
    brand=data.computerName;
    more_info='systemMemoryInMegabytes='+data.systemMemoryInMegabytes.toString();

  }
  bool sendingImage=false;
  var _image;
  String imageAddress="",imgPath="";
  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    _phoneControll.dispose();
    _nameControll.dispose();
    _codeControll.dispose();
    _controller.dispose();
    super.dispose();
  }
  void _incrementCounter(bool visibility,String code,bool show_gif) {
    setState(() {
      visibilityPhone=visibility;
      _show_gif=show_gif;
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
    });
  }
  void _phoneListen(){
    if (_phoneControll.text.isEmpty) {
      _phone = "";
    } else {
      _phone = _phoneControll.text.toEnglishDigit();
      if(!_phone.isNumeric()){
          Fluttertoast.showToast(
              msg: "لطفا از عددها استفاده نمایید",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.TOP,
              timeInSecForIosWeb: 3,
              webBgColor: '#F44336',
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );

          setState(() {
            _phoneControll.text=_phone.substring(0,_phone.length-1);
            _phoneControll.selection = TextSelection.fromPosition(TextPosition(offset: _phoneControll.text.length));
          });
      };
    }
  }
  void _codeListen(){
    if(_codeControll.text.isNotEmpty) {

      String _code=_codeControll.text.toEnglishDigit();
      if (!_code.isNumeric()) {
        Fluttertoast.showToast(
            msg: "لطفا از اعداد استفاده نمایید",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#F44336',
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        setState(() {
          _codeControll.text=_codeControll.text.substring(0,_codeControll.text.length-1);
          _codeControll.selection = TextSelection.fromPosition(TextPosition(offset: _codeControll.text.length));
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: SingleChildScrollView(
          child:  Stack(
            children: [
              _show_gif?const LinearProgressIndicator(minHeight: 250,):Container(
                height: 250,
                width: double.infinity,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                        'به جویا خوش آمدید.',style: TextStyle(fontFamily: "Vazir",fontSize: 15,color: Colors.white)),
                  ],
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 250,
                    margin: const EdgeInsets.only(top: 160),
                    child: Card(
                      color: Colors.white,
                      shape:const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      elevation: 5,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: _get_name? Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            if(_nameControll.text=="")
                              Container(
                                child: const Text(
                                  'ثبت نام',style: TextStyle(fontFamily: "Vazir",fontSize: 15),
                                ),
                              ),
                            InkWell(
                              onTap: ()async{
                                uploadImage2().then((value) {
                                  if(value!="") {
                                    setState(() {
                                      sendingImage = false;
                                      imageAddress = value;
                                    });
                                  }
                                });
                              },
                              child:   sendingImage?const CircularProgressIndicator():
                              imageAddress!=""?
                              CircleAvatar(
                                backgroundImage: NetworkImage(globals.ip+imageAddress),
                                radius: 30,
                              ): Icon(
                                  CupertinoIcons.person_crop_circle,size: 60,
                                  color: Colors.grey[700]),
                            ),
                            TextButton(onPressed: (){
                              uploadImage2().then((value) {
                                if(value!="") {
                                  setState(() {
                                    sendingImage = false;
                                    imageAddress = value;
                                  });
                                }
                              });

                            }, child: Text(imageAddress==""?"انتخاب تصویر پروفایل":"تغییر تصویر پروفایل",
                              style: const TextStyle(fontFamily: "estedad"),)),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Radio(
                                    value: 1,
                                    groupValue: _genderValue,
                                    onChanged: radioButtonChanges,
                                  ),
                                  const Text(
                                    'آقا',
                                    style:  TextStyle(
                                        fontSize: 12.0,
                                        fontFamily: "estedad",
                                        color: Colors.black87
                                    ),
                                  ),
                                  Radio(
                                    value: 2,
                                    groupValue: _genderValue,
                                    onChanged: radioButtonChanges,
                                  ),
                                  const Text(
                                    'خانم',
                                    style:  TextStyle(
                                        fontSize: 12.0,
                                        fontFamily: "estedad",
                                        color: Colors.black87
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 210,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                child:TextFormField(
                                  autofocus: true,
                                  controller: _nameControll,
                                  onFieldSubmitted: (text){
                                    _incrementCounter(false,_codeControll.text,true);
                                    _sendInfo(true);
                                  },
                                  textDirection: TextDirection.rtl,
                                  decoration: const InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        top: 10, right: 5, bottom: 10, left: 5),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.amber, width: 2)),
                                    labelText: 'نام و نام خانوادگی',
                                    hintTextDirection: TextDirection.rtl,
                                    border: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Colors.amber, width: 2)),
                                    errorStyle: TextStyle(color: Colors.redAccent),
                                  ),
                                  style: const TextStyle(
                                    fontSize: 15.0,
                                  ),
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(30),
                                  ],
                                  keyboardType: TextInputType.name,
                                ),
                              ),
                            ),
                            if(!_show_gif)
                              Container(
                                margin: const EdgeInsets.all(12),
                                child:  MaterialButton(
                                  color: Colors.amber,
                                  onPressed:  ()  {
                                    _incrementCounter(false,_codeControll.text,true);
                                    _sendInfo(true);
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                                    child: Text("ادامه",style: TextStyle(fontFamily: "Vazir",fontSize: 16),),
                                  ),
                                ),
                              ),

                          ],
                        ):
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[

                            if(visibilityPhone)const Text(
                                'با وارد کردن شماره همراه خود قوانین استفاده از این اپلیکیشن را پذیرفته اید',style: TextStyle(fontFamily: "Vazir",fontSize: 12)),
                                if(visibilityPhone)TextButton(onPressed: (){launch("http://rules.jooyaap.com");}, child:
                            const Text(
                              'قوانین جویا',style: TextStyle(fontFamily: "Vazir",fontSize: 12,color: Colors.blue)   )),
                            visibilityPhone? const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                'شماره همراه خود را وارد نمایید',style: TextStyle(fontFamily: "Vazir",fontSize: 14),
                              ),
                            ):Text(
                              ' کد دریافتی به شماره $_phone :',style: const TextStyle(fontFamily: "Vazir",fontSize: 13),
                            ),
                            visibilityPhone? Container(
                              margin: const EdgeInsets.only(top: 15),
                              child: IntrinsicWidth(
                                child:Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical:4),
                                  child: TextFormField(
                                    autofocus: visibilityPhone?true:false,
                                    controller: _phoneControll,
                                    textDirection: TextDirection.ltr,
                                    textInputAction: TextInputAction.send,
                                    onFieldSubmitted: (text){
                                      _incrementCounter(true,"",true);
                                      _sendPhone();
                                    },
                                    decoration: const InputDecoration(
                                      contentPadding: EdgeInsets.only(
                                          top: 10, right: 5, bottom: 10, left: 5),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.amber, width: 2)),
                                      hintText: '09                       ',
                                      hintTextDirection: TextDirection.ltr,
                                      border: OutlineInputBorder(
                                          borderSide:BorderSide(color: Colors.amber, width: 2)),
                                      errorStyle: TextStyle(color: Colors.redAccent),
                                    ),
                                    style: const TextStyle(
                                      fontSize: 18.0,
                                    ),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly,
                                    ],
                                  ),
                                ),
                              ),
                            ): Container(
                              margin: const EdgeInsets.only(top: 15),
                              child: IntrinsicWidth(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                                  child: TextFormField(
                                    autofocus: true,
                                    controller: _codeControll,
                                    textDirection: TextDirection.ltr,
                                    decoration: const InputDecoration(
                                      contentPadding: EdgeInsets.only(
                                          top: 10, right: 5, bottom: 10, left: 5),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.amber, width: 2)),
                                      labelText: 'کد دریافتی',
                                      hintText: '                             ',
                                      border: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.amber, width: 2)),
                                      errorStyle: TextStyle(color: Colors.redAccent),
                                    ),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ], // Only numbers can be entered
                                    style: const TextStyle(
                                      fontSize: 18.0,
                                    ),
                                    textInputAction: TextInputAction.send,
                                    onFieldSubmitted: (text){
                                      _incrementCounter(false,_codeControll.text.toEnglishDigit(),true);
                                      _sendCode();
                                    },
                                  ),
                                ),
                              ),
                            ),
                            if(!_show_gif && visibilityPhone)
                              Container(
                                margin: const EdgeInsets.all(20),
                                child: MaterialButton(
                                  color: Colors.amber,
                                  onPressed:  ()  {
                                    _incrementCounter(true,"",true);
                                    _sendPhone();

                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                                    child: Text('دریافت کد تایید',style: TextStyle(fontFamily: "Vazir",fontSize: 16),),
                                  ),
                                ),
                              ),
                            if(!_show_gif && !visibilityPhone)
                              Container(
                                margin: const EdgeInsets.all(10),
                                child:MaterialButton(
                                  color: Colors.amber,
                                  onPressed:  ()  {
                                    _incrementCounter(false,_codeControll.text,true);
                                    _sendCode();

                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                                    child: Text("تایید",style: TextStyle(fontFamily: "Vazir",fontSize: 16),),
                                  ),
                                ),
                              ),
                            if(!visibilityPhone)
                              Row(
                                children: [
                                  TextButton(
                                    onPressed: () {
                                      setState(() {
                                        _phoneControll.text="";
                                      });
                                      _incrementCounter(true,"",false) ; },
                                    child: const Text('تغییر شماره',style: TextStyle(fontFamily: "Vazir",fontSize: 11),),
                                  ),
                                  Spacer(),
                                  CountdownTimer(
                                    endTime: endTime,
                                    widgetBuilder: (_, CurrentRemainingTime? time) {
                                      if (time == null) {
                                        return countDown(0);
                                      }
                                      else {
                                        return countDown(time.sec);
                                      }
                                      /*return Text(
                'days: [ ${time.days} ], hours: [ ${time.hours} ], min: [ ${time.min} ], sec: [ ${time.sec} ]');*/
                                    },
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      if(endTime<DateTime.now().millisecondsSinceEpoch){
                                      _sendPhone();
                                      setState(() {
                                        canResend=false;
                                      });}
                                      else{
                                        Fluttertoast.showToast(
                                            msg: "تا پایان شمارش منتظر بمانید",
                                            toastLength: Toast.LENGTH_LONG,
                                            gravity: ToastGravity.TOP,
                                            timeInSecForIosWeb: 2,
                                            webBgColor: '#F44336',
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0
                                        );
                                      }
                                      },
                                    child:  const Text('ارسال مجدد',style: TextStyle(fontFamily: "Vazir",fontSize: 11,color:Colors.blue),),
                                  ),
                                ],
                              ),
                          ],
                        )
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      /* floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter(false,""),
        tooltip: 'Increment',
        child: Icon(Icons.add),
        isExtended: true,
      ),*/ // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
  Widget countDown(int? time){
    time ??= 0;
    String sm = time.toString().padLeft(2, "0");
    return Container(
      width: 22,
      height: 27,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Center(child: Text(sm,style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white),)),
    );
  }
  Future<bool> _sendPhone() async {
    _phone=globals.checkPhone(_phone.toEnglishDigit());

    if(_phone==""){
      print("wrong phone");
      Fluttertoast.showToast(
          msg: "شماره وارد شده صحیح نیست!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 3,
          webBgColor: '#F44336',
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      setState(() {
        _show_gif=false;
      });
      // await new Future.delayed(const Duration(milliseconds:500));
      return true;
    }
    else{
      setState(() {
        endTime=DateTime.now().millisecondsSinceEpoch + 1000 * 60;
      });
      var jsonResponse = null;
      String url=globals.ip+"/api/login";
      print("url=$url");
      globals.socket.on('event', (data) => print(data));
      // String ip="http://192.168.0.60:44398/api/Account/AppCustomerLogin";

      var response = await   http.post(
        Uri.parse(url),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'phone': _phone,
        }),
      ).timeout(
        const Duration(seconds: 20),
        onTimeout: () {
          Fluttertoast.showToast(
              msg: "زمان ارتباط تمام شد!(اینترنت را چک کنید)",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.TOP,
              timeInSecForIosWeb: 3,
              webBgColor: '#4CAF50',
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0
          );
          setState(() {
            _show_gif=false;
          });
          // Time has run out, do what you wanted to do.
          return http.Response('Error', 408); // Request Timeout response status code
        },
      ).onError((error, stackTrace){
        Fluttertoast.showToast(
            msg: "زمان ارتباط تمام شد!(اینترنت را چک کنید)",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#4CAF50',
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0
        );
        setState(() {
          _show_gif=false;
        });
        return http.Response('Error',407);
      });

      if(response.statusCode == 200) {
        jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if(jsonResponse != null) {
          bool ok=jsonResponse['ok'];
          if(ok){
            //var code=jsonResponse['code'].toString();
            //_exist=!jsonResponse['newUser'];
           /* String msg=jsonResponse['msg'];
          //  print("code is : $code");
            Fluttertoast.showToast(
                msg: msg,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.TOP,
                timeInSecForIosWeb: 3,
                webBgColor: '#4CAF50',
                backgroundColor: Colors.green,
                textColor: Colors.white,
                fontSize: 16.0
            );*/
            //  _codeControll.text=code;
              _show_gif=false;

            _incrementCounter(false,"",false);

          }
        }
      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور با مشکل مواجه شد!",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#F44336',
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        setState(() {
          _show_gif=false;
        });
        print("response.statusCode is: ${response.statusCode}");
        print("The error message is: ${response.body}");
      }
      return true;
    }
  }
  Future<void> _sendCode() async {
    String _code = _codeControll.text.toEnglishDigit();
    print("_code = $_code");
    if (_code.length != 4) {
      print("wrong code");
      Fluttertoast.showToast(
          msg: "کد وارد شده صحیح نیست!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 3,
          webBgColor: '#F44336',
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      setState(() {
        _show_gif = false;
      });
      // await new Future.delayed(const Duration(milliseconds:500));
      return;
    }
    else {
      print("code is =" + _code);
      var jsonResponse = null;
      String url = globals.ip+"/api/verify";
      var response = await http.post(
        Uri.parse(url),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'phone': _phone,
          'code': _code,
        }),
      );

      if (response.statusCode == 200) {
         print("response code="+response.body);
        jsonResponse = json.decode(response.body);
        print("response js=" + jsonResponse.toString());
        if (jsonResponse != null) {
          bool ok = jsonResponse['ok'];
          if (ok) {
            if(jsonResponse['code']==1){
              Fluttertoast.showToast(
                  msg: "کد وارد شده نامعتبر است!",
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.TOP,
                  timeInSecForIosWeb: 3,
                  webBgColor: '#F44336',
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0
              );
              setState(() {
                _show_gif=false;
              });
            }
            else{
              _show_gif=false;
              _get_name=true;
              if(jsonResponse['name']!=null) {
                _nameControll.text=jsonResponse['name'];
              }
              if(jsonResponse['pic']!=null) {
                imageAddress=jsonResponse['pic'];

              }
              if(_nameControll.text.isNotEmpty){
                _sendInfo(false);
              }
              else {
                setState(() {
                  _show_gif=false;
                });
              }
            }
          }else{
            Fluttertoast.showToast(
                msg: jsonResponse['msg'],
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.TOP,
                timeInSecForIosWeb: 3,
                webBgColor: '#F44336',
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0
            );
            setState(() {
              _show_gif = false;
            });
          }
        }
        else {
          Fluttertoast.showToast(
              msg: "در ارتباط با سرور با مشکل مواجه شد!",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.TOP,
              timeInSecForIosWeb: 3,
              webBgColor: '#F44336',
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
          setState(() {
            _show_gif = false;
          });
          print("response.statusCode is: ${response.statusCode}");
          print("The error message is: ${response.body}");
        }
        return;
      }
    }

  }

  Future<void> _sendInfo(bool goNextPage) async {
    print("send info");
    if (_nameControll.text.length < 2) {
      print("wrong name");
      Fluttertoast.showToast(
          msg: "نام وارد شده کامل نیست!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 3,
          webBgColor: '#F44336',
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      setState(() {
        _show_gif = false;
      });
      // await new Future.delayed(const Duration(milliseconds:500));
      return ;
    }
   /* if(goNextPage) {
      interest1=await  Navigator.push(context,
        MaterialPageRoute(builder: (This) =>  SelectTopics()));
    }*/

    print("name is =" + _nameControll.text);
    var jsonResponse = null;
    String url = globals.ip+"/api/signup";
    // String ip="http://192.168.0.60:44398/api/Account/AppCustomerLogin";

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'phone': _phone,
        'name': _nameControll.text,
        'code': _codeControll.text,
        'fire_token': fire_token,
        'pic': imageAddress,
        'platform': platform,
        'device': device,
        'brand': brand,
        'more_info': more_info,
        'gender': _genderValue,
        'interest1': interest1,
      }),
    );
    print("res body = " + response.body.toString());
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        bool ok = jsonResponse['ok'];
        Fluttertoast.showToast(
            msg: jsonResponse['msg'],
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#F44336',
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        if (ok) {
          if(jsonResponse['active']){
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.setString("name", _nameControll.text);
            prefs.setString("phone", _phoneControll.text);
            prefs.setString("pic", imageAddress);
            prefs.setString("user_id", jsonResponse['user_id']);
            prefs.setString("token", jsonResponse['token']);
            prefs.setBool("isLogin", true);
            await Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomeScreen()), (Route<dynamic> route) => false);
          }
          else{
            setState(() {
              _codeControll.text="";
              _nameControll.text="";
              _phoneControll.text="";
              visibilityPhone=true;
              _show_gif=false;
              _get_name=false;
              _phone="";
            });

          }

        }
        else {
          setState(() {
            _show_gif = false;
          });
        }
      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور با مشکل مواجه شد!",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#F44336',
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        setState(() {
          _show_gif = false;
        });
        print("response.statusCode is: ${response.statusCode}");
        print("The error message is: ${response.body}");
      }
    }
    else {
      print(response);
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور با مشکل مواجه شد!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 3,
          webBgColor: '#F44336',
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      print("response.statusCode is: ${response.statusCode}");
      print("The error message is: ${response.body}");
    }
    return;
  }
  /*Future<String> uploadImage1(File _image) async {
    String path="";
    String phone=_phoneControll.text.toString().toEnglishDigit();
    // open a byteStream
    var stream =  http.ByteStream(DelegatingStream.typed(_image.openRead()));
    // var stream  =  http.ByteStream(_image.openRead());
    stream.cast();
    // get file length
    var length = await _image.length();

    // string to uri
    var uri = Uri.parse(globals.ip+"/upload");

    // create multipart request
    var request =  http.MultipartRequest("POST", uri);

    // multipart that takes file.. here this "image_file" is a key of the API request
    var multipartFile =  http.MultipartFile(phone, stream, length, filename: "$phone.jpg");


    // add file to multipart
    request.files.add(multipartFile);


    // send request to upload image
    await request.send().then((response) async {
      // listen for response
      response.stream.transform(utf8.decoder).listen((value) {
        //  print("value="+value.toString());
        var data=json.decode(value);
        path=data['path'];
      });

    }).catchError((e) {
      print(e);
    });
    return path;
  }*/

  Future<String> uploadImage2() async {
    String path="";

    String phone=_phoneControll.text.toString().toEnglishDigit();
    final result = await FilePicker.platform.pickFiles(
      type: FileType.image,
      withData: false,
      withReadStream: true,
    );

    if (result != null) {
      final file = result.files.first;

      var uri = Uri.parse(globals.ip + "/upload");
      Stream<List<int>>? ss = file.readStream;
      if (ss != null) {
        var stream = http.ByteStream(ss);

        var request = http.MultipartRequest('POST', uri);

        var multipartFile = http.MultipartFile(
            phone,
            stream,
            file.size,
            filename: file.name
        );

        request.files.add(multipartFile);

        final httpClient = http.Client();
        final response = await httpClient.send(request);

        if (response.statusCode != 200) {
          throw Exception('HTTP ${response.statusCode}');
        }

        final body = await response.stream.transform(
            utf8.decoder).join();

        print("body ="+body.toString());
        var data=json.decode(body);
        path=data['path'];
        return path;
      }
    }
    return path;
  }
/*  Future<ImageSourceType> _selectImage(BuildContext context) async{
    var type=ImageSourceType.nothing;
    await showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        isScrollControlled: true,
        transitionAnimationController: controller,
        builder: (context) {
          return FractionallySizedBox(
            heightFactor: 0.25,
            child: Column(
              children:  [
                const RotatedBox(quarterTurns: 2,
                    child: Icon(CupertinoIcons.minus,size: 45,color: Colors.black26,)),
                Expanded(
                    child: Row(
                      children: [
                        Expanded(child:
                        MaterialButton(
                          child: Column(
                            children: const [
                              Icon(CupertinoIcons.viewfinder_circle,color: Colors.red,size: 50,),
                              Text('انتخاب از گالری',
                                  style: TextStyle(fontFamily: "Vazir",fontSize: 12,fontWeight: FontWeight.bold )),
                            ],
                          ),
                          onPressed: () {
                            type=ImageSourceType.gallery;
                            Navigator.pop(context);
                          },
                        ),
                        ),
                        Expanded(child:
                        MaterialButton(
                          child: Column(
                            children: const [
                              Icon(CupertinoIcons.camera,color: Colors.red,size: 50,),
                              Text('انتخاب از دوربین',
                                  style: TextStyle(fontFamily: "Vazir",fontSize: 12 ,fontWeight: FontWeight.bold)),
                            ],
                          ),
                          onPressed: () {
                            type=ImageSourceType.camera;
                            Navigator.pop(context);
                          },
                        ),
                        ),

                      ],
                    )
                ),
              ],
            ),
          );
        });
    return type;
  }*/


}
