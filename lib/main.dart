import 'dart:async';

import 'package:flutter/material.dart';
import 'start/globals.dart' as globals;
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'main/splashScreen.dart';

void main() {
  globals.socket.onConnect((_) {
    print('connect');
    //socket.emit('msg', 'test');
  });
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'جویا',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  void checkConnection(){
    Timer(const Duration(seconds:8), () async {

      print("check connection");
      if(globals.socket.disconnected){
        globals.socket.connect();
        // checkConnection();
      }
    });
  }
  @override
  void initState() {
    globals.socket.onDisconnect((data)  {
      print("disconnect");
      checkConnection();
    });

    super.initState();
  }
  @override
  void dispose() {
    globals.socket.disconnect();
    super.dispose();
  }

  Widget build(BuildContext context) {

    // TODO: implement build
    return  MaterialApp(
        debugShowCheckedModeBanner: false,
         home: SplashScreen()
        // home: ReadContact()
       // home: loginShow()
      //  home: MessageList()

    );
  }
}
