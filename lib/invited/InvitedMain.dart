// ignore_for_file: file_names

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:just_audio/just_audio.dart';

import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../datamap/AnswerDataMap.dart';
import '../datamap/AnswerMap.dart';
import '../datamap/AnswerQuestionMap.dart';
import '../datamap/EnterOnDataMap.dart';
import '../datamap/QuestionMap.dart';
import '../datamap/SettingsMap.dart';
import '../datamap/preMap.dart';

import '../widget/globals.dart' as global;
import 'package:http/http.dart' as http;
import 'package:confetti/confetti.dart';

class InvitedMainPage extends StatefulWidget {
  late PreMap myPresent;
  InvitedMainPage({Key? key, required this.myPresent}) : super(key: key);

  @override
  State<InvitedMainPage> createState() => _InvitedMainPageState(myPresent);
}

class _InvitedMainPageState extends State<InvitedMainPage> {

/*
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch(state) {
      case AppLifecycleState.resumed:
        print("resSSSSSSsssssssumed");
      // Handle this case
        break;
      case AppLifecycleState.inactive:
        print("inAAAAAAAAAAAActive");
      // Handle this case
        break;
      case AppLifecycleState.paused:
        print("pauseeEEEEEEEEEEEed");
      // Handle this case
        break;
      case AppLifecycleState.detached:
        print("detachedDDDDDDDDDDDDDDDed");
        // TODO: Handle this case.
        break;
    }
  }
*/

  late PreMap myPresent;
  bool loadQuestion=true;
  QuestionMap questions=QuestionMap("", "", "", 0, "", 0, "", "", "", "", "", false, false, 0, 0, 0);
  List<AnswerMap> answers=[];
  List<AnswerDataMap> myAns=[];
  TextEditingController numAnswerController = TextEditingController(text: "");
  TextEditingController wordAnswerController = TextEditingController(text: "");
  _InvitedMainPageState(this.myPresent){
    numAnswerController.addListener(_numListen);
  }
  late ConfettiController _controllerCenter;
  String wSelect="";
  List<String> sel_list=[];
  List<String> quList=[];
  String phone="";
  String user_id="";
  String name="";
  bool show_bt=false;
  int state=0;
  bool cropLogo=true;
  late AudioPlayer player;
  final alarmAudioPath = "assets/didi.mp3";
  double rating=3;
  bool enableEdit=true;
  List<QuestionMap> freeQuestions=[];
  List<List<AnswerMap>> freeAnswers=[];
  void FirstEnter()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    phone = prefs.getString('phone').toString();
    user_id = prefs.getString('user_id').toString();
    name =await prefs.getString('name').toString();
    EnterOnDataMap eo = EnterOnDataMap(myPresent.id, user_id, phone,name,"");
    String eos = EnterOnDataMap.encode(eo);
    global.socket.emit("enterOn", eos);
  }
  void check()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    phone = prefs.getString('phone').toString();
    user_id = prefs.getString('user_id').toString();

    print("now="+(DateTime.now().millisecondsSinceEpoch/1000).toString());
    print("end="+(myPresent.time_end).toString());
    print("start="+(myPresent.time).toString());
    if((myPresent.time_end+(60*15))<(DateTime.now().millisecondsSinceEpoch/1000)){
     await _getAfterQuestion(myPresent.id,true);
    }
    else {
      await _getLastState(myPresent.id);
    }
    if((myPresent.time+(60*15))>(DateTime.now().millisecondsSinceEpoch/1000)){
      await _getAfterQuestion(myPresent.id,false);
    }
    if(quList.isNotEmpty) {
      String loadId="";
      for(String quID in quList){
        bool amIAnswer=await _isMeAnswerThis(quID,user_id);
        if(!amIAnswer){
          loadId=quID;
          break;
        }
      }
      if(loadId!=""){
        await _getOneQuestion(loadId);
        checkMyAns();
      }
      else {
        setState(() {
          loadQuestion = false;
        });
      }
    }
    else {
      setState(() {
        loadQuestion = false;
      });
    }
  }
  void checkMyAns() {
    print("checkMyAns");
    print("myAns.length"+myAns.length.toString());
    print("answers.length"+answers.length.toString());
    sel_list.clear();
    enableEdit=true;
    if(questions.model=="multiAnswer" || questions.model=="oneAnswer" || questions.model=="OneAnswer") {
      if (answers.isNotEmpty) {
        for (int i = 0; i < myAns.length; i++) {
          String maId = myAns[i].ans;
          print("maId=" + maId);
          for (var a in answers) {
            String aId = a.id;
            print("aId=" + aId);
            if (aId == maId) {
              enableEdit=false;
              if (questions.model == "multiAnswer") {
                sel_list.add(aId);
              }
              else
                wSelect = aId;
              setState(() {

              });
            }
          }
        }
      }
    }
    if(questions.model=="numberAnswer"){
      if(myAns.isNotEmpty) {
        numAnswerController.text = myAns[0].ans;
        enableEdit=false;
      }
    }
    if(questions.model=="wordAnswer"){
      if(myAns.isNotEmpty) {
        wordAnswerController.text=myAns[0].ans;
        enableEdit=false;
      }

    }
    if(questions.model=="rateAnswer"){
      if(myAns.isNotEmpty) {
        rating=double.parse(myAns[0].ans);
        enableEdit=false;
      }
    }
    setState(() {

    });

  }
  @override
  void initState() {
    // TODO: implement initState
    print("initState was called");
    FirstEnter();
    _controllerCenter = ConfettiController(duration: const Duration(seconds: 6));
    String setting=myPresent.settings;
    if(setting!=""){
      SettingsMap settingsMap=SettingsMap.decodeSettings(setting);
      cropLogo=settingsMap.cropLogo;
    }

    check();
    setPreSocket(myPresent.id,context);
    setActiveQSocket(myPresent.id);
    player = AudioPlayer();

    super.initState();
  }
  @override
  void dispose() {
    print("dispose was called");
    EnterOnDataMap eo = EnterOnDataMap(myPresent.id, user_id, phone,name,"");
    String eos = EnterOnDataMap.encode(eo);
    global.socket.emit("enterOFF", eos);
    _controllerCenter.dispose();
    numAnswerController.dispose();
    super.dispose();
  }
  /// A custom Path to paint stars.
  Path drawStar(Size size) {
    // Method to convert degree to radians
    double degToRad(double deg) => deg * (pi / 180.0);

    const numberOfPoints = 5;
    final halfWidth = size.width / 2;
    final externalRadius = halfWidth;
    final internalRadius = halfWidth / 2.5;
    final degreesPerStep = degToRad(360 / numberOfPoints);
    final halfDegreesPerStep = degreesPerStep / 2;
    final path = Path();
    final fullAngle = degToRad(360);
    path.moveTo(size.width, halfWidth);

    for (double step = 0; step < fullAngle; step += degreesPerStep) {
      path.lineTo(halfWidth + externalRadius * cos(step),
          halfWidth + externalRadius * sin(step));
      path.lineTo(halfWidth + internalRadius * cos(step + halfDegreesPerStep),
          halfWidth + internalRadius * sin(step + halfDegreesPerStep));
    }
    path.close();
    return path;
  }
  void _numListen(){
    if (enableEdit) {
      if (numAnswerController.text.isNotEmpty) {
        String _min = numAnswerController.text.toEnglishDigit();
        if (!_min.isNumeric()) {
          Fluttertoast.showToast(
              msg: "لطفا از عدد استفاده نمایید",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.TOP,
              timeInSecForIosWeb: 3,
              webBgColor: '#F44336',
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
          setState(() {
            numAnswerController.text = _min.substring(0, _min.length - 1);
            numAnswerController.selection = TextSelection.fromPosition(
                TextPosition(offset: numAnswerController.text.length));
          });
        }
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          floatingActionButton:
          FloatingActionButton.extended(
            heroTag: null,
            onPressed: () async {
              _writeQuestion(context);
            },
            label: const Text('سوال دارم'),
            icon: const Icon(CupertinoIcons.question),
            backgroundColor: Colors.pink,
            extendedTextStyle: const TextStyle(fontFamily: "estedad",fontSize: 12),
          ),
        appBar: AppBar(
          title: Row(
            children: <Widget>[
              CachedNetworkImage(
                imageUrl: global.ip+global.minImage(myPresent.logo),
                height: 50,
                imageBuilder: (context, imageProvider) => Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: cropLogo?BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.contain),
                  ): BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.contain)
                  ),
                ),
                placeholder: (context, url) => const CircularProgressIndicator(),
                errorWidget: (context, url, error) => Image.asset("image/rad_survey_back.png"),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(myPresent.title.toString().toPersianDigit(),
                    style: const TextStyle(fontFamily: "estedad",fontSize: 13,fontWeight: FontWeight.bold,color: Colors.white),),
                  Text(myPresent.exp.toString().toPersianDigit(),
                    style: const TextStyle(fontFamily: "estedad",fontSize: 11,color: Colors.white),),

                ],
              ),
            ],
          ),
          centerTitle: true,
        ),
        body: SafeArea(
          child: Stack(
            children: [

              Column(
                // This next line does the trick.
                  textDirection: TextDirection.rtl,
                  children:
                  [
                    if(loadQuestion)const LinearProgressIndicator(),
                    if(questions.pic.toString().length>11)Container(
                      margin: const EdgeInsets.only(top: 5),
                      child: Image.network(  global.ip+questions.pic,
                        height: 200.0,
                        fit: BoxFit.contain,)
                    ) ,
                    if(questions.title!="")Container(
                      margin: const EdgeInsets.only(top: 5),
                      child: Row(
                        textDirection: TextDirection.rtl,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(16.0),
                            child: Icon(CupertinoIcons.question_circle,color: Colors.blue,),
                          ),
                          SizedBox(
                            width:  MediaQuery.of(context).size.width-100,
                            child: Text(
                              questions.title.toString().toPersianDigit(),textDirection: TextDirection.rtl,
                              style: const TextStyle(fontFamily: "estedad",fontSize: 15,color: Colors.black54,fontWeight: FontWeight.bold),maxLines: 5,
                            ),
                          ),

                        ],
                      ),
                    ) ,
                    if(questions.title!="" && questions.model=="multiAnswer")
                    const Padding(
                      padding: EdgeInsets.only(bottom: 6),
                      child: Text(
                          "میتوانید چند گزینه را انتخاب کنید.",textDirection: TextDirection.rtl,
                          style: TextStyle(fontFamily: "estedad",fontSize: 12,)
                      ),
                    ),
                    if(!loadQuestion && questions.title=="")
                      const Center(child: Text("نظرسنجی فعلا دردسترس نیست!",style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "estedad"),))
                    ,
                    if(questions.title!="")
                      Expanded(
                        child: Column(
                          children: [
                            if(questions.model.toLowerCase()=="oneanswer"||questions.model=="multiAnswer")
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(11.0),
                                  child: ListView.separated(
                                    separatorBuilder: (BuildContext context, int index) {
                                      return const SizedBox(height: 12);
                                    },
                                    shrinkWrap: true,
                                    itemCount: answers.length,
                                    itemBuilder: (ctx, i) {
                                      final item = answers[i];
                                      return listMaker2(answers[i].title,answers[i].id,answers[i].color,ctx,questions.model);
                                    },
                                  ),
                                ),
                              ),
                            if(questions.model=="numberAnswer")
                              SizedBox(
                                width: 150,
                                child: TextFormField(
                                  autofocus: true,
                                  enabled: enableEdit,
                                  controller: numAnswerController,
                                  onChanged: (value) {
                                    if(enableEdit) {
                                      int nn = int.parse(
                                          value.toString().toEnglishDigit());
                                      if (nn > questions.model_num &&
                                          nn < questions.number) {
                                        setState(() {
                                          show_bt = true;
                                        });
                                      }
                                      else {
                                        setState(() {
                                          show_bt = true;
                                        });
                                      }
                                    }
                                  },
                                  decoration: InputDecoration(
                                    // ignore: prefer_const_constructors
                                    border: OutlineInputBorder(),
                                    labelText:"پاسخ ("+questions.exp+")",
                                  ),
                                  style: const TextStyle(
                                      fontSize: 14.0,
                                      fontFamily: 'estedad',
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            if(questions.model=="wordAnswer")
                              SizedBox(
                                width: 300,
                                child: TextFormField(
                                  autofocus: true,
                                  enabled: enableEdit,
                                  controller: wordAnswerController,
                                  onChanged: (value) {
                                    if(enableEdit) {
                                      if (value.length >
                                          questions.model_num &&
                                          value.length < questions.number) {
                                        setState(() {
                                          show_bt = true;
                                        });
                                      }
                                      else {
                                        setState(() {
                                          show_bt = true;
                                        });
                                      }
                                    }
                                  },
                                  decoration: InputDecoration(
                                    // ignore: prefer_const_constructors
                                    border: OutlineInputBorder(),
                                    labelText:questions.exp,
                                  ),
                                  style: const TextStyle(
                                      fontSize: 14.0,
                                      fontFamily: 'estedad',
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            if(questions.model=="rateAnswer")
                              Center(
                                child: RatingBar.builder(
                                  initialRating: rating,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: true,
                                  itemCount: 5,
                                  updateOnDrag: enableEdit,
                                  itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => const Icon(
                                    CupertinoIcons.star_fill,
                                    color: Colors.blue,
                                  ),
                                  onRatingUpdate: (rat) {
                                    if(enableEdit) {
                                      setState(() {
                                        show_bt = true;
                                        rating = rat;
                                      });
                                    }

                                  },
                                ),
                              ),
                          ],
                        ),
                      ),
                    if(show_bt)
                      Center(
                        child: MaterialButton(
                          onPressed: (){
                           // //celebrate

                            //  _sendAns(wSelect+1);
                            if(questions.model.toString().toLowerCase()=="oneanswer") {
                              AnswerQuestionMap ansQ = AnswerQuestionMap(
                                  myPresent.id, questions.id, wSelect,
                                  user_id);
                              //AnswerQuestionMap ansQ=AnswerQuestionMap(p_id, q_id, a_id, user_id)
                              String ansQs = AnswerQuestionMap.encode(
                                  ansQ);
                              global.socket.emit("newAnswer", ansQs);
                            }
                            else if(questions.model=="multiAnswer"){
                              for(String a_id in sel_list){
                                AnswerQuestionMap ansQ = AnswerQuestionMap(
                                    myPresent.id, questions.id, a_id,
                                    user_id);
                                String ansQs = AnswerQuestionMap.encode(
                                    ansQ);
                                global.socket.emit("newAnswer", ansQs);
                              }
                            }
                            else if(questions.model=="numberAnswer" || questions.model=="wordAnswer"){
                              String a_id=questions.model=="numberAnswer"?numAnswerController.text:wordAnswerController.text;
                              AnswerQuestionMap ansQ = AnswerQuestionMap(
                                  myPresent.id, questions.id, a_id,
                                  user_id);
                              //AnswerQuestionMap ansQ=AnswerQuestionMap(p_id, q_id, a_id, user_id)
                              String ansQs = AnswerQuestionMap.encode(
                                  ansQ);
                              global.socket.emit("newAnswer", ansQs);
                            }
                            else if(questions.model=="rateAnswer"){
                              String a_id=rating.toString();
                              AnswerQuestionMap ansQ = AnswerQuestionMap(
                                  myPresent.id, questions.id, a_id,
                                  user_id);
                              //AnswerQuestionMap ansQ=AnswerQuestionMap(p_id, q_id, a_id, user_id)
                              String ansQs = AnswerQuestionMap.encode(
                                  ansQ);
                              global.socket.emit("newAnswer", ansQs);
                            }
                            bool isCorrect=false;
                            bool hasCorrect=false;
                            for(int i=0;i<answers.length;i++){
                              if(answers[i].answer==1)hasCorrect=true;
                              if(answers[i].id==wSelect){
                                if(answers[i].answer==1){
                                  isCorrect=true;
                                  break;
                                }
                              }

                            }
                            if(hasCorrect) {
                              if (isCorrect) {
                                _controllerCenter.play();
                                Fluttertoast.showToast(
                                    msg: "جواب شما درست بود",
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 3,
                                    backgroundColor: Colors.blue,
                                    webBgColor: '#2196F3',
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }
                              else {
                                Fluttertoast.showToast(
                                    msg: "جواب اشتباه بود!",
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 3,
                                    backgroundColor: Colors.red,
                                    webBgColor: '#F44336',
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }
                              setState(() {
                                show_bt = false;
                              });
                              Timer(const Duration(seconds: 4), () async {
                                _controllerCenter.stop();
                                setState(() {
                                  questions = QuestionMap(
                                      "",
                                      "",
                                      "",
                                      0,
                                      "",
                                      0,
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      false,
                                      false,
                                      0,
                                      0,
                                      0);
                                  answers.clear();
                                  myAns.clear();
                                  quList.clear();
                                });
                                check();
                              });
                            }
                            else {
                              setState(() {
                                show_bt = false;
                                questions = QuestionMap(
                                    "",
                                    "",
                                    "",
                                    0,
                                    "",
                                    0,
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    false,
                                    false,
                                    0,
                                    0,
                                    0);
                                answers.clear();
                                myAns.clear();
                                quList.clear();
                              });
                              check();
                            }


                          },child: Card(
                          color: Colors.blue,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Container(
                                color: Colors.blue,
                                width:170,
                                child:  const Center(
                                    child: Text("ارسال",style: TextStyle(fontFamily: "Vazir",fontSize: 15,color: Colors.white),))),
                          ),
                        ),),
                      ),
                  ]
              ),
              //CENTER -- Blast
              Align(
                alignment: Alignment.center,
                child: ConfettiWidget(
                  confettiController: _controllerCenter,
                  blastDirectionality: BlastDirectionality
                      .explosive, // don't specify a direction, blast randomly
                  shouldLoop:
                  true, // start again as soon as the animation is finished
                  colors: const [
                    Colors.green,
                    Colors.blue,
                    Colors.pink,
                    Colors.orange,
                    Colors.purple
                  ], // manually specify the colors to be used
                  createParticlePath: drawStar, // define a custom shape/path.
                ),
              ),
            ],
          ),
        )
        // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
  Future<bool> _getAfterQuestion(String p_id,bool after ) async {
    //print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/pre_question";
    if(after) url=global.ip+"/api/after_question";
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    // String token=prefs.getString("access_token").toString();
    // print("url ="+url);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'p_id': p_id,
      }),
    );
    // print("response ="+response.body.toString());
    if(response.statusCode == 200) {
      //  print(response.body);

      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        if(jsonResponse['ok']) {
          freeQuestions.clear();
          if(jsonResponse['data']!=null){
            if(jsonResponse['data'].toString().length>8){
              var datas = json.decode(jsonResponse['data']);
               print("datas"+datas.toString());
              for (var data in datas) {
                String _id = data['_id'];
                quList.add(_id);
              }
              return true;
            }
          }}
      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    return false;
  }
  Future<bool> _isMeAnswerThis(String q_id,String user_id ) async {
    var jsonResponse;
    String url=global.ip+"/api/isMeAnswerThisQuestion";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'q_id': q_id,
        'user_id': user_id,
      }),
    );
    // print("response ="+response.body.toString());
    if(response.statusCode == 200) {
      //  print(response.body);

      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        if(jsonResponse['ok']) {
         if(jsonResponse['answer']){
           return true;
         }
         else {
           return false;
         }
        }
      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    return false;
  }
  void _writeQuestion(BuildContext context) {

    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        TextEditingController questionController=TextEditingController(text: "");
        return AlertDialog(

          title: const Center(child: Text('سوال خود را از میزبان بپرسید',style: TextStyle(color: Colors.blueAccent,fontFamily: "estedad",fontSize: 15),)),
          content: Directionality(
            textDirection: TextDirection.rtl,
            child: SingleChildScrollView(
              child:  Center(
                child: Column(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical:4),
                        child: TextFormField(
                          autofocus: true,
                          controller: questionController,
                          decoration: const InputDecoration(
                            // ignore: prefer_const_constructors
                              border: OutlineInputBorder(),
                              labelText: 'سوال'
                          ),
                          style: const TextStyle(
                              fontSize: 15.0,
                              fontFamily: 'estedad',
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: MaterialButton(
                        child: const SizedBox(
                          width: double.infinity,
                          child: Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Center(
                              child: Text('ارسال',
                                  style: TextStyle(fontFamily: "estedad",fontSize: 15 ,fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ),
                        onPressed: () async{
                          await _sendQuestion(myPresent.id,questionController.text,phone);
                          Navigator.pop(context);
                        },
                        color: Colors.blue,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
  Future<String> _sendQuestion(String p_id, String text,String phone) async {
    String url = global.ip + "/api/new/person";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'p_id': p_id,
        'phone': phone,
        'type': 'question',
        'more': text,
      }),
    );
    String id="";
    if (response.statusCode == 200) {
      if (response.body != null) {
        var jsonResponse = json.decode(response.body);
        if (jsonResponse['ok'])
          return jsonResponse['id'];
        else
          return id;
      }
      else {
        return id;
      }
    }
    else {
      return id;
    }
  }

  /*Future<bool> _getFreeQuestion(String p_id,String user_id) async {
    //print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/free_question";
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    // String token=prefs.getString("access_token").toString();
    // print("url ="+url);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'p_id': p_id,
        'user_id': user_id,
      }),
    );
    setState(() {
      loadQuestion = false;
    });
    // print("response ="+response.body.toString());
    if(response.statusCode == 200) {
      //  print(response.body);

      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        if(jsonResponse['ok']) {
          freeQuestions.clear();
          if(jsonResponse['data']!=null){
            if(jsonResponse['data'].toString().length>8){
              var datas = json.decode(jsonResponse['data']);

              /// print("datas"+datas.toString());
              int i = 0,
                  f = 0;
              for (var data in datas) {
                String _id = data['_id'];
                String owner = data['owner'];
                String title = data['title'];
                String titleExp = data['title_exp'];
                //print("titleExp"+titleExp);
                String model = data['model'];
                int model_num = data['model_num'];
                int number = data['number'];
                int viewer = data['viewer'];
                int vote_it = data['vote_it'];
                String group = data['group'];
                String p_id = data['p_id'];
                String settings = data['settings'];
                //("settings"+settings);
                bool isActive = data['is_active'];
                bool is_public = data['is_public'];
                bool free_ans = data['free_ans'];
                freeQuestions.add(QuestionMap(
                    _id,
                    owner,
                    p_id,
                    number,
                    model,
                    model_num,
                    title,
                    titleExp,
                    "",
                    group,
                    settings,
                    isActive,
                    is_public,
                    viewer,
                    vote_it,
                    free_ans));
                freeAnswers.add([]);
                await _getQAns(_id, f);
                f++;
                await _getOneDataAns(_id); //for only this user answers
                setSocket(_id, is_public);
                if (is_public) {
                  await _getAllDataAns(_id); //for all users answers
                }
              }
            }
            setState(() {

            });
          }}
      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    return true;
  }
*/
  void setPreSocket(String p_id,ctx){
    global.socket.on(p_id, (dataS) async{
      var data=json.decode(dataS);
      //  print("connectSocket WWWWWWWWWWWAttTTTCH= "+data.toString());
      String id = data['_id'];

      String owner = data['owner'];
      String title = data['title'];
      String titleExp = data['title_exp'];
      String logo = data['logo'];
      String pic = data['pic'];
      int p_code = data['p_code'];

      String inviteText = data['invite_text'];
      String settings = data['settings'];
      int time_date = data['time_date'];
      int time_date_end = data['time_date_end'];
      String invitedInfo = data['invitedInfo'];
      String location = data['location'];
      String tel = data['tel'];
      String whatsUp = data['whats_up'];
      String email = data['email'];
      String web = data['address'];
      bool isActive = data['is_active'];
      bool freeQ = data['free_question'];
      bool is_del = data['is_del']??false;
      if(is_del)Navigator.of(context).popUntil((route) => route.isFirst);
      if(!isActive)Navigator.of(context).popUntil((route) => route.isFirst);
  /*    if(myPresent.freeQuestion != freeQ){
        if(!freeQ){
          setState(() {
            for(var aa in questio){
              global.socket.off("A"+aa.id);
            }
            freeQuestions.clear();
            freeAnswers.clear();
          });
        }

        myPresent= PreMap(owner,time_date,title,
            titleExp, pic, logo, p_code,myPresent.group, inviteText, settings, location,
            invitedInfo, tel, whatsUp, email, web, id,freeQ, isActive);
        await _getFreeQuestion(myPresent.id,user_id);
        setState(() {

        });
      }*/

        setState(() {
          myPresent=PreMap(owner,time_date,time_date_end,title,
              titleExp, pic, logo, p_code,myPresent.group, inviteText, settings, location,
              invitedInfo, tel, whatsUp, email, web, id,freeQ, isActive);
        });


      return null;
    });
  }

  Widget listMaker2(String answer,String id,int color,BuildContext context,String type) {

    bool select=false;
    if(type=="multiAnswer") {
      for (int i = 0; i < sel_list.length; i++) {
        if (sel_list[i] == id) {
          select = true;
          break;
        }
      }
    }else{
      if(wSelect==id)select=true;
    }
    print("LLLLLLLLLLListMAKet2 = ="+answer);
    Widget result = Card(
      shape:const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      color: Color(color),
      elevation: 3,
      child: InkWell(
        onTap: () {
          if(enableEdit) {
            if (type == "multiAnswer") {
              bool add = true;
              for (int i = 0; i < sel_list.length; i++) {
                if (sel_list[i] == id) {
                  sel_list.removeAt(i);
                  add = false;
                  break;
                }
              }
              if (add) {
                sel_list.add(id);
              }
              setState(() {
                show_bt = sel_list.isNotEmpty;
              });

            } else {
              show_bt = true;
              setState(() {
                wSelect = id;
              });
            }
          }
        },
        child: Padding(
          padding: const EdgeInsets.fromLTRB(5, 15, 5,10),
          child: Row(
            textDirection: TextDirection.rtl,
            children: [
              Expanded(
                flex: 2,
                child: Icon(
                  select ?CupertinoIcons.checkmark_alt_circle_fill:CupertinoIcons.circle,
                  color: select ? Colors.white : Colors.grey,
                ),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  textDirection: TextDirection.rtl,
                  children: [
                    Text(answer.toString().toPersianDigit(),textDirection:TextDirection.rtl,
                        style:select ?  const TextStyle(color: Colors.white,fontFamily: "Vazir",fontSize: 15,fontWeight: FontWeight.bold):
                        const TextStyle(color: Colors.white,fontFamily: "Vazir",fontSize: 14)  )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
    return result;
  }

  Future<bool> _getOneQuestion(String q_id) async {
    print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/oneQuestion";
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    // String token=prefs.getString("access_token").toString();
    print("url ="+url);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'q_id': q_id,
      }),
    );
    // print("response ="+response.body.toString());
    if(response.statusCode == 200) {
      //  print(response.body);
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        if(jsonResponse['ok']){
          var datas=json.decode(jsonResponse['data']);
          answers.clear();
          //  print("datas"+datas.toString());
          for(var data in datas ) {
            print("datas _getOneActiveQuestion " + data.toString());
            int i = 0;

            String _id = data['_id'];
            String owner = data['owner'];
            String title = data['title'];
            String titleExp = data['title_exp'];

            String model = data['model'];
            String pic = data['pics'];
            int model_num = data['model_num'];
            int number = data['number'];
            int viewer = data['viewer'];
            int vote_it = data['vote_it'];
            String group = data['group'];
            String p_id = data['p_id'];
            String settings = data['settings'];
            bool isActive = data['is_active'];
            bool is_public = data['is_public'];
            int free_ans = data['free_ans'];

            questions = QuestionMap(
                _id,
                owner,
                p_id,
                number,
                model,
                model_num,
                title,
                titleExp,
                pic,
                group,
                settings,
                isActive,
                is_public,
                viewer,
                vote_it,
                free_ans);

            await _getQAns(_id, 0);

            await _getOneDataAns(_id); //for only this user answers

            setSocket(_id, is_public);

            if (is_public) {
              await _getAllDataAns(_id); //for all users answers
            }
          }

        }

      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
        setState(() {
          loadQuestion=false;
        });
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
      setState(() {
        loadQuestion=false;
      });
    }
    setState(() {
      loadQuestion=false;
    });
    return true;
  }
  Future<bool> _getLastState(String p_id,) async {
    var jsonResponse;
    String url=global.ip+"/api/getLastState";
    print("url ="+url);
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'p_id': p_id,
      }),
    );
    print("_getLastState ="+response.body.toString());
    if(response.statusCode == 200) {
      //  print(response.body);
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {

        if(jsonResponse['ok']){
          if(jsonResponse['data']!=null) {
            if (jsonResponse['data'].toString().length > 8) {
              var data = json.decode(jsonResponse['data']);
              state=data['state'];
              if(state==0){
                quList.clear();
                return true;
              }
              else{
                quList.add(data["state_id"]);
                return true;
              }
            }
          }
        }
      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    return false;
  }

  Future<bool> _getQAns(String q_id,int qPos) async {
    /// print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/answers";
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    // String token=prefs.getString("access_token").toString();
    /// print("url ="+url);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'q_id': q_id,
      }),
    );
    //print("response ="+response.body.toString());
    if(response.statusCode == 200) {
      // print("answers="+response.body.toString());
      answers.clear();
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        if(jsonResponse['ok']){
          var datas=json.decode(jsonResponse['data']);
          // if(!isFreeQ) print("data answers"+datas.toString());
          for(var data in datas ) {
            String _id = data['_id'];
            String title = data['title']??"";
            String exp = data['title_exp'];
            String pic = data['pic'];
            int color = data['color'];
            int ans = data['answers'];

            AnswerMap ansMap=AnswerMap(color, ans, _id, q_id, title, exp, pic);
            /// if(!isFreeQ) print(" ansMap"+ansMap.title.toString());
            answers.add(ansMap);
          }
          return true;
        }
        return false;

      }
      else {
        Fluttertoast.showToast(
            msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            webBgColor: '#F44336',
            textColor: Colors.white,
            fontSize: 16.0
        );
        return false;
      }
    } else {
      Fluttertoast.showToast(
          msg: "در ارتباط با سرور ناموفق بود، کمی بعد امتحان کنید",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          webBgColor: '#F44336',
          textColor: Colors.white,
          fontSize: 16.0
      );
      return false;
    }
  }

  Future<bool> _getOneDataAns(String q_id) async {
    //for only this user
    //print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/dataAnswer";

    ///  print("url ="+url);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'q_id': q_id,
        'user_id': user_id,
      }),
    );
    print("response  data answer  xxxx="+response.body.toString());
    if(response.statusCode == 200) {
      // print(response.body);
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        if(jsonResponse['ok']){
          if(jsonResponse['data']!=null && jsonResponse['data']!="null") {
            var data =json.decode( jsonResponse['data']);
            myAns.add(AnswerDataMap(q_id, data['a_id'],user_id));
            print("AAAZZZA="+myAns.toString());
          }
          return true;
        }

      }
    }
    return false;
  }
  Future<bool> _getAllDataAns(String q_id) async {
    ///print("get my pre");
    var jsonResponse;
    String url=global.ip+"/api/getAllAnsData";

    // print("url ="+url);

    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'q_id': q_id,
      }),
    );
    // print("all_data="+response.body.toString());
    if(response.statusCode == 200) {
      //  print(response.body);
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        if(jsonResponse['ok']){
          if(jsonResponse['data']!=null && jsonResponse['data']!="null") {
            var data =json.decode( jsonResponse['data']);
            //  myAns.add(data['a_id']);
          }
          return true;
        }

      }
    }
    return false;
  }

  void setSocket(String q_id,bool public){
    global.socket.on("A"+q_id, (dataS) {

      var na=json.decode(dataS);
      String q_id=na['q_id'];
      String a_id=na['a_id'];
      String us_id=na['user_id'];
      if(user_id==us_id){
        myAns.add(AnswerDataMap(q_id, a_id,us_id));
        checkMyAns();
        print("AAAZZZA="+myAns.toString());
      }
      return null;
    });
  }
  void setActiveQSocket(String p_id){
    global.socket.on("state"+p_id, (dataS) async {
      print("stattetette="+dataS.toString());
      var data = json.decode(dataS);
      state=data['state'];
      if(state==0){
        setState(() {
          questions=QuestionMap("", "", "", 0, "", 0, "", "", "", "", "", false, false, 0, 0, 0);
          answers.clear();
          myAns.clear();
        });
      }
      if(state==1){
        String id=data["stateId"];
        questions=QuestionMap("", "", "", 0, "", 0, "", "", "", "", "", false, false, 0, 0, 0);
        answers.clear();
        myAns.clear();
        await _getOneQuestion(id);
        checkMyAns();

      }
      else{
        setState(() {
          questions=QuestionMap("", "", "", 0, "", 0, "", "", "", "", "", false, false, 0, 0, 0);
          answers.clear();
          myAns.clear();
          loadQuestion=false;
        });
      }
      return null;
    });
  }
}




