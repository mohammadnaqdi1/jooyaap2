// ignore_for_file: file_names

import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../datamap/InfoProfileDataMap.dart';
import '../datamap/InviteUserMap.dart';
import '../datamap/SettingsMap.dart';
import '../datamap/UsersMap.dart';
import '../datamap/preMap.dart';
import '../widget/globalQuestion.dart';
import 'package:url_launcher/url_launcher.dart';
import '../datamap/InvitedInfoMap.dart';
import '../datamap/UserProfileMap.dart';
import '../main/profile.dart';
import '../widget/globals.dart' as global;
import 'package:http/http.dart' as http;
import 'InvitedMain.dart';

class InvitedPostPage extends StatefulWidget {
  late PreMap myPresent;
  bool preView;
  List<InfoProfielDataMap> orgInfo;
  InvitedPostPage({Key? key, required this.myPresent, required this.preView, required this.orgInfo}) : super(key: key);

  @override
  State<InvitedPostPage> createState() => _InvitedPostPageState(myPresent: myPresent,preView:preView,orgInfo:orgInfo);
}

class _InvitedPostPageState extends State<InvitedPostPage> {
  late PreMap myPresent;
  bool preView;
  List<InfoProfielDataMap> orgInfo=[];
  _InvitedPostPageState({Key? key, required this.myPresent, required this.preView, required this.orgInfo});
  String user_id="", phone="",name="";
  late SharedPreferences prefs;
  bool cropLogo=true,cover=false;
  bool newAddress=true;
  bool birthDay=false,email=false,city=false,education=false,job=false,gender=false;
  List<String> needInfo=[];
  List<String> needInfoE=[];
  UserProfileMap user=UserProfileMap("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 0, 0, 0);
  UsersMap owner=UsersMap("", "", "", "", "","");

  void checkFirst() async{
    prefs = await SharedPreferences.getInstance();
    phone = prefs.getString('phone').toString();
    name =await prefs.getString('name').toString();
    String my_user_id =await prefs.getString('user_id').toString();
    _getMyInfo(my_user_id);

  }
  List<InfoProfielDataMap> phoneList=[];
  List<InfoProfielDataMap> socialList=[];
  List<InfoProfielDataMap> emailList=[];
  List<InfoProfielDataMap> webList=[];
  List<InfoProfielDataMap> logoList=[];
  List<InfoProfielDataMap> linkList=[];
  String aboutList="";
  List<InfoProfielDataMap> talkList=[];
  List<InfoProfielDataMap> orgList=[];
  List<InfoProfielDataMap> moreList=[];
  InfoProfielDataMap inviteCode=InfoProfielDataMap("", "", "", "", 0, 0, 0, 0);
  InfoProfielDataMap companyOrg=InfoProfielDataMap("", "", "", "", 0, 0, 0, 0);

  void infoListOpen(){
    for(var info in orgInfo){
      print("info.group="+info.group.toString());
      if(info.group==1) {

        if (info.title == global.spinnerItems1[0]) {//تلفن
          phoneList.add(info);
        }
        else if (info.title == global.spinnerItems1[4]) {//"وبسایت"
          webList.add(info);
        }
        else if (info.title == global.spinnerItems1[5]) {//"ایمیل"
          emailList.add(info);
        }
        else if (info.title == global.spinnerItems1[1]) {//"واتساپ"
          if(!kIsWeb) {
            if (Platform.isAndroid) {
              info.word_a = "https://wa.me/" + info.word_a;
            } else {
              info.word_a = "https://api.whatsapp.com/send?phone=" +
                  info.word_a; // new line
            }
          }else{
            info.word_a = "https://api.whatsapp.com/send?phone=" +
                info.word_a;
          }
          info.title = "images/social/WhatsApp.png";
          socialList.add(info);
        }
        else if (info.title == global.spinnerItems1[2] ) {//"تلگرام"
          info.word_a = "https://t.me/" + info.word_a;
          info.title = "images/social/Telegram.png";
          socialList.add(info);
        }
        else if (info.title == global.spinnerItems1[3]) {//"اینستاگرام"
          info.word_a =
          "https://i.instagram.com/api/v1/users/${info.word_a}/info";
          info.title = "images/social/Instagram.png";
          socialList.add(info);
        }
        else if (info.title == global.spinnerItems1[6]) {//"یوتیوب"
          info.title = "images/social/YouTube.png";
          socialList.add(info);
        }
        else if (info.title == "کد دعوت") {
          inviteCode = info;
        }
        else {
          linkList.add(info);
        }
      }
      else{
        print("info.title="+info.title.toString());
        if (info.model == 0) {//"درباره برگزارکننده"
          aboutList+=info.word_a+"\n";
        }
        else  if (info.model ==1) {// "شرکت ، موسسه یا شغل"
          companyOrg=info;
        }
        else  if (info.model == 2) {//سخنران
          talkList.add(info);
        }
        else  if (info.model == 3) {//"برگزارکننده"
          orgList.add(info);
        }
        else{
          moreList.add(info);
        }
      }
    }

    setState(() {

    });
  }

  String perDate="";
  String endDate="";
  String pic="";


  @override
  void initState(){
    super.initState();

    String setting=myPresent.settings;
    if(setting!=""){
      SettingsMap settingsMap=SettingsMap.decodeSettings(setting);
      cropLogo=settingsMap.cropLogo;
      cover=settingsMap.cover;
    }
    var date = DateTime.fromMillisecondsSinceEpoch(myPresent.time * 1000);
     perDate=date.toPersianDateStr()+" ساعت: "+date.hour.toString()+":"+date.minute.toString();
    var date2 = DateTime.fromMillisecondsSinceEpoch(myPresent.time_end * 1000);
      if(date2.day==date.day) {
        endDate=" ساعت: "+date2.hour.toString()+":"+date2.minute.toString();
      } else {
        endDate=date2.toPersianDateStr()+" ساعت: "+date2.hour.toString()+":"+date2.minute.toString();
      }
     pic=global.ip+myPresent.pic;
    checkFirst();
    _getOwner(myPresent.owner);
    if(!preView) _getOrgInfo(myPresent.id);
    else infoListOpen();
  }


  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            children: <Widget>[
              if(myPresent.logo!="")CachedNetworkImage(
                imageUrl: global.ip+global.minImage(myPresent.logo),
                height: 50,
                imageBuilder: (context, imageProvider) => Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: cropLogo?BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: cover?BoxFit.fill:BoxFit.contain),
                  ): BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider, fit: cover?BoxFit.fill:BoxFit.contain)
                  ),
                ),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(myPresent.title.toString().toPersianDigit(),
                    style: const TextStyle(fontFamily: "estedad",fontSize: 13,fontWeight: FontWeight.bold,color: Colors.white),),
                  Text(myPresent.inviteText.toString().toPersianDigit(),
                    style: const TextStyle(fontFamily: "estedad",fontSize: 11,color: Colors.white),),

                ],
              ),
            ],
          ),
          centerTitle: true,
        ),
        body: Column(
          children: [
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,

            children:  [
              if(myPresent.pic.toString().length>5)
                CachedNetworkImage(
                  imageUrl: pic,
                  height: 200,
                  width: double.infinity,
                  placeholder: (context, url) => const Center(child: CircularProgressIndicator()),
                ),
              if((myPresent.time_end+(60*15))<(DateTime.now().millisecondsSinceEpoch/1000)) Padding(
                padding: const EdgeInsets.all(8.0),
                child:  RichText(
                  textDirection: TextDirection.rtl,
                  text: const TextSpan(
                      style: TextStyle( fontFamily: "estedad",color: Colors.blue,   fontSize: 12.0 ),
                      children: [
                        TextSpan(text:"رویداد پایان یافته میتوانید به سوالات پایان رویداد پاسخ دهید",),
                      ]
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child:  RichText(
                  textDirection: TextDirection.rtl,
                  text: TextSpan(
                      style: const TextStyle( fontFamily: "Vazir",color: Colors.blue,   fontSize: 14.0 ),
                      children: [
                        const TextSpan(text:"زمان: ",),
                        TextSpan(text:perDate.toString().toPersianDigit()+" تا "+endDate.toString().toPersianDigit(),
                            style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.black54,)),
                      ]
                  ),
                ),
              ),
              Text(myPresent.exp.toString().toPersianDigit(),
                style: const TextStyle(fontFamily: "estedad",fontSize: 13,color: Colors.black87),),

              /*SelectableAutoLinkText(
                myPresent.address,
                linkStyle: const TextStyle(color: Colors.blueAccent),
                highlightedLinkStyle: TextStyle(
                  color: Colors.blueAccent,
                  backgroundColor: Colors.blueAccent.withAlpha(0x33),
                ),
                onTap: (url) => launch(url, forceSafariVC: false),
                onLongPress: (url) => Share.share(url),
              ),*/
              if(needInfo.isNotEmpty)Padding(
                padding: const EdgeInsets.all(8.0),
                child:Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      textDirection: TextDirection.rtl,
                      children:  [
                        const Icon(CupertinoIcons.exclamationmark_triangle_fill,color:Colors.yellow),
                        RichText(
                          text: const TextSpan(
                              style: TextStyle( fontFamily: "estedad",color: Colors.black87,   fontSize: 13.0 ),
                              children: [
                                TextSpan(text:"برای ورود به این رویداد ",),
                                TextSpan(text:"تکمیل اطلاعات",
                                    style: TextStyle(color: Colors.red,)),
                                TextSpan(text:" زیر مورد نیاز است",),
                              ]
                          ),
                        ),
                      ],
                    ),
                   SizedBox(
                     height: 120,
                     width: double.infinity,
                     child: Wrap(
                       // This next line does the trick.
                         textDirection: TextDirection.rtl,
                         children:
                         List.generate(needInfo.length, (int position){
                           return Wrap(
                             children: [
                               InkWell(
                                 child: Container(
                                   color: Colors.green,
                                   margin: const EdgeInsets.all(5),
                                   width: 75,
                                   height: 30,
                                   child:Center(
                                     child: AutoSizeText(
                                       needInfo[position],
                                       maxFontSize: 14,
                                       style: const TextStyle(fontFamily: "estedad",color: Colors.white,fontWeight: FontWeight.bold
                                       ),
                                       minFontSize: 9,
                                       maxLines: 1,
                                       overflow: TextOverflow.ellipsis,
                                     ),
                                   )
                                 ),
                                 onTap: (){
                                   //_getNeedInf(needInfoE[position]);
                                 },
                               )
                             ],
                           );
                         })
                     ),

                   )
                  ],
                )
              ),

              if(inviteCode.title!="")Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(inviteCode.title+" "+myPresent.pCode.toString().toPersianDigit(),
                      style:  const TextStyle(fontFamily: "estedad"),),
                  ],
                ),
              ),
              Row(
                children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("برگزار کننده:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                ],
              ),
             if(owner.name!="") Padding(padding: const EdgeInsets.only(right: 40),
              child: Row(
                children: [
                  InkWell(
                    onTap: (){
                      Navigator.push( context,MaterialPageRoute(builder: (context) =>  profile(isMe: false,user_id:owner.id ,organizer: false,)));
                      //globals.socket.emit("viewer"," {some}");
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          owner.pic!=""? CircleAvatar(
                            backgroundImage: NetworkImage(global.ip+owner.pic),
                            backgroundColor: Colors.blue,
                            radius: 25,
                          ):Icon(
                              CupertinoIcons.person_crop_circle,size: 40,
                              color: Colors.grey[700]),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(owner.name,style:const TextStyle(fontFamily: "estedad",fontSize: 14),),
                                Text(companyOrg.word_a,style:const TextStyle(fontFamily: "estedad",fontSize: 13),),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              ),
              if(aboutList.isNotEmpty)Row(
                children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("درباره برگزارکننده:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                ],
              ),
             if(aboutList.isNotEmpty)
               Padding(padding: const EdgeInsets.only(right: 30),
                   child: Text(aboutList.toPersianDigit(),
                      style:const TextStyle( fontFamily: "estedad",   fontSize: 12.0 ),)
              ),
              if(orgList.isNotEmpty)Row(
                children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("برگزارکنندگان:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                ],
              ),
             if(orgList.isNotEmpty) Padding(padding: const EdgeInsets.only(right: 30),
              child: SizedBox(
                height: 65,
                child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: orgList.length, itemBuilder: (context, index)
                  {
                    return Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          orgList[index].word_a!=""? CircleAvatar(
                            backgroundImage: NetworkImage(global.ip+orgList[index].word_a),
                            radius: 15,
                          ):Icon(
                              CupertinoIcons.person_crop_circle,size: 40,
                              color: Colors.grey[700]),
                          Text(orgList[index].title.toString().toPersianDigit(),
                            style:const TextStyle( fontFamily: "estedad",color: Colors.black54,fontSize: 12.0 ),),
                        ],
                      ),
                    );
                  })
              ),
              ),
              if(talkList.isNotEmpty)Row(
                children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("سخنرانان:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                ],
              ),
             if(talkList.isNotEmpty) Padding(padding: const EdgeInsets.only(right: 30),
              child: SizedBox(
                height: 75,
                child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: talkList.length, itemBuilder: (context, index)
                  {
                    return Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          talkList[index].word_a!=""? CircleAvatar(
                            backgroundImage: NetworkImage(global.ip+talkList[index].word_a),
                            radius: 17,
                          ):CircleAvatar(
                  backgroundColor: Colors.blue,
                  child: Text(talkList[index].title.toString().substring(0,1),style: TextStyle(color: Colors.white,fontSize: 22,fontWeight: FontWeight.bold),),
                  radius: 17,
                  ),
                          Text(talkList[index].title.toString().toPersianDigit(),
                            style:const TextStyle( fontFamily: "estedad",color: Colors.black54,fontSize: 12.0 ),),
                        ],
                      ),
                    );
                  })
              ),
              ),
              if(moreList.isNotEmpty)Row(
                children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("اطلاعات بیشتر:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                ],
              ),
             if(moreList.isNotEmpty) Padding(padding: const EdgeInsets.only(right: 30),
              child: SizedBox(
                height: 35,
                child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: moreList.length, itemBuilder: (context, index)
                  {
                    return Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(moreList[index].title.toString().toPersianDigit(),
                            style:const TextStyle( fontFamily: "estedad",color: Colors.black54,fontSize: 11.0 ),),
                          Text(moreList[index].word_a.toString().toPersianDigit(),
                            style:const TextStyle( fontFamily: "estedad",color: Colors.black54,fontSize: 13.0 ),),
                        ],
                      ),
                    );
                  })
              ),
              ),
              if(webList.isNotEmpty)Row(
                children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("وبسایت:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                ],
              ),
             if(webList.isNotEmpty) Padding(padding: const EdgeInsets.only(right: 40),
              child: SizedBox(
                height: 35,
                child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: webList.length, itemBuilder: (context, index)
                  {
                    return InkWell(
                      onTap: ()async{
                        String _url=webList[index].word_a;
                        if (!await launch(_url)) {
                          Clipboard.setData(ClipboardData(text: _url));
                          Fluttertoast.showToast(
                              msg: "آدرس کپی شد",
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 2,
                              backgroundColor: Colors.red,
                              webBgColor: '#F44336',
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                          throw 'Could not launch $_url';
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(CupertinoIcons.desktopcomputer,color: Colors.blue,),
                          ),
                          RichText(
                            textDirection: TextDirection.rtl,
                            text: TextSpan(
                                style: const TextStyle( fontFamily: "Vazir",color: Colors.black54,   fontSize: 15.0 ),
                                children: [
                                  const TextSpan(text:"وب : ",),
                                  TextSpan(text:webList[index].word_a, style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.green,)),
                                ]
                            ),
                          ),
                        ],
                      ),
                    );
                  })
              ),
              ),


              if(emailList.isNotEmpty)  Row(
                children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("ایمیل:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                ],
              ),
             if(emailList.isNotEmpty) Padding(padding: const EdgeInsets.only(right: 40),
              child: SizedBox(
                height: 35,
                child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: emailList.length, itemBuilder: (context, index)
                  {
                    return InkWell(
                      onTap: ()async{
                        String _url=emailList[index].word_a;
                        if (!await launch(_url)) {
                          Clipboard.setData(ClipboardData(text: _url));
                          Fluttertoast.showToast(
                              msg: "آدرس کپی شد",
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 2,
                              backgroundColor: Colors.red,
                              webBgColor: '#F44336',
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                          throw 'Could not launch $_url';
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(3.0),
                            child: Icon(CupertinoIcons.mail,color: Colors.red,),
                          ),
                          RichText(
                            textDirection: TextDirection.rtl,
                            text: TextSpan(
                                style: const TextStyle( fontFamily: "Vazir",color: Colors.black54,   fontSize: 13.0 ),
                                children: [
                                  const TextSpan(text:"ایمیل : ",),
                                  TextSpan(text:emailList[index].word_a, style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.green,)),
                                ]
                            ),
                          ),
                        ],
                      ),
                    );
                  })
              ),
              ),
              if(socialList.isNotEmpty)Row(
                children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("شبکه های اجتماعی:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                ],
              ),
             if(socialList.isNotEmpty)
               Padding(padding: const EdgeInsets.only(right: 40),
              child: SizedBox(
                height: 35,
                child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: socialList.length, itemBuilder: (context, index)
                  {
                    return InkWell(
                      onTap: ()async{
                        String url=socialList[index].word_a;

                        if (!await launch(url)) {
                          Clipboard.setData(ClipboardData(text: url));
                          Fluttertoast.showToast(
                              msg: "آدرس کپی شد",
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 2,
                              backgroundColor: Colors.red,
                              webBgColor: '#F44336',
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                          throw 'Could not launch $url';
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Image.asset(socialList[index].title,height: 30,width: 30,)
                      ),
                    );
                  }
                )),
              ),
              if(phoneList.isNotEmpty) Row(
                 children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("تلفن تماس:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                 ],
               ),
             if(phoneList.isNotEmpty)
               Padding(padding: const EdgeInsets.only(right: 40),
              child: SizedBox(
                height: 35,
                child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: phoneList.length, itemBuilder: (context, index)
                  {
                    return InkWell(
                      onTap: (){
                        String tell=phoneList[index].word_a;
                        launch("tel://$tell");
                      },
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(CupertinoIcons.phone_circle_fill,color: Colors.blue,),
                          ),
                          RichText(
                            textDirection: TextDirection.rtl,
                            text: TextSpan(
                                style: const TextStyle( fontFamily: "Vazir",color: Colors.black54,   fontSize: 15.0 ),
                                children: [
                                  TextSpan(text:phoneList[index].word_a.toString().toPersianDigit(), style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.green,)),
                                ]
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                )),
              ),
              if(linkList.isNotEmpty) Row(
                 children: const[
                   Padding(
                     padding: EdgeInsets.only(right: 40),
                     child: Text("لینک ها:",style:  TextStyle(fontFamily: "estedad"),),
                   ),
                 ],
               ),
             if(linkList.isNotEmpty)
               Padding(padding: const EdgeInsets.only(right: 40),
              child: SizedBox(
                height: 40,
                child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: linkList.length, itemBuilder: (context, index)
                  {
                    return InkWell(
                      onTap: ()async{
                        String _url=linkList[index].word_a;
                        if (!await launch(_url)) {
                        Clipboard.setData(ClipboardData(text: _url));
                        Fluttertoast.showToast(
                        msg: "آدرس کپی شد",
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 2,
                        backgroundColor: Colors.red,
                        webBgColor: '#F44336',
                        textColor: Colors.white,
                        fontSize: 16.0
                        );
                        throw 'Could not launch $_url';
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(linkList[index].title)
                      ),
                    );
                  }
                )),
              ),

            ],
    ),
          ),
        ),
            preView?Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: MaterialButton(
                      child: const SizedBox(
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Center(
                            child: Text('برگشت',
                                style: TextStyle(fontFamily: "estedad",fontSize: 12,color: Colors.white )),
                          ),
                        ),
                      ),
                      onPressed: () {
                            Navigator.pop(context,false);
                      },
                      color: Colors.red,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: MaterialButton(
                        child: const SizedBox(
                          child: Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Center(
                              child: Text('تایید و ذخیره',
                                  style: TextStyle(fontFamily: "estedad",fontSize: 15 ,
                                      fontWeight: FontWeight.bold,color: Colors.white)),
                            ),
                          ),
                        ),
                        onPressed: () {
                          Navigator.pop(context,true);  },
                        color: Colors.blue,
                      ),
                    ),
                  ),
                ],
              ),
            )
                :Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: MaterialButton(
                      child: const SizedBox(
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Center(
                            child: Text('حذف دعوت',
                                style: TextStyle(fontFamily: "estedad",fontSize: 12,color: Colors.white )),
                          ),
                        ),
                      ),
                      onPressed: () async{
                        bool? del=await removeInviteDialog(context);
                        if(del!){
                          bool doit=await changeInvite(phone,"reject");
                          if(doit){
                            Navigator.pop(context);
                          }
                        }
                      },
                      color: Colors.red,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: MaterialButton(
                        child:  SizedBox(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Center(
                              child: Text(needInfo.isEmpty?'ورود':"تکمیل اطلاعات",
                                  style: const TextStyle(fontFamily: "estedad",fontSize: 15 ,
                                      fontWeight: FontWeight.bold,color: Colors.white)),
                            ),
                          ),
                        ),
                        onPressed: () async {
                          if(needInfo.isEmpty) {
                            changeInvite(phone,"accept");
                            Navigator.push( context,MaterialPageRoute(builder: (context) =>  InvitedMainPage(myPresent: myPresent)),);
                          }
                          else{
                            bool reload=await showDialog(
                              context: context,
                              builder: (_) => globalQuestion(needInfo: needInfo, needInfoE: needInfoE,user: user,),
                            );
                            if(reload){
                              String my_user_id =await prefs.getString('user_id').toString();
                              needInfo.clear();
                              needInfoE.clear();
                              _getMyInfo(my_user_id);
                            }

                          }
                        },
                        color:Colors.blue,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        )
        // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
  Future<bool?> removeInviteDialog(BuildContext context) async{
   bool? del=await showDialog<bool>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Center(child: Text('حذف دعوت',style: TextStyle(color: Colors.blueAccent,fontFamily: "Vazir",fontSize: 15),)),
          content: const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 8, vertical:4),
              child: Text('آیا میخواهید این رویداد برای شما حذف شود؟',style: TextStyle(color: Colors.blueAccent,fontFamily: "Vazir",fontSize: 15),)
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('لغو'),
              onPressed: () {
                Navigator.pop(context,false);
              },
            ),
            TextButton(
              child: const Text('حذف',style: TextStyle(color: Colors.redAccent),),
              onPressed: () async{
                Navigator.pop(context,true);
              },
            ),
          ],
        );
      },
    );
   return del;
  }

  void _launchMapsUrl(String latlng) async {
    final url = 'https://www.google.com/maps/search/?api=1&query=$latlng';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  ///////////////////////

  Future<bool> changeInvite(String phone,String accept) async {
    String url = global.ip + "/api/change/pdataInvite";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'phone': phone,
        'p_id': myPresent.id,
        'accept': accept,
      }),
    );

    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      if (jsonResponse['ok']) {
        if(accept=="reject") {
          InviteUserMap inviteUserMap = InviteUserMap(
              "not " + "invited", myPresent.id, phone);
          String inviteUser = InviteUserMap.encode(inviteUserMap);
          global.socket.emit("newInviteUser", inviteUser);
        }

        return true;
      } else {
        return false;
      }
    }
    else {
      return false;
    }
  }
  Future<void> _getOwner(String user_id) async {

    var jsonResponse = null;
    String url = global.ip+"/api/getUserById";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'user_id': user_id,
      }),
    );
    if (response.statusCode == 200) {
      print("response code="+response.body);
      jsonResponse = json.decode(response.body);
      print("response js=" + jsonResponse.toString());
      if (jsonResponse != null) {
        bool ok = jsonResponse['ok'];
        if (ok) {
          var data = json.decode(jsonResponse['data']);
          print("data=" + data.toString());
          bool userActive=data['user_active'];
          String user_name=data['user_name'];
          String user_phone=data['user_phone'];
          String user_pic=data['user_pic'];
          String user_company=data['user_company'];
          owner=UsersMap(user_id, user_phone, user_company, user_name, user_pic,"");
          setState(() {

          });
        }
      }
      else {
        Fluttertoast.showToast(
            msg: jsonResponse['data'],
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#F44336',
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        print("response.statusCode is: ${response.statusCode}");
        print("The error message is: ${response.body}");
      }
      return;
    }
  }
  Future<void> _getOrgInfo(String p_id) async {

    var jsonResponse = null;
    String url = global.ip+"/api/PreProfileData";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'p_id': p_id,
      }),
    );
    if (response.statusCode == 200) {
      print("response code="+response.body);
      jsonResponse = json.decode(response.body);
      print("response js=" + jsonResponse.toString());
      if (jsonResponse != null) {
        bool ok = jsonResponse['ok'];
        if (ok) {
          var data = json.decode(jsonResponse['data']);
          for(var info in data){
            orgInfo.add(InfoProfielDataMap(info['_id'], info['user_id'],info['word_a'] ,
                info['title'],info['num_a'] , info['model'], info['group'],info['link'] ));
          }
          infoListOpen();
        }
      }
      else {
        Fluttertoast.showToast(
            msg: jsonResponse['data'],
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#F44336',
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        print("response.statusCode is: ${response.statusCode}");
        print("The error message is: ${response.body}");
      }
      return;
    }
  }

  Future<void> _getMyInfo(String user_id) async {

    var jsonResponse = null;
    String url = global.ip+"/api/getUserById";
    var response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'user_id': user_id,
      }),
    );
    if (response.statusCode == 200) {
     // print("response code="+response.body);
      jsonResponse = json.decode(response.body);
     // print("response js=" + jsonResponse.toString());
      if (jsonResponse != null) {
        bool ok = jsonResponse['ok'];
        if (ok) {
          var data = json.decode(jsonResponse['data']);
          print("data=" + data.toString());
          bool userActive=data['user_active'];
          String user_name=data['user_name'];
          String user_phone=data['user_phone'];
          String user_pic=data['user_pic']??"";
          String user_device=data['user_device']??"";
          String email=data['email']??"";
          String whats_up=data['whats_up']??"";
          String user_job=data['user_job']??"";
          String user_city=data['user_city']??"";
          String interest1=data['interest1']??"";
          String interest2=data['interest2']??"";
          String interest3=data['interest3']??"";
          String interest4=data['interest4']??"";
          String education=data['education']??"";
          String user_company=data['user_company']??"";
          int user_date=data['user_date']??0;
          int user_birthday=data['user_birthday']??0;
          int gender=data['gender'];
          user=UserProfileMap(user_id, user_phone, user_device, user_name, user_pic,
              email, whats_up, user_company, user_job, user_city, interest1,
              interest2, interest3, interest4, education, user_date, user_birthday, gender);
       /*   if(user_birthday!=0) {
            try {
             // var date =  DateTime.fromMillisecondsSinceEpoch(user.user_birthday * 1000);
             // birthday=date.year.toString()+"/"+date.month.toString()+""+date.day.toString();
            } on Exception catch (_) {
              print('no info');
            }
          }*/
          try {
            InvitedInfoMap invitedInfoMap=InvitedInfoMap.decodeSettings(myPresent.invitedInfo);
            bool bbirthDay=invitedInfoMap.birthDay;
            bool bemail=invitedInfoMap.email;
            bool bcity=invitedInfoMap.city;
            bool beducation=invitedInfoMap.education;
            bool bjob=invitedInfoMap.job;
            bool bgender=invitedInfoMap.gender;
            newAddress=true;
            if(bbirthDay && user_birthday==0) { needInfo.add(" تاریخ تولد ");needInfoE.add("birthday");}
            if(bemail && email==""){needInfo.add(" ایمیل ");needInfoE.add("email");}
            if(bcity && user_city==""){needInfo.add(" شهر محل زندگی ");needInfoE.add("city");}
            if(beducation && education==""){needInfo.add(" تحصیلات ") ;needInfoE.add("edu");}
            if(bjob && user_job==""){needInfo.add(" شغل ") ;needInfoE.add("job");}
            if(bgender && gender==0){needInfo.add(" جنسیت ");needInfoE.add("gender");}
          } on Exception catch (_) {
            newAddress=false;
            print('no info');
          }
          setState(() {

          });
        }
      }
      else {
        Fluttertoast.showToast(
            msg: jsonResponse['data'],
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            webBgColor: '#F44336',
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        print("response.statusCode is: ${response.statusCode}");
        print("The error message is: ${response.body}");
      }
      return;
    }
  }
  void _getNeedInfo(String need){
    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Center(child: Text('حذف اسلاید',style: TextStyle(color: Colors.blueAccent,fontFamily: "Vazir",fontSize: 15),)),
          content: Directionality(
            textDirection: TextDirection.rtl,
            child: Container(
              height: 110,
              child: Center(
                child: Column(
                  children: const [
                    Padding(
                        padding:  EdgeInsets.symmetric(horizontal: 8, vertical:4),
                        child: Text('آیا از حذف این اسلاید مطمئن هستید؟',style: TextStyle(color: Colors.blueAccent,fontFamily: "Vazir",fontSize: 15),)
                    ),
                    // MaterialButton(onPressed: (){},color: Colors.blue,child: Text("ارسال",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),),)
                  ],
                ),
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('لغو'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('حذف',style: TextStyle(color: Colors.redAccent),),
              onPressed: () async{
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return const Center(child: CircularProgressIndicator(),);
                    });

                //await Navigator.popAndPushNamed(context, '/');
              },
            ),
          ],
        );
      },
    );
  }



}



