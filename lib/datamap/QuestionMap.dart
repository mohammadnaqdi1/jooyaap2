// ignore_for_file: file_names
class QuestionMap {

  int _model_num,_viewer,_vote_it,_number,_free_ans;
  String _id,_owner,_title,_exp,_pic,_model,_p_id,_group,
      _settings;
  bool _isActive,_is_public;

  QuestionMap(
      this._id,
      this._owner,
      this._p_id,
      this._number,
      this._model,
      this._model_num,
      this._title,
      this._exp,
      this._pic,
      this._group,
      this._settings,
      this._isActive,
      this._is_public,
      this._viewer,
      this._vote_it,
      this._free_ans);

  get free_ans => _free_ans;

  set free_ans(value) {
    _free_ans = value;
  }

  get number => _number;

  set number(value) {
    _number = value;
  }

  get is_public => _is_public;

  set is_public(value) {
    _is_public = value;
  }

  bool get isActive => _isActive;

  set isActive(bool value) {
    _isActive = value;
  }

  get settings => _settings;

  set settings(value) {
    _settings = value;
  }

  get group => _group;

  set group(value) {
    _group = value;
  }

  get p_id => _p_id;

  set p_id(value) {
    _p_id = value;
  }

  get model => _model;

  set model(value) {
    _model = value;
  }

  get pic => _pic;

  set pic(value) {
    _pic = value;
  }

  get exp => _exp;

  set exp(value) {
    _exp = value;
  }

  get title => _title;

  set title(value) {
    _title = value;
  }

  get owner => _owner;

  set owner(value) {
    _owner = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  get vote_it => _vote_it;

  set vote_it(value) {
    _vote_it = value;
  }

  get viewer => _viewer;

  set viewer(value) {
    _viewer = value;
  }

  int get model_num => _model_num;

  set model_num(int value) {
    _model_num = value;
  }
}