// ignore_for_file: file_names

import 'dart:convert';

class EligibleListMap {
 
  String userId,name;
  int num;

  EligibleListMap({required this.num,required this.userId,required this.name});

  factory EligibleListMap.fromJson(Map<String, dynamic> jsonData) {
    return  EligibleListMap(
      num: jsonData['num'],
      userId: jsonData['userId'],
      name: jsonData['name'],
      );
  }

  static Map<String, dynamic> toMap(EligibleListMap station) => {
    'num': station.num,
    'userId': station.userId,
    'name': station.name,
  };

  static String encode(List<EligibleListMap> sts) => json.encode(
    sts.map<Map<String, dynamic>>((st) => EligibleListMap.toMap(st))
        .toList(),
  );

  static List<EligibleListMap> decode(String EligibleListMapList) =>
      (json.decode(EligibleListMapList) as List<dynamic>)
          .map<EligibleListMap>((item) => EligibleListMap.fromJson(item))
          .toList();
}
