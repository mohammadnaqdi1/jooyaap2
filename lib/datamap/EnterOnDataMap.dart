// ignore_for_file: file_names
import 'dart:convert';

class EnterOnDataMap {


  String p_id,user_id,phone,name,pic;

  EnterOnDataMap(this.p_id, this.user_id, this.phone, this.name, this.pic);


  static Map<String, dynamic> toMap(EnterOnDataMap services) => {
    'p_id': services.p_id,
    'user_id': services.user_id,
    'phone': services.phone,
    'name': services.name,
    'pic': services.pic,
  };
  factory EnterOnDataMap.fromJson(Map<String, dynamic> jsonData) {

    return EnterOnDataMap(
      jsonData['p_id'],
      jsonData['user_id'],
      jsonData['phone'],
      jsonData['name'],
      jsonData['pic'],
    );
  }

  static String encode(EnterOnDataMap sts) => json.encode(EnterOnDataMap.toMap(sts));

  //static EnterOnDataMap decode(String EnterOnDataMap_list) => (json.decode(EnterOnDataMap_list)).;

  static EnterOnDataMap decodeSettings(String ServicesMap_list) {
    return EnterOnDataMap.fromJson(json.decode(ServicesMap_list));
  }
  
}