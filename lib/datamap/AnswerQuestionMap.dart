// ignore_for_file: file_names

import 'dart:convert';

class AnswerQuestionMap {

  final String p_id,q_id,a_id,user_id;


  AnswerQuestionMap( this.p_id, this.q_id,
      this.a_id,this.user_id);

  static Map<String, dynamic> toMap(AnswerQuestionMap services) => {
    'p_id': services.p_id,
    'q_id': services.q_id,
    'a_id': services.a_id,
    'user_id': services.user_id,
  };

  static String encode(AnswerQuestionMap sts) => json.encode(AnswerQuestionMap.toMap(sts));


}
