class OrgProfileMap{
  String _user_id,_title,_company,_about,_pic,_logo,_more_info,_topic,_connect_info;
  int approved;

  OrgProfileMap(
      this._user_id,
      this._title,
      this._company,
      this._about,
      this._pic,
      this._logo,
      this._more_info,
      this._topic,
      this._connect_info,
      this.approved);

  get connect_info => _connect_info;

  set connect_info(value) {
    _connect_info = value;
  }

  get topic => _topic;

  set topic(value) {
    _topic = value;
  }

  get more_info => _more_info;

  set more_info(value) {
    _more_info = value;
  }

  get logo => _logo;

  set logo(value) {
    _logo = value;
  }

  get pic => _pic;

  set pic(value) {
    _pic = value;
  }

  get about => _about;

  set about(value) {
    _about = value;
  }

  get company => _company;

  set company(value) {
    _company = value;
  }

  get title => _title;

  set title(value) {
    _title = value;
  }

  String get user_id => _user_id;

  set user_id(String value) {
    _user_id = value;
  }
}