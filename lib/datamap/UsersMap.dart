// ignore_for_file: file_names
class UsersMap {


  String _id,_phone,_type,_name,pic,more;

  UsersMap(this._id, this._phone, this._type, this._name, this.pic,this.more);

  get name => _name;

  set name(value) {
    _name = value;
  }

  get type => _type;

  set type(value) {
    _type = value;
  }

  get phone => _phone;

  set phone(value) {
    _phone = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }
}