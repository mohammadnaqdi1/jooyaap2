// ignore_for_file: file_names
class SimpleDataMap {


  String _id,_qId;
  int _num;

  SimpleDataMap(this._qId,this._id, this._num);

  int get num => _num;

  set num(int value) {
    _num = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  get qId => _qId;

  set qId(value) {
    _qId = value;
  }
}