// ignore_for_file: file_names
class UserProfileMap {


  String _id,_phone,_user_device,_name,_pic,_email,
      _whats_up,_user_company,_user_job,_user_city,_interest1,
      _interest2,_interest3,_interest4,_education;
  int _user_date,_user_birthday,_gender;

  UserProfileMap(
      this._id,
      this._phone,
      this._user_device,
      this._name,
      this._pic,
      this._email,
      this._whats_up,
      this._user_company,
      this._user_job,
      this._user_city,
      this._interest1,
      this._interest2,
      this._interest3,
      this._interest4,
      this._education,
      this._user_date,
      this._user_birthday,
      this._gender);

  get gender => _gender;

  set gender(value) {
    _gender = value;
  }

  get user_birthday => _user_birthday;

  set user_birthday(value) {
    _user_birthday = value;
  }

  int get user_date => _user_date;

  set user_date(int value) {
    _user_date = value;
  }

  get education => _education;

  set education(value) {
    _education = value;
  }

  get interest4 => _interest4;

  set interest4(value) {
    _interest4 = value;
  }

  get interest3 => _interest3;

  set interest3(value) {
    _interest3 = value;
  }

  get interest2 => _interest2;

  set interest2(value) {
    _interest2 = value;
  }

  get interest1 => _interest1;

  set interest1(value) {
    _interest1 = value;
  }

  get user_city => _user_city;

  set user_city(value) {
    _user_city = value;
  }

  get user_job => _user_job;

  set user_job(value) {
    _user_job = value;
  }

  get user_company => _user_company;

  set user_company(value) {
    _user_company = value;
  }

  get whats_up => _whats_up;

  set whats_up(value) {
    _whats_up = value;
  }

  get email => _email;

  set email(value) {
    _email = value;
  }

  get pic => _pic;

  set pic(value) {
    _pic = value;
  }

  get name => _name;

  set name(value) {
    _name = value;
  }

  get user_device => _user_device;

  set user_device(value) {
    _user_device = value;
  }

  get phone => _phone;

  set phone(value) {
    _phone = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }
}
