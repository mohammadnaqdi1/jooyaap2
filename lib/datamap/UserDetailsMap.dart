// ignore_for_file: file_names

class UserDetailsMap{

  String _userId,_name,_phone,_date,_answer;
  int _color;
  List<int> _listColor;
  List<String> _listAns;


  UserDetailsMap(this._userId, this._name, this._phone, this._date,
      this._answer, this._color, this._listColor ,this._listAns);


  List<int> get listColor => _listColor;

  set listColor(List<int> value) {
    _listColor = value;
  }

  int get color => _color;

  set color(int value) {
    _color = value;
  }

  get answer => _answer;

  set answer(value) {
    _answer = value;
  }

  get date => _date;

  set date(value) {
    _date = value;
  }

  get phone => _phone;

  set phone(value) {
    _phone = value;
  }

  get name => _name;

  set name(value) {
    _name = value;
  }

  String get userId => _userId;

  set userId(String value) {
    _userId = value;
  }

  List<String> get listAns => _listAns;

  set listAns(List<String> value) {
    _listAns = value;
  }
}