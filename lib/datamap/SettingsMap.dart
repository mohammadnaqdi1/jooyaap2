// ignore_for_file: file_names

import 'dart:convert';

import 'package:flutter/cupertino.dart';

class SettingsMap {
 final int titleColor,inviteColor,dataColor,otherColor ,model;
  final String backImage, more;
  final bool cropLogo,cover;


 SettingsMap(this.titleColor, this.inviteColor, this.dataColor,
      this.otherColor, this.backImage, this.more, this.cropLogo, this.cover, this.model);


 static Map<String, dynamic> toMap(SettingsMap services) => {
   'titleColor': services.titleColor,
   'inviteColor': services.inviteColor,
   'dataColor': services.dataColor,
   'otherColor': services.otherColor,
   'backImage': services.backImage,
   'more': services.more,
   'cropLogo': services.cropLogo,
   'cover': services.cover,
   'model': services.model,
 };
 factory SettingsMap.fromJson(Map<String, dynamic> jsonData) {

   return SettingsMap(
       jsonData['titleColor'],
       jsonData['inviteColor'],
       jsonData['dataColor'],
        jsonData['otherColor'],
       jsonData['backImage'],
       jsonData['more'],
        jsonData['cropLogo'],
        jsonData['cover'],
        jsonData['model'],
   );
 }

 static String encode(SettingsMap sts) => json.encode(SettingsMap.toMap(sts));

 //static SettingsMap decode(String SettingsMap_list) => (json.decode(SettingsMap_list)).;

 static SettingsMap decodeSettings(String ServicesMap_list) {
     return SettingsMap.fromJson(json.decode(ServicesMap_list));
 }

}
