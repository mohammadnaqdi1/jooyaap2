// ignore_for_file: file_names
class InfoProfielDataMap {


  String _id,_user_id,_word_a,_title;
  int _num_a,_model,_group,_link;
// title: String,//like name,birthday,job,web
  //     model: Number,//1:show only icon 2:show title ,3:show both,4:show  only word
  //     group: Number,//1:connection 2:info
  //     link:Number//1:call 2:web 3:email 4:share 5:location
  InfoProfielDataMap(this._id, this._user_id, this._word_a, this._title, this._num_a,
      this._model, this._group, this._link);

  get link => _link;

  set link(value) {
    _link = value;
  }

  get group => _group;

  set group(value) {
    _group = value;
  }

  get model => _model;

  set model(value) {
    _model = value;
  }

  int get num_a => _num_a;

  set num_a(int value) {
    _num_a = value;
  }

  get title => _title;

  set title(value) {
    _title = value;
  }

  get word_a => _word_a;

  set word_a(value) {
    _word_a = value;
  }

  get user_id => _user_id;

  set user_id(value) {
    _user_id = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }




}