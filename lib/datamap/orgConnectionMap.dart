// ignore_for_file: file_names

import 'dart:convert';

class orgConnectionMap {

   String telegram, whatsUp, instagram,tel, web, youtube, email, other1,
      other2, other3;


  orgConnectionMap(this.telegram, this.whatsUp,
      this.instagram, this.tel, this.web, this.youtube, this.email, this.other1,
      this.other2, this.other3,);



  static Map<String, dynamic> toMap(orgConnectionMap services) =>
      {
        'telegram': services.telegram,
        'whatsUp': services.whatsUp,
        'instagram': services.instagram,
        'tel': services.tel,
        'web': services.web,
        'youtube': services.youtube,
        'email': services.email,
        'other1': services.other1,
        'other2': services.other2,
        'other3': services.other3,
      };

  factory orgConnectionMap.fromJson(Map<String, dynamic> jsonData) {
    return orgConnectionMap(
      jsonData['telegram'],
      jsonData['whatsUp'],
      jsonData['instagram'],
      jsonData['tel'],
      jsonData['web'],
      jsonData['youtube'],
      jsonData['email'],
      jsonData['other1'],
      jsonData['other2'],
      jsonData['other3'],
    );
  }

  static String encode(orgConnectionMap sts) =>
      json.encode(orgConnectionMap.toMap(sts));

  static orgConnectionMap decodeSettings(String ServicesMap_list) {
    return orgConnectionMap.fromJson(json.decode(ServicesMap_list));
  }
}
