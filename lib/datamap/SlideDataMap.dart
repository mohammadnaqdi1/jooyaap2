// ignore_for_file: file_names
class SlideDataMap {


  String _id,_pId,_title,_titleExp,_owner,_slideType,_address,_settings;
  bool _active;

  SlideDataMap(this._id, this._pId, this._title, this._titleExp, this._owner,
      this._slideType, this._address, this._settings, this._active);

  bool get active => _active;

  set active(bool value) {
    _active = value;
  }

  get settings => _settings;

  set settings(value) {
    _settings = value;
  }

  get address => _address;

  set address(value) {
    _address = value;
  }

  get slideType => _slideType;

  set slideType(value) {
    _slideType = value;
  }

  get owner => _owner;

  set owner(value) {
    _owner = value;
  }

  get titleExp => _titleExp;

  set titleExp(value) {
    _titleExp = value;
  }

  get title => _title;

  set title(value) {
    _title = value;
  }

  get pId => _pId;

  set pId(value) {
    _pId = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }
}