// ignore_for_file: file_names
class AnswerMap {

  int _color,_answer;
  String _id,_q_id,_title,_exp,_pic;

  AnswerMap(this._color, this._answer, this._id, this._q_id, this._title,
      this._exp, this._pic);

  get pic => _pic;

  set pic(value) {
    _pic = value;
  }

  get exp => _exp;

  set exp(value) {
    _exp = value;
  }

  get title => _title;

  set title(value) {
    _title = value;
  }

  get q_id => _q_id;

  set q_id(value) {
    _q_id = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  get answer => _answer;

  set answer(value) {
    _answer = value;
  }

  int get color => _color;

  set color(int value) {
    _color = value;
  }
}