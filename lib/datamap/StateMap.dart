// ignore_for_file: file_names

import 'dart:convert';

class StateMap {

  final String pId,stateId;
  int state;


  StateMap( this.pId, this.stateId,
      this.state);

  static Map<String, dynamic> toMap(StateMap services) => {
    'pId': services.pId,
    'stateId': services.stateId,
    'state': services.state,
  };

  static String encode(StateMap sts) => json.encode(StateMap.toMap(sts));


}
