// ignore_for_file: file_names

import 'dart:convert';

class PreMaps {

  final int time_date;
  final String owner,title,title_exp,pic,logo,p_code,group,invite_text,
      settings,location,invitedInfo,tel,tags,email,address,_id;
  bool is_active,freeQuestion;

  PreMaps(
      this.owner,
      this.time_date,
      this.title,
      this.title_exp,
      this.pic,
      this.logo,
      this.p_code,
      this.group,
      this.invite_text,
      this.settings,
      this.location,
      this.invitedInfo,
      this.tel,
      this.tags,
      this.email,
      this.address,
      this._id,
      this.freeQuestion,
      this.is_active);

  static Map<String, dynamic> toMap(PreMaps services) => {
    'owner': services.owner,
    'time_date': services.time_date,
    'title': services.title,
    'title_exp': services.title_exp,
    'pic': services.pic,
    'logo': services.logo,
    'p_code': services.p_code,
    'group': services.group,
    'invite_text': services.invite_text,
    'settings': services.settings,
    'location': services.location,
    'invitedInfo': services.invitedInfo,
    'tel': services.tel,
    'tags': services.tags,
    'email': services.email,
    'address': services.address,
    '_id': services._id,
    'freeQuestion': services.freeQuestion,
    'is_active': services.is_active,
  };

  static String encode(PreMaps sts) => json.encode(PreMaps.toMap(sts));
  
}