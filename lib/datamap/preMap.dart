// ignore_for_file: file_names
class PreMap {

  int _pCode,_time,_time_end;
  String _owner,_title,_exp,_pic,_logo,_group,_inviteText,
      _settings,_location,_invitedInfo,_tel,tags,_email,_address,_id;
  bool _isActive,_freeQuestion;

  PreMap(
      this._owner,
      this._time,
      this._time_end,
      this._title,
      this._exp,
      this._pic,
      this._logo,
      this._pCode,
      this._group,
      this._inviteText,
      this._settings,
      this._location,
      this._invitedInfo,
      this._tel,
      this.tags,
      this._email,
      this._address,
      this._id,
      this._freeQuestion,
      this._isActive);

  bool get isActive => _isActive;

  set isActive(bool value) {
    _isActive = value;
  }
  bool get freeQuestion => _freeQuestion;

  set freeQuestion(bool value) {
    _freeQuestion = value;
  }


  get id => _id;

  set id(value) {
    _id = value;
  }

  get address => _address;

  set address(value) {
    _address = value;
  }

  get email => _email;

  set email(value) {
    _email = value;
  }

  get whats_up => tags;

  set whats_up(value) {
    tags = value;
  }

  get tel => _tel;

  set tel(value) {
    _tel = value;
  }

  get invitedInfo => _invitedInfo;

  set invitedInfo(value) {
    _invitedInfo = value;
  }

  get location => _location;

  set location(value) {
    _location = value;
  }

  get settings => _settings;

  set settings(value) {
    _settings = value;
  }

  get inviteText => _inviteText;

  set inviteText(value) {
    _inviteText = value;
  }

  get group => _group;

  set group(value) {
    _group = value;
  }

  get pCode => _pCode;

  set pCode(value) {
    _pCode = value;
  }

  get logo => _logo;

  set logo(value) {
    _logo = value;
  }

  get pic => _pic;

  set pic(value) {
    _pic = value;
  }

  get exp => _exp;

  set exp(value) {
    _exp = value;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  get time => _time;

  set time(value) {
    _time = value;
  }
  get time_end => _time_end;

  set time_end(value) {
    _time_end = value;
  }

  String get owner => _owner;

  set owner(String value) {
    _owner = value;
  }
}