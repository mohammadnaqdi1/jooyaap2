class LotteryModel{
  String _id,_title,p_id;
  int _type,_winner,_state,time;
  bool _isEnd;
  List<dynamic> _names,_ids;

  LotteryModel(this._id, this._title, this.p_id, this._type, this._winner,
      this._state, this.time, this._isEnd, this._names, this._ids);

  get ids => _ids;

  set ids(value) {
    _ids = value;
  }

  List<dynamic> get names => _names;

  set names(List<dynamic> value) {
    _names = value;
  }

  bool get isEnd => _isEnd;

  set isEnd(bool value) {
    _isEnd = value;
  }

  get state => _state;

  set state(value) {
    _state = value;
  }

  get winner => _winner;

  set winner(value) {
    _winner = value;
  }

  int get type => _type;

  set type(int value) {
    _type = value;
  }

  get title => _title;

  set title(value) {
    _title = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }
}