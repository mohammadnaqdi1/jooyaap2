// ignore_for_file: file_names

import 'dart:convert';

class InviteUserMap {

  final String pId,phone,type;


  InviteUserMap( this.type, this.pId,
      this.phone);

  static Map<String, dynamic> toMap(InviteUserMap services) => {
    'type': services.type,
    'pId': services.pId,
    'phone': services.phone,
  };

  static String encode(InviteUserMap sts) => json.encode(InviteUserMap.toMap(sts));


}
