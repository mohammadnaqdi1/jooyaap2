// ignore_for_file: file_names
class UsersMapAccess {


  String _id,_phone,_type,_name,pic;
  bool _can_delete,_can_change,_add_admin,_manage_user,_remove_admin,_can_publish,_send_q,_manage_q,_can_copy;

  UsersMapAccess(
      this._id,
      this._phone,
      this._type,
      this._name,
      this.pic,
      this._can_delete,
      this._can_change,
      this._add_admin,
      this._manage_user,
      this._remove_admin,
      this._can_publish,
      this._send_q,
      this._manage_q,
      this._can_copy);

  get can_copy => _can_copy;

  set can_copy(value) {
    _can_copy = value;
  }

  get manage_q => _manage_q;

  set manage_q(value) {
    _manage_q = value;
  }

  get send_q => _send_q;

  set send_q(value) {
    _send_q = value;
  }

  get can_publish => _can_publish;

  set can_publish(value) {
    _can_publish = value;
  }

  get remove_admin => _remove_admin;

  set remove_admin(value) {
    _remove_admin = value;
  }

  get manage_user => _manage_user;

  set manage_user(value) {
    _manage_user = value;
  }

  get add_admin => _add_admin;

  set add_admin(value) {
    _add_admin = value;
  }

  get can_change => _can_change;

  set can_change(value) {
    _can_change = value;
  }

  bool get can_delete => _can_delete;

  set can_delete(bool value) {
    _can_delete = value;
  }

  get name => _name;

  set name(value) {
    _name = value;
  }

  get type => _type;

  set type(value) {
    _type = value;
  }

  get phone => _phone;

  set phone(value) {
    _phone = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }
}