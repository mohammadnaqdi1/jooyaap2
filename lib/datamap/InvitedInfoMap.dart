// ignore_for_file: file_names

import 'dart:convert';

import 'package:flutter/cupertino.dart';

class InvitedInfoMap {
  final String p_id, more;
  final bool birthDay,email,city,education,job,gender,other1,other2,other3;


  InvitedInfoMap(
      this.p_id,
      this.more,
      this.birthDay,
      this.email,
      this.city,
      this.education,
      this.job,
      this.gender,
      this.other1,
      this.other2,
      this.other3);

  static Map<String, dynamic> toMap(InvitedInfoMap services) => {
   'p_id': services.p_id,
   'more': services.more,
   'birthDay': services.birthDay,
   'email': services.email,
   'city': services.city,
   'education': services.education,
   'job': services.job,
   'gender': services.gender,
   'other1': services.other1,
   'other2': services.other2,
   'other3': services.other3,
 };
 factory InvitedInfoMap.fromJson(Map<String, dynamic> jsonData) {

   return InvitedInfoMap(
       jsonData['p_id'],
       jsonData['more'],
       jsonData['birthDay'],
        jsonData['email'],
       jsonData['city'],
       jsonData['education'],
        jsonData['job'],
        jsonData['gender'],
        jsonData['other1'],
        jsonData['other2'],
        jsonData['other3'],
   );
 }

 static String encode(InvitedInfoMap sts) => json.encode(InvitedInfoMap.toMap(sts));

 //static InvitedInfoMap decode(String InvitedInfoMap_list) => (json.decode(InvitedInfoMap_list)).;

 static InvitedInfoMap decodeSettings(String ServicesMap_list) {
     return InvitedInfoMap.fromJson(json.decode(ServicesMap_list));
 }

}
