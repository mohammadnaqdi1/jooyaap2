// ignore_for_file: file_names
class AnswerDataMap {


  String _qid,_ans,_userId;

  AnswerDataMap(this._qid, this._ans,this._userId);

  get ans => _ans;

  set ans(value) {
    _ans = value;
  }

  get userId => _userId;

  set userId(value) {
    _userId = value;
  }

  String get qid => _qid;

  set qid(String value) {
    _qid = value;
  }
}